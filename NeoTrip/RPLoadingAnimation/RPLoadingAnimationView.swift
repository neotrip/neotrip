//
//  RPLoadingAnimationView.swift
//  RPLoadingAnimation
//
//  Created by naoyashiga on 2015/10/11.
//  Copyright © 2015年 naoyashiga. All rights reserved.
//

import UIKit

public enum RPLoadingAnimationType {
    case RotatingCircle, SpininngDot, LineScale, DotTrianglePath,
    DotSpinningLikeSkype, FunnyDotsA
}

class AnimationFactory {
    class func animation() -> RPLoadingAnimationDelegate {
        return RotatingCircle()
    }
}

public class RPLoadingAnimationView: UIView {
    private static let defaultType = RPLoadingAnimationType.RotatingCircle
    private static let defaultColor = UIColor.blackColor()
    private static let defaultSize = CGSize(width: 40, height: 40)
    
    private var color: UIColor
    private var size: CGSize
    
    required public init?(coder aDecoder: NSCoder) {
        self.color = RPLoadingAnimationView.defaultColor
        self.size = RPLoadingAnimationView.defaultSize
        super.init(coder: aDecoder)
    }
    
    init(frame: CGRect, color: UIColor = defaultColor) {
        self.color = color
        self.size = CGSize(width: 100, height: 100)
        super.init(frame: frame)
    }
    
    func setupAnimation() {
        let animation = AnimationFactory.animation()
        layer.sublayers = nil
        animation.setup(layer, size: size, color: color)
    }
}