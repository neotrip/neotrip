//
//  MyProfileController.swift
//  NeoTrip
//
//  Created by Lucas Padilha on 1/25/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

class MyProfileController: ProfileController {

    @IBOutlet weak var logoutButton: CustomButton!
    @IBOutlet weak var editButton: CustomButton!
    
    @IBOutlet weak var myVisitedCountries: UICollectionView!
    
    let waitingIndicator = UIActivityIndicatorView()
    
    override func viewDidLoad() {
        traveler = databaseManager!.traveler!
        super.viewDidLoad()
        
        segueIdentifier = "showFollow"

        travels = Travel.search(traveler, option: TRAVELSOWNED)
        cellIdentifier = "myFlagCell"
    
        logoutButton.setColorBorder(UIColor.clearColor())
        logoutButton.enabled = true
        
        editButton.setColorBorder(ColorPalette.darkGray())
        
        myVisitedCountries.delegate = self
        myVisitedCountries.dataSource = self
        myVisitedCountries.backgroundColor = UIColor.clearColor()
    }
    
    override func refresh() {
        super.refresh()
        travels = Travel.search(traveler, option: TRAVELSOWNED)
        travelsTableView.reloadData()
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        if traveler.countries == nil{
            traveler.countries = "BR"
        }
        
        self.countriesArray = (traveler.countries?.ISOArray)!
        myVisitedCountries.reloadData()
        travelsTableView.reloadData()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        travelerName.text = traveler.firstName! + " " + traveler.lastName!
        headerLabel.text = travelerName.text
        if (traveler.city != nil) {
            livingCity.text = "\(traveler.city!.name!), \(traveler.city!.country!.name!)"
        }
        else {
            livingCity.text = "-"
        }
        
        if (traveler.followers != nil) {
            followerButton.setTitle(String(traveler.followers!.count) + " Seguidores", forState: .Normal)
        }
        if (traveler.following != nil) {
            followingButton.setTitle(String(traveler.following!.count) + " Seguindo", forState: .Normal)
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        super.prepareForSegue(segue, sender: sender)
        
        if (segue.identifier == "showEditProfile") {
            let editController = segue.destinationViewController as! EditProfileController
            editController.traveler = traveler
        }
    }
    
    // MARK: - Button Actions
    @IBAction func editProfile(sender: CustomButton){
        performSegueWithIdentifier("showEditProfile", sender: self)
    }
    
    @IBAction func tappedLogout(sender: UIButton) {
        logoutButton.enabled = false
        waitingIndicator.color = UIColor.redColor()
        logoutButton.startWaiting(waitingIndicator)
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            databaseManager?.logout()
            dispatch_sync(dispatch_get_main_queue()) {
                self.logoutButton.enabled = true
                self.logoutButton.stopWaiting(self.waitingIndicator)
                self.navigationController?.navigationController?.popToRootViewControllerAnimated(false)
            }
        }
    }
    
    // MARK: - Unwind Segue
    @IBAction func doneEditProfileUnwindSegue(unwindSegue: UIStoryboardSegue) {
        
    }
}
