//
//  TestsViewController.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 10/27/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import UIKit
import CoreData

class TestsServerViewController: UIViewController, NSStreamDelegate {
    var traveler = Traveler()

    let database = ServerManager()
    
    override func viewDidLoad() {
        
    }
    
    @IBAction func signup(sender: AnyObject) {
        let user = User()
        user.id = "jao"
        user.password = "0000"
        
        traveler = Traveler()
        traveler.id = user.id
        traveler.firstName = "Joao"
        traveler.lastName = "da Silva"
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            print(self.database.signup(self.traveler, withUser: user))
        }
        
    }
    @IBAction func doSomething(sender: AnyObject) {
        //database.disconnect()
        
        let user = User()
        user.id = "jao"
        user.password = "0000"
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            let (_, traveler) = self.database.login(user, retry: false)
            if let t = traveler {
                self.traveler = t
                print(true)
            } else {
                print(false)
            }
        }
    }
    
    var travel:Travel?
    var place:Place?
    var place2:Place?
    var transport:Transport?
    
    @IBAction func doAnotherSomething(sender: AnyObject) {
        
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            let queries:NSMutableDictionary = NSMutableDictionary()
            queries[Place.COUNTRY] = 4
            queries[Place.CITY] = 8
            queries["query"] = "Húe"
            print(self.database.search(SearchType.PLACE, options: queries))
            
//            queries[City.COUNTRY] = 4
//            print(self.database.search(GetType.City, query: "Húe", options: queries))
        }

        /*
        travel = Travel()
        place = Place()
        place2 = Place()
        transport = Transport()
        let itinerary = travel?.itinerary!
        
        travel?.name = "Teste"
        travel?.travelerOwner = traveler
        travel?.save()
        
        itinerary?.startTime = NSDate()
        itinerary?.interval = 10
        itinerary?.save()
        
        
        place?.name = "abc"
        place?.price = 10
        place?.startTime = NSDate()
        place?.interval = 10
        place?.itinerary = itinerary
        place?.save()
        
        
        place2?.name = "def"
        place2?.price = 10
        place2?.startTime = NSDate()
        place2?.interval = 10
        place2?.itinerary = itinerary
        place2?.save()
        
        transport?.price = 10
        transport?.startTime = NSDate()
        transport?.interval = 10
        transport?.startPoint = place
        transport?.endPoint = place2
        transport?.itinerary = itinerary
        transport?.save()
*/
        
    }
    
    @IBAction func doMoreMoreSomething(sender: AnyObject) {
        travel?.save()
        travel?.itinerary?.save()
        place?.save()
        place2?.save()
        transport?.save()
    }
    @IBAction func doAnotherMoreSomething(sender: AnyObject) {
        travel?.delete()
    }
    
    @IBAction func getSomething(sender: AnyObject) {

//        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
//            let (result, travelsOwned, travelsTaking) = self.database.getTravelsOwned(forTraveler: self.traveler)
//            
//            if (travelsOwned.count != 0) {
//                self.database.getTravelInfo(forTravel: travelsOwned[0] as! Travel)
//            }
//        }
    }
    
}

