//
//  ProfileTravelsTableViewCell.swift
//  NeoTrip
//
//  Created by Lucas Padilha on 1/21/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

class ProfileTravelsTableViewCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var interval: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func fillCell(travel: Travel) {
        self.name.text = travel.fillName()
        if (travel.itinerary?.interval != nil) {
            self.interval.text = String(travel.itinerary!.interval!)
        }
        else {
            self.interval.text = "0"
        }
    }
}
