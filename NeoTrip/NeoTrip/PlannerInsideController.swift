//
//  PlannerInsideController.swift
//  NeoTrip
//
//  Created by Joao Pedro Fabiano Franco on 1/6/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

class PlannerInsideController: UIViewController {
    
    @IBOutlet weak var graph: Graph!
    
    @IBOutlet weak var placeName: UILabel!
    @IBOutlet weak var placePrice: UILabel!
    
    @IBOutlet weak var cityView: CircularView!
    @IBOutlet weak var plannerInsideView: CircularView!
    
    @IBOutlet weak var beginDateTextField: UILabel!
    @IBOutlet weak var endDateTextField: UILabel!
    
    var beginDate : NSDate?
    var endDate : NSDate?
    
//    var cityViewPosition : CGPoint!
    
    var place: Place! // cidade
    var itinerary: Itinerary!
    var newPosition: CGPoint!
    
    var transportEdge = TransportEdge()
    var began: PlaceNode!
    var ended: PlaceNode!
    
    var deleteMode : Bool = false
    
    var currentEdittingPlace : Place? = nil
    var currentEdittingTransport : Transport? = nil
    var type : Int = -1
    
    // Tutorial
    var tutorial: Tutorial?
    @IBOutlet weak var tutorialFeedback: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.graph.contentSize = self.plannerInsideView.frame.size
        self.graph.contentInset = UIEdgeInsetsZero
        
        // Gestures
        let touchGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("addNewAA:"))
        self.graph.addGestureRecognizer(touchGesture)
        
        self.graph.plannerInside = self
        self.graph.planner = nil
        
        // Default
        currentEdittingPlace = nil
        
        self.cityView.makeCircle()
        self.plannerInsideView.makeCircle()
        
        graph.itinerary = itinerary
        
        if (beginDate != nil) {
            beginDateTextField.text = beginDate!.dateToString
        }
        else {
            beginDateTextField.text = "-"
        }
        if (endDate != nil) {
            endDateTextField.text = endDate!.dateToString
        }
        else {
            endDateTextField.text = "-"
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "refresh", name: TravelRefreshed, object: nil)
        
        fillPlanner()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        popIn()
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        UIApplication.sharedApplication().statusBarStyle = .Default
    }
    
    // MARK: - Animation
    func popIn() {
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            self.cityView.transform = CGAffineTransformMakeScale(5.5, 5.5)
//            self.cityView.center = self.plannerInsideView.center
            }) { (result) -> Void in
                self.plannerInsideView.hidden = false
                self.graph.generateGraphInside(self.place.city!)
                if (self.tutorial != nil) {
                    self.tutorial!.initTutorial()
                }
        }
    }
    
    func popOut () {
        self.plannerInsideView.hidden = true
        UIView.animateWithDuration(0.2, animations: { () -> Void in
            self.cityView.transform = CGAffineTransformMakeScale(1, 1)
//            self.cityView.center = self.cityViewPosition
            }) { (result) -> Void in
                self.performSegueWithIdentifier("closeDonePlannerInside", sender: self)
        }
    }
    
    // MARK: - Actions
    @IBAction func backCity(sender: UIButton) {
        popOut()
    }
    
    // MARK: - Tap Gesture
    func addNewAA(recognizer: UITapGestureRecognizer){
        if (!deleteMode){
            performSegueWithIdentifier("showModalOptions", sender: self)
        } else {
            changeDeleteMode(!deleteMode)
        }
    }
    
    // MARK: - Segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        changeDeleteMode(false)
        if (segue.identifier == "showModalOptions"){
            let optionsController = segue.destinationViewController as! OptionsController
            optionsController.itinerary = itinerary
            optionsController.place = place
        }
        if (segue.identifier == "showModalAttraction") {
            let attractionController = segue.destinationViewController as! AttractionController
            if (currentEdittingPlace is Attraction) {
                attractionController.attraction = currentEdittingPlace as? Attraction
            }
            attractionController.itinerary = itinerary
            attractionController.place = place
        }
        if (segue.identifier == "showModalAccommodation") {
            let accommodationController = segue.destinationViewController as! AccommodationController
            if (currentEdittingPlace is Accomodation) {
                accommodationController.accommodation = currentEdittingPlace as? Accomodation
            }
            accommodationController.itinerary = itinerary
            accommodationController.place = place
        }
        if (segue.identifier == "showModalPort") {
            let portController = segue.destinationViewController as! PortController
            if (currentEdittingPlace is Port) {
                portController.port = currentEdittingPlace as? Port
            }
            portController.itinerary = itinerary
            portController.place = place
        }
        if (segue.identifier == "showModalTransportInside") {
            let transportController = segue.destinationViewController as! TransportController
            transportController.inside = true
            transportController.transport = self.currentEdittingTransport
            transportController.startNode = self.transportEdge.startNode
            transportController.endedNode = self.transportEdge.endedNode
            transportController.itinerary = self.itinerary
        }
    }
    
    // MARK: - Aux
    func refresh() {
        dispatch_async(dispatch_get_main_queue()) {
            self.fillPlanner()
            self.graph.generateGraphInside(self.place.city!)
        }
    }
    
    func addTransport(){
        performSegueWithIdentifier("showModalTransportInside", sender: self)
    }
    
    func fillPlanner(){
        placeName.text = place.city!.name
        placePrice.text = String.cashString(place.sumPrices())
    }
    
    // MARK: - Alert
    func showFailAlertView(){
        let alertFailTransport = UIAlertController(title: "Erro", message: "Nao foi possivel ligar", preferredStyle: UIAlertControllerStyle.Alert)
        
        alertFailTransport.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alertFailTransport, animated: true, completion: nil)
    }
    
    // MARK: - TextField Delegate
    func textFieldShouldReturn(textField: UITextField!) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Unwind Segue
    @IBAction func cancelOptionsUnwindSegue(unwindSegue: UIStoryboardSegue){
        
    }
    
    @IBAction func doneOptionsUnwindSegue(unwindSegue: UIStoryboardSegue){
        self.cityView.center = self.plannerInsideView.center
        refresh()
    }
    
    @IBAction func cancelAttractionUnwindSegue(unwindSegue: UIStoryboardSegue) {
        
    }
    
    @IBAction func doneAttractionUnwindSegue(unwindSegue: UIStoryboardSegue) {
        self.cityView.center = self.plannerInsideView.center
        refresh()
        
        if (tutorial != nil) {
            if (tutorial!.index == 4) {
                tutorial!.advanceLevel()
                tutorial!.initTutorial()
            }
        }
    }
    
    @IBAction func cancelAccommodationUnwindSegue(unwindSegue: UIStoryboardSegue) {
        
    }
    
    @IBAction func doneAccommodationUnwindSegue(unwindSegue: UIStoryboardSegue) {
        self.cityView.center = self.plannerInsideView.center
        refresh()
        
        if (tutorial != nil) {
            if (tutorial!.index == 4) {
                tutorial!.advanceLevel()
                tutorial!.initTutorial()
            }
        }
    }
    
    @IBAction func cancelPortUnwindSegue(unwindSegue: UIStoryboardSegue) {
        
    }
    
    @IBAction func donePortUnwindSegue(unwindSegue: UIStoryboardSegue) {
        self.cityView.center = self.plannerInsideView.center
        refresh()
        
        if (tutorial != nil) {
            if (tutorial!.index == 4) {
                tutorial!.advanceLevel()
                tutorial!.initTutorial()
            }
        }
    }
    
    @IBAction func cancelTransportInsideUnwindSegue(unwindSegue: UIStoryboardSegue) {
        
    }
    
    @IBAction func doneTransportInsideUnwindSegue(unwindSegue:UIStoryboardSegue) {
        self.plannerInsideView.center = self.cityView.center
        refresh()
        currentEdittingTransport = nil
    }
    
    // MARK: - Auxiliary Methods
    func deletePlace (place: Place) {
        place.delete()
        
        changeDeleteMode(!self.deleteMode)
        
        refresh()
    }
    
    func deleteTransport(transport: Transport) {
        transport.delete()
        
        changeDeleteMode(!self.deleteMode)
        
        refresh()
    }
    
    func changeDeleteMode (value: Bool) {
        self.deleteMode = value
        if (value) {
            for node in graph.placesNode {
//                node.changeColor(UIColor.grayColor())
                node.changeDeleteMode(self.deleteMode)
            }
            
            for node in graph.transportsNode {
                //                node.changeColor(UIColor.grayColor())
                node.changeDeleteMode(self.deleteMode)
            }
        }
        else {
            for node in graph.placesNode {
//                if (node.place is Accomodation) {
//                    node.changeColor(ColorPalette.red())
//                }
//                else if (node.place is Attraction) {
//                    node.changeColor(ColorPalette.yellow())
//                }
//                else if (node.place is Port) {
//                    node.changeColor(ColorPalette.purple())
//                }
                node.changeDeleteMode(self.deleteMode)
            }
            
            for node in graph.transportsNode {
                //                node.changeColor(UIColor.grayColor())
                node.changeDeleteMode(self.deleteMode)
            }
        }
    }
}

extension PlannerInsideController: placeNodeDelegate {

    func tapOnNode(placeNode: PlaceNode) {
        if (placeNode.place is Accomodation) {
            currentEdittingPlace = placeNode.place as! Accomodation
            performSegueWithIdentifier("showModalAccommodation", sender: self)
        }
        else if (placeNode.place is Attraction) {
            currentEdittingPlace = placeNode.place as! Attraction
            performSegueWithIdentifier("showModalAttraction", sender: self)
        }
        else if (placeNode.place is Port) {
            currentEdittingPlace = placeNode.place as! Port
            performSegueWithIdentifier("showModalPort", sender: self)
        }
    }
    
    func longPressOnNode(placeNode: PlaceNode) {
        changeDeleteMode(!self.deleteMode)
    }
    
    func panOnNode(placeNode: PlaceNode, recognizer: UIPanGestureRecognizer) {
        let position = recognizer.locationInView(self.graph)
        var abble : Bool = true
        began = placeNode
        
        if (!deleteMode) {
            if (recognizer.state == .Began) {
                transportEdge.startNode = began.place
            }
            else if (recognizer.state == .Changed) {
                if (began != nil){
                    self.graph.removeTemporaryEdges()
                    self.graph.layer.addSublayer(self.graph.setArrow(began.center, point2: position))
                }
                
                let boundsLeft = self.graph.bounds.origin.x
                let boundsRight = self.graph.bounds.origin.x + self.graph.bounds.size.width
                let positionX = position.x
                let margin: CGFloat = 15.0
                let scroll: CGFloat = 100.0
                
                if (positionX < boundsLeft + margin){
                    self.graph.scrollRectToVisible(CGRect(x: self.graph.bounds.origin.x - scroll, y: self.graph.bounds.origin.y, width: self.graph.bounds.size.width, height: self.graph.bounds.size.height), animated: true)
                }
                
                if (positionX > boundsRight - margin){
                    self.graph.scrollRectToVisible(CGRect(x: self.graph.bounds.origin.x + scroll, y: self.graph.bounds.origin.y, width: self.graph.bounds.size.width, height: self.graph.bounds.size.height), animated: true)
                }
                
                began.selectionView.layer.borderColor = ColorPalette.blue().CGColor
                began.selectionView.layer.borderWidth = 3
                
                for node in graph.placesNode {
                    if (CGRectContainsPoint(node.frame, position)) {
                        for t in itinerary!.transports!.allObjects as! [Transport] {
                            if (t.endPoint == node.place) {
                                abble = false
                            }
                        }
                        if (abble) {
                            node.selectionView.layer.borderColor = ColorPalette.blue().CGColor
                            node.selectionView.layer.borderWidth = 3
                        }
                        else {
                            node.selectionView.layer.borderColor = ColorPalette.red().CGColor
                            node.selectionView.layer.borderWidth = 3
                        }
                    }
                    else {
                        if (node != began) {
                            node.selectionView.layer.borderColor = UIColor.clearColor().CGColor
                            node.selectionView.layer.borderWidth = 0
                        }
                    }
                }
            }
            else if (recognizer.state == .Ended) {
                abble = true
                began.selectionView.layer.borderColor = UIColor.clearColor().CGColor
                began.selectionView.layer.borderWidth = 0
                began = nil
                self.graph.removeTemporaryEdges()
                for node in graph.placesNode {
                    if (CGRectContainsPoint(node.frame, position)) {
                        for t in itinerary!.transports!.allObjects as! [Transport] {
                            if (t.endPoint == node.place) {
                                abble = false
                            }
                        }
                        ended = node
                        ended.selectionView.layer.borderColor = UIColor.clearColor().CGColor
                        ended.selectionView.layer.borderWidth = 0
                        if (abble) {
                            transportEdge.endedNode = node.place
                        }
                        else {
                            transportEdge.startNode = nil
                            transportEdge.endedNode = nil
                        }
                        break;
                        
                    }
                }
                
                if (((transportEdge.startNode) != nil) &&
                    ((transportEdge.endedNode) != nil) &&
                    (transportEdge.startNode != transportEdge.endedNode) &&
                    abble) {
                        
                        addTransport()
                        
                        transportEdge.startNode = nil
                        transportEdge.endedNode = nil
                }
                else if (!abble) {
                    // NAO PODE VOLTAR PRA UMA CIDADE QUE VC JA SAIU
                    showFailAlertView()
                }
                else{
                    showFailAlertView()
                }
            }
            else {
                began.selectionView.layer.borderColor = UIColor.clearColor().CGColor
                began.selectionView.layer.borderWidth = 0
                began = nil
                self.graph.removeTemporaryEdges()
            }
        }
        else {
            if (recognizer.state == .Began) {
                
            }
            else if (recognizer.state == .Changed) {
                let translation = recognizer.translationInView(self.view)
                placeNode.center = CGPointMake(placeNode.initialPosition.x + translation.x, placeNode.initialPosition.y + translation.y)
            }
            else if (recognizer.state == .Ended) {
                placeNode.center = placeNode.initialPosition
            }
        }
    }
    
    func tapOnDelete(placeNode: PlaceNode) {
        deletePlace(placeNode.place)
    }
    
    func tapOnRoot(placeNode: PlaceNode) {
        
    }
}

extension PlannerInsideController: TransportNodeDelegate{
    
    func showAddNewCity() {
        performSegueWithIdentifier("showModalCity", sender: self)
    }
    
    func showEditTransport(transport: Transport) {
        currentEdittingTransport = transport
        performSegueWithIdentifier("showModalTransportInside", sender: self)
    }
    
    func touchDelete(transportNode: TransportNode) {
        deleteTransport(transportNode.transport!)
    }
    
    func longPress(transportNode: TransportNode) {
        changeDeleteMode(!self.deleteMode)
    }
    
}

// MARK: - Tutorial
extension PlannerInsideController: TutorialDelegate {
    func prepareTutorial(index: Int) {
        switch index {
        case 4:
            tutorialFeedback.hidden = false
            tutorialFeedback.text = "Toque em algum espaço vazio para criar uma atração, acomodação ou um porto dentro dessa cidade"
            break
        case 5:
            tutorialFeedback.hidden = false
            tutorialFeedback.text = "Toque em pronto para salvar e sair da cidade"
            break
        default:
            tutorialFeedback.hidden = true
            break
        }
    }
    
    func updateName() {
        
    }
    
    func advanceLevel() {
        
    }
    
    func finishTutorial() {
        tutorial = nil
    }
}