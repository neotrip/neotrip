//
//  CityView.swift
//  NeoTrip
//
//  Created by Joao Pedro Fabiano Franco on 10/30/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import UIKit

class CityView: UIView {
    
    
    @IBOutlet weak var name: ViewTextEdit!
    @IBOutlet weak var daysSlider: UISlider!
    @IBOutlet weak var accomodation: ViewLabel!
    @IBOutlet weak var attraction: ViewLabel!
    
    
    
    func returnObject ()-> City{
        let cityObject = City()
            cityObject.name = name.textedit.text
        return cityObject
    }
}
