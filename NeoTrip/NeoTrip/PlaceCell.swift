//
//  PlaceCell.swift
//  NeoTrip
//
//  Created by Carlos Henrique Cayres on 12/17/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import UIKit

class PlaceCell: UITableViewCell {

    
    @IBOutlet weak var placeName: UILabel!
    
    @IBOutlet weak var placePrice: UILabel!
    
    @IBOutlet weak var placeAddress: UILabel!
    
    @IBOutlet weak var placeImage: UIImageView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
