//
//  ProfileController.swift
//  NeoTrip
//
//  Created by Lucas Padilha on 1/25/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

class ProfileController: UIViewController, UIScrollViewDelegate, UITableViewDelegate, UITableViewDataSource, UICollectionViewDelegate, UICollectionViewDataSource{
    
    // ScrollView
    @IBOutlet var scrollView:UIScrollView!
    
    // Images
    @IBOutlet var avatarImageView:UIImageView!
    @IBOutlet var headerImageView:UIImageView!
    @IBOutlet var headerBlurImageView:UIImageView!
    var blurredHeaderImageView:UIImageView?
    
    // Header
    @IBOutlet var header:UIView!
    
    // Labels
    @IBOutlet var headerLabel:UILabel!
    @IBOutlet weak var travelerName: UILabel!
    @IBOutlet weak var travelerUsername: UILabel!
    @IBOutlet weak var livingCity: UILabel!
    
    // Table View
    @IBOutlet weak var travelsTableView: UITableView!
    
    // Buttons
    @IBOutlet weak var followingButton: UIButton!
    @IBOutlet weak var followerButton: UIButton!
    var tapFollowing : Bool = false // AUX
    
    // AUXILIAR
    let offset_HeaderStop:CGFloat = 40.0 // At this offset the Header stops its transformations
    let offset_B_LabelHeader:CGFloat = 95.0 // At this offset the Black label reaches the Header
    let distance_W_LabelHeader:CGFloat = 35.0 // The distance between the bottom of the Header and the top of the White Label
    var segueIdentifier: String = "showFollow"
    var cellIdentifier: String = "flagCell"
    
    // Outside information
    var traveler : Traveler! // Viajante que esta
    var travels: [Travel] = []
    
    var countriesArray:[String] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        scrollView.delegate = self
        
        avatarImageView.image = UIImage(data: traveler.avatar.image!)

        headerImageView = UIImageView(frame: header.bounds)
        headerImageView!.image = UIImage(data: traveler.bgImage.image!)
        headerBlurImageView = UIImageView(frame: header.bounds)
        
        self.travelerUsername.text = "@" + self.traveler.id!
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            databaseManager!.getTravelerInfo(forTraveler: self.traveler)
            databaseManager!.getTravels(forTraveler: self.traveler)
            
            dispatch_sync(dispatch_get_main_queue()) {
                self.refresh()
            }
        }
        
        headerImageView.frame = header.bounds
        headerBlurImageView.frame = header.bounds
        headerImageView?.contentMode = UIViewContentMode.ScaleAspectFill
        header.insertSubview(headerImageView, belowSubview: headerLabel)
        
        // Header - Blurred Image
        headerBlurImageView?.image = headerImageView.image!.blurredImageWithRadius(10, iterations: 20, tintColor: UIColor.clearColor())
        headerBlurImageView?.contentMode = UIViewContentMode.ScaleAspectFill
        headerBlurImageView?.alpha = 0.0
        header.insertSubview(headerBlurImageView, belowSubview: headerLabel)
        
        header.clipsToBounds = true
        headerLabel.shadowColor = UIColor.blackColor()
    }
    
    func refresh() {
        
        self.travelerName.text = self.traveler.firstName! + " " + self.traveler.lastName!
        self.headerLabel.text = self.travelerName.text
        self.travelerUsername.text = "@" + self.traveler.id!
        
        avatarImageView.image = UIImage(data: traveler.avatar.image!)
        headerImageView?.image = UIImage(data: traveler.bgImage.image!)
        
        if (self.traveler.city != nil) {
            self.livingCity.text = "\(self.traveler.city!.name!), \(self.traveler.city!.country!.name!)"
        } else {
            self.livingCity.text = "-"
        }
    
        if (self.traveler.followers != nil) {
            self.followingButton.setTitle(String(self.traveler.following!.count) + " Seguindo", forState: .Normal)
        }
        if (self.traveler.following != nil) {
            self.followerButton.setTitle(String(self.traveler.followers!.count) + " Seguidores", forState: .Normal)
        }
    
    
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - ScrollView Methods
    func scrollViewDidScroll(scrollView: UIScrollView) {
        if (scrollView == self.scrollView) {
            let offset = scrollView.contentOffset.y
            var avatarTransform = CATransform3DIdentity
            var headerTransform = CATransform3DIdentity
            
            // PULL DOWN -----------------
            if offset < 0 {
                let headerScaleFactor:CGFloat = -(offset) / header.bounds.height
                let headerSizevariation = ((header.bounds.height * (1.0 + headerScaleFactor)) - header.bounds.height)/2.0
                headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
                headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
                
                header.layer.transform = headerTransform
            }
                
            // SCROLL UP/DOWN ------------
            else {
                
                // Header -----------
                headerTransform = CATransform3DTranslate(headerTransform, 0, max(-offset_HeaderStop, -offset), 0)
                
                //  ------------ Label
                let labelTransform = CATransform3DMakeTranslation(0, max(-distance_W_LabelHeader, offset_B_LabelHeader - offset), 0)
                headerLabel.layer.transform = labelTransform
                
                //  ------------ Blur
                headerBlurImageView?.alpha = min (1.0, (offset - offset_B_LabelHeader)/distance_W_LabelHeader)
                
                // Avatar -----------
                let avatarScaleFactor = (min(offset_HeaderStop, offset)) / avatarImageView.bounds.height / 1.4 // Slow down the animation
                let avatarSizeVariation = ((avatarImageView.bounds.height * (1.0 + avatarScaleFactor)) - avatarImageView.bounds.height) / 2.0
                avatarTransform = CATransform3DTranslate(avatarTransform, 0, avatarSizeVariation, 0)
                avatarTransform = CATransform3DScale(avatarTransform, 1.0 - avatarScaleFactor, 1.0 - avatarScaleFactor, 0)
                
                if offset <= offset_HeaderStop {
                    
                    if avatarImageView.layer.zPosition < header.layer.zPosition{
                        header.layer.zPosition = 0
                    }
                    
                }else {
                    if avatarImageView.layer.zPosition >= header.layer.zPosition{
                        header.layer.zPosition = 2
                    }
                }
            }
            
            // Apply Transformations

            header.layer.transform = headerTransform
            avatarImageView.layer.transform = avatarTransform
        }
        else {
            
        }
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.destinationViewController is FollowController) {
            let followController = segue.destinationViewController as! FollowController
            
            if (tapFollowing) {
                // following button
                followController.followTravelers = traveler.following?.allObjects as! [Traveler]
            }
            else {
                // follower button
                followController.followTravelers = traveler.followers?.allObjects as! [Traveler]
            }
        }
    }
    
    // MARK: - Button Action
    @IBAction func tappedFollowers(sender: UIButton) {
        tapFollowing = false
        performSegueWithIdentifier(segueIdentifier, sender: self)
    }
    
    @IBAction func tappedFollowing(sender: UIButton) {
        tapFollowing = true
        performSegueWithIdentifier(segueIdentifier, sender: self)
    }
    
    
    // MARK: - TableView Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return travels.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let travelsCell = tableView.dequeueReusableCellWithIdentifier("profileTravelsCell", forIndexPath: indexPath) as! TravelCell
        travelsCell.fillCell(travels[indexPath.row])
        return travelsCell
    }
    
    // MARK: - CollectionViewDelegate
    func numberOfSectionsInCollectionView(collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return countriesArray.count
    }
    
    func collectionView(collectionView: UICollectionView, cellForItemAtIndexPath indexPath: NSIndexPath) -> UICollectionViewCell {
        
        let cell = collectionView.dequeueReusableCellWithReuseIdentifier(cellIdentifier, forIndexPath: indexPath)
        
        if (UIImage(named: countriesArray[indexPath.row].lowercaseString) != nil){
            let flagImageView = UIImageView(image: UIImage(named: countriesArray[indexPath.row].lowercaseString))
            flagImageView.frame = CGRect(origin: CGPoint(x: 0, y: 0), size: cell.frame.size)
            flagImageView.contentMode = .ScaleAspectFill
            cell.addSubview(flagImageView)
            
        }else{
            cell.backgroundColor = ColorPalette.red()
        }
        
        return cell
    }
    
    
}
