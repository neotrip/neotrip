//
//  Extensions.swift
//  NeoTrip
//
//  Created by Joao Pedro Fabiano Franco on 10/27/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import UIKit

/*
let defaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
if let bool = defaults.objectForKey("tutorial") as? Bool {
// if exists, can be true or false
} else {
// if not, nil is returned. set true if you are not gonna show again. maybe we can set false if he wants to see tutorial again, don't know
defaults.setBool(true/false, forKey: "tutorial")
defaults.synchronize()
}*/

let encoding = NSUTF8StringEncoding
let TravelRefreshed = "travel_refreshed"
let AddObject = "add_object"
let ConnectionChanged = "connection_changed"

let VERTEX_SIZE: CGFloat = 40.0
let VERTEX_SIZE_AA: CGFloat = 35.0
let TRANSPORT_HEIGHT: CGFloat = 30.0

let INFINITY: CGFloat = 9999.99
let ZERO: CGFloat = 0.0

let GRAPHSIZE: CGFloat = 1000

let COOLDIST: CGFloat = 150

func stringFromDate(date:NSDate?) -> String? {
    if date == nil {
        return nil
    } else {
        let format:NSDateFormatter = NSDateFormatter()
        format.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ"
        
        return format.stringFromDate(date!)
    }
}

func dateFromString(string:String?) -> NSDate? {
    if string == nil {
        return nil
    } else {
        let format:NSDateFormatter = NSDateFormatter()
        format.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZZZ"
        
        return format.dateFromString(string!)
    }
}

protocol Object {
    var id : String? { get set }
    var status : Status { get set }
    var synced : NSDate? { get set }
    func sync()
    func save()
    func delete()
    func canSync() -> Bool
    func setNil()
    func getClass() -> String
    func json(complete:Bool) -> NSDictionary
}

enum Connection : Int {
    case DISCONNECTED
    case CONNECTING
    case CONNECTED
    case LOGGED
}

enum ResponseType : String {
    case STOPPED
    case READY
    case WAITING
    case ACKNOWLEDGE
    case ERROR
    case PUSH
    case AUTHORIZED
}

enum MessageType : String {
    case CONNECT
    
    case IMAGE
    case USER
    case PLANNER
    case SEARCH
    case SOCIAL
    
    case ENDCONNECTION
}

enum UserType : Int {
    case LOGIN
    case SIGNUP
}

enum PlannerType : Int {
    case CREATE
    case EDIT
    case DELETE
    
    case INVITE
    case ACCEPT
    case UNTAKEPART
    
    case REGISTER
    case UNREGISTER
    case REQUESTLOCK
}

enum SearchType : Int {
    case TRAVELSOWNED
    case TRAVELINFO
    case CITY
    case PLACE
    case TRAVELERS
    case TRAVELERINFO
}



enum SocialType : Int {
    case FOLLOW
    case UNFOLLOW
}

enum ImageType : Int {
    case AVATAR
}

enum Result : Int {
    case YES
    case NO
    case WAIT
    case UNCONNECTED
    case ERROR
    case DEPRECATED
}

enum Direction : String {
    case NORTH
    case SOUTH
    case EAST
    case WEST
}

enum GraphType : String {
    case NORMAL
    case INSIDE
}

@objc enum Status : Int32 {
    case edited
    case syncing
    case synced
    case wait
    case delete
    case deleting
    case deleted
    case notSynced
}

extension String {
    
    static func cashString(cashValue: CGFloat) -> String!{
        return "R$ " + String(cashValue)
    }
    
    var priceValue:Double? {
        let numberFormater = NSNumberFormatter()
        let number:NSNumber = numberFormater.numberFromString(self)!
        numberFormater.numberStyle = .CurrencyStyle
        numberFormater.maximumFractionDigits = 2
        return number.doubleValue
    }
    
    var stringToDate:NSDate? {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .MediumStyle
        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
        return dateFormatter.dateFromString(self)
    }
    
    //retorna um array com todos os ISO da String
    var ISOArray:[String]?{
        let string = Array(self.characters)
        var array:[String] = []
        var iso:String!
        
        for c in string{
            if (c == ";"){
                array.append(iso)
                iso = nil
            }else{
                if iso != nil{
                    iso = "\(iso)\(c)"
                }else{
                    iso = "\(c)"
                }
            }
        }
        return array
    }
    
}

extension Array{
    
    //Retorna uma String com todos os ISO do array
    var ISOString:String?{
        var isoString:String!
        
        for element in self {
            let iso = element as! String
            if (isoString != nil){
                isoString = String("\(isoString)\(iso);")
            }else{
                isoString = String("\(iso);")
            }
            
        }
        return isoString
    }
    
}


extension NSDate {
    
    var dateToString:String? {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .MediumStyle
        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
        return dateFormatter.stringFromDate(self)
    }
    
    func daysFrom(date:NSDate) -> Int{
        return NSCalendar.currentCalendar().components(.Day, fromDate: date, toDate: self, options: []).day
    }
    
}

extension UITextField {
    
    override public func awakeFromNib() {
        super.awakeFromNib()
        setDoneButton()
    }
    
    func dateTextField(min:NSDate){
        
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.minimumDate = min
        datePickerView.datePickerMode = UIDatePickerMode.Date
        datePickerView.addTarget(self, action: Selector("datePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        datePickerView.backgroundColor = UIColor.whiteColor()
        datePickerView.tintColor = ColorPalette.purple()
        self.inputView = datePickerView
        datePickerValueChanged(datePickerView)
    }
    
    func dateTextField(min: NSDate, date:NSDate) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.minimumDate = min
        datePickerView.date = date
        datePickerView.datePickerMode = UIDatePickerMode.Date
        datePickerView.addTarget(self, action: Selector("datePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        datePickerView.backgroundColor = UIColor.whiteColor()
        datePickerView.tintColor = ColorPalette.purple()
        self.inputView = datePickerView
        datePickerValueChanged(datePickerView)
    }
    
    func dateTextField(min:NSDate, max:NSDate) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.minimumDate = min
        datePickerView.maximumDate = max
        datePickerView.datePickerMode = UIDatePickerMode.Date
        datePickerView.addTarget(self, action: Selector("datePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        datePickerView.backgroundColor = UIColor.whiteColor()
        datePickerView.tintColor = ColorPalette.purple()
        self.inputView = datePickerView
        datePickerValueChanged(datePickerView)
    }
    
    func dateTextField(min: NSDate, max:NSDate, date:NSDate) {
        let datePickerView:UIDatePicker = UIDatePicker()
        datePickerView.minimumDate = min
        datePickerView.maximumDate = max
        datePickerView.date = date
        datePickerView.datePickerMode = UIDatePickerMode.Date
        datePickerView.addTarget(self, action: Selector("datePickerValueChanged:"), forControlEvents: UIControlEvents.ValueChanged)
        datePickerView.backgroundColor = UIColor.whiteColor()
        datePickerView.tintColor = ColorPalette.purple()
        self.inputView = datePickerView
        datePickerValueChanged(datePickerView)
    }
    
    func setDoneButton(){
        let toolBarOKButton = UIBarButtonItem(barButtonSystemItem: .Done, target: self, action: Selector("dismissPicker"))
        
        let toolBar = UIToolbar(frame: CGRectMake(0, 0, 0, 50))
        toolBar.backgroundColor = UIColor.whiteColor()
        toolBar.tintColor = ColorPalette.purple()
        toolBar.items = [UIBarButtonItem(barButtonSystemItem: .FlexibleSpace, target: nil, action: nil),toolBarOKButton]
        self.inputAccessoryView = toolBar
    }
    
    func dismissPicker(){
        self.inputView?.removeFromSuperview()
        self.inputAccessoryView?.removeFromSuperview()
        self.resignFirstResponder()
    }
    
    func datePickerValueChanged(sender:UIDatePicker) {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = .MediumStyle
        dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
        self.text = dateFormatter.stringFromDate(sender.date)
    }
    
}

extension UIImageView{
    
    func makeBlur(){
        let darkBlur = UIBlurEffect(style: UIBlurEffectStyle.Dark)
        let blurView = UIVisualEffectView(effect: darkBlur)
        blurView.frame = self.bounds
        self.addSubview(blurView)
    }
    
}

extension UIView{
    
    class func loadFromNibNamed(nibNamed: String, bundle : NSBundle? = nil) -> UIView {
        return (UINib(nibName: nibNamed,bundle: bundle).instantiateWithOwner(nil, options: nil)[0] as? UIView)!
    }
    
    func makeCurve(curve: CGFloat){
        self.layer.cornerRadius = curve
        self.layer.masksToBounds = true
    }
    
    func makeBorder(color: UIColor){
        self.layer.borderWidth = 5
        self.layer.borderColor = color.CGColor
    }
    
    func makeCircular(curve: CGFloat){
        self.layer.cornerRadius = curve
        self.layer.masksToBounds = true
    }
    
    func makeCircle(){
        self.layer.cornerRadius = getRadius()
        //        self.layer.cornerRadius = 10
        self.layer.masksToBounds = true
    }
    
    func getRadius() -> CGFloat{
        return self.frame.size.height/2.0
    }
    
    func whichQuad(v1: UIView, v2: UIView) -> (CGPoint, CGPoint){
        var p1: CGPoint!
        var p2: CGPoint!
        
        let margin: CGFloat = 5.0
        let a: CGFloat = abs(v1.frame.origin.y - v2.frame.origin.y)
        let b: CGFloat = abs(v1.frame.origin.x - v2.frame.origin.x)
        let a2: CGFloat = a*a
        let b2: CGFloat = b*b
        let hipo: CGFloat = sqrt(a2 + b2)
        
        let sen: CGFloat = a/hipo
        let cos: CGFloat = b/hipo
        
        let center1x = v1.center.x
        let center1y = v1.center.y
        let center2x = v2.center.x
        let center2y = v2.center.y
        
        if (center2x >= center1x && center2y < center1y){
            p1 = CGPointMake(center1x + v1.getRadius()*cos + margin, center1y - v1.getRadius()*sen - margin)
            p2 = CGPointMake(center2x - v2.getRadius()*cos - margin, center2y + v2.getRadius()*sen + margin)
        } else if (center2x < center1x && center2y < center1y){
            p1 = CGPointMake(center1x - v1.getRadius()*cos - margin, center1y - v1.getRadius()*sen - margin)
            p2 = CGPointMake(center2x + v2.getRadius()*cos + margin, center2y + v2.getRadius()*sen + margin)
        } else if (center2x < center1x && center2y >= center1y){
            p1 = CGPointMake(center1x - v1.getRadius()*cos - margin, center1y + v1.getRadius()*sen + margin)
            p2 = CGPointMake(center2x + v2.getRadius()*cos + margin, center2y - v2.getRadius()*sen - margin)
        } else if (center2x >= center1x && center2y >= center1y){
            p1 = CGPointMake(center1x + v1.getRadius()*cos + margin, center1y + v1.getRadius()*sen + margin)
            p2 = CGPointMake(center2x - v2.getRadius()*cos - margin, center2y - v2.getRadius()*sen - margin)
        }
        
        return (p1, p2)
    }
    
    func randomCenter(parentView: UIView) -> CGPoint{
        let x: CGFloat = CGFloat(arc4random_uniform(UInt32(Int(parentView.frame.size.width) - Int(self.getRadius())))) + self.getRadius()
        let y: CGFloat = CGFloat(arc4random_uniform(UInt32(Int(parentView.frame.size.height) - Int(self.getRadius())))) + self.getRadius()
        
        return CGPointMake(x, y)
    }
    
    func startWaiting(activityIndicator: UIActivityIndicatorView){
        self.addSubview(activityIndicator)
        
        activityIndicator.frame = self.bounds
        activityIndicator.startAnimating()
    }
    
    func stopWaiting(activityIndicator: UIActivityIndicatorView){
        activityIndicator.stopAnimating()
        activityIndicator.removeFromSuperview()
    }
    
    func startLoadingAnimation(color: UIColor){
        let size = self.frame.size
        let animationFrame = CGRect(origin: CGPointZero, size: size)
        
        let animationView = RPLoadingAnimationView(frame: animationFrame, color: color)
        
        self.addSubview(animationView)
        
        animationView.setupAnimation()
    }
    
    func startRefreshingControl(color: UIColor){
        let size = CGSize(width: self.frame.width*0.3, height: self.frame.height*0.3)
        let animationFrame = CGRect(origin: CGPointZero, size: size)
        
        let animationView = RPLoadingAnimationView(frame: animationFrame, color: color)
        
        self.addSubview(animationView)
        
        animationView.setupAnimation()
    }
    
    func stopLoadingAnimation(){
        for view in self.subviews{
            if (view is RPLoadingAnimationView){
                view.removeFromSuperview()
            }
        }
    }
    
    class func dist(p1 p1: CGPoint, p2: CGPoint) -> CGFloat{
        let xDist: CGFloat = (p2.x - p1.x);
        let yDist: CGFloat = (p2.y - p1.y);
        return sqrt((xDist * xDist) + (yDist * yDist));
    }
}

class File: NSObject {
    
    class func open(path: String, utf8: NSStringEncoding = NSUTF8StringEncoding) -> String? {
        let path = NSBundle.mainBundle().pathForResource(path, ofType: "txt")
        let text = try! String(contentsOfFile: path!, encoding: NSUTF8StringEncoding)
        return text
    }
    
    class func read(linhas: String) -> [String]{
        return linhas.componentsSeparatedByString("\n")
    }
    
}

extension UITableView{
    
    func animateTable(){
        let cells = self.visibleCells
        
        let tableHeight: CGFloat = self.bounds.size.height
        for i in cells {
            let cell: UITableViewCell = i
            cell.transform = CGAffineTransformMakeTranslation(0, tableHeight)
        }
        
        var index = 0
        for a in cells {
            let cell: UITableViewCell = a
            UIView.animateWithDuration(1.5, delay: 0.05 * Double(index), usingSpringWithDamping: 0.8, initialSpringVelocity: 0, options: UIViewAnimationOptions.AllowAnimatedContent, animations: {
                cell.transform = CGAffineTransformMakeTranslation(0, 0);
                }, completion: nil)
            index += 1
        }
    }
    
}

extension NSMutableSet {
    func addObjectAndNotify(object: AnyObject) {
        addObject(object)
        NSNotificationCenter.defaultCenter().postNotificationName(AddObject, object: nil)
    }
}
