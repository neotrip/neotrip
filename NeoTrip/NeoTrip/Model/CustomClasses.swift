//
//  CustomClasses.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 1/8/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

class Queue: NSOperationQueue {
    
    override func addOperation(op: NSOperation) {
        if operationCount > 1 {
            operations[operationCount-1].cancel()
        }
        
        do {
            try super.addOperation(op)
        } catch {}
        
    }

}

protocol TutorialDelegate {
    func prepareTutorial(index: Int)
    func advanceLevel()
    func finishTutorial()
    func updateName()
}

class Tutorial : NSObject {
    var index : Int = 0
    var travel : Travel!
    var delegate : TutorialDelegate!
    
    func advanceLevel () {
        index++
        if (index < 6) {
            travel.name = "Tutorial " + String(index) + ":" + travel.name!.componentsSeparatedByString(":")[1]
            delegate.updateName()
            travel.save()
        }
        else if (index == 6) {
            travel.name = travel.name!.componentsSeparatedByString(": ")[1]
            delegate.updateName()
            travel.save()
        }
        else {
            delegate.finishTutorial()
        }
    }
    
    func initTutorial () {
        delegate.prepareTutorial(index)
    }
}