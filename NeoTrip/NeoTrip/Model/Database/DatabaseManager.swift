//
//  DatabaseManager.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 10/27/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import Foundation

/* Abstract Class to manage database. */
protocol DatabaseManager {
    var status:Connection { get }
    var traveler:Traveler? { get }
    
    func signup(traveler:Traveler, withUser user:User) -> Result
    func login(user:User, retry:Bool) -> (Result, Traveler?)
    func logout()
    
    func getTravelerInfo(forTraveler traveler:Traveler) -> Result
    func inviteTravelerToTravel(traveler:Traveler, travel:Travel) -> Result
    func untakePartTravel(travel:Travel) -> Result
    
    func followTraveler(traveler:Traveler) -> Result
    func unfollowTraveler(traveler:Traveler) -> Result
    
    func getTravels(forTraveler traveler:Traveler) -> (Result)
    func getTravelInfo(forTravel travel:Travel)
    
    func register(travel:Travel) -> Result
    func unregister() -> Result
    
    func search(type:SearchType, options:NSDictionary) -> (Result, NSArray)
    
    func clearCache()
}

let set:NSMutableSet = NSMutableSet()
var databaseManager:DatabaseManager?
