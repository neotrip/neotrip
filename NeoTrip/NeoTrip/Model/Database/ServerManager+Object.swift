//
//  ServerManager+Object.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 1/29/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

extension ServerManager {
    /* Object functions. */
    func edit(var object:Object) {
        object.status = Status.syncing
        if (object.canSync()) {
            let dict = NSMutableDictionary()
            dict["object"] = object.json(true)
            dict["class"] = object.getClass()
            
            lock.lock()
            var bool:Bool
            if (object is Image) {
                dict["type"] = PlannerType.CREATE.rawValue
                bool = send(MessageType.IMAGE, dict: dict, requiresLock: true)
            } else {
                if (object.id == nil) {
                    dict["type"] = PlannerType.CREATE.rawValue
                } else {
                    dict["type"] = PlannerType.EDIT.rawValue
                }
                bool = send(MessageType.PLANNER, dict: dict, requiresLock: true)
            }
            
            if (bool) {
                if (wait() == Result.YES) {
                    if (response == ResponseType.ACKNOWLEDGE && jsonResponse?.objectForKey("class") as! String == "Object") {
                        let objectDict = jsonResponse?.objectForKey("object") as! NSDictionary
                        
                        object.id = objectDict.objectForKey("id") as? String
                        object.synced = dateFromString(objectDict.objectForKey("updatedAt") as? String)
                        object.status = Status.synced
                    } else if (response == ResponseType.ERROR) {
                        object.status = Status.edited
                    }
                }
                response = ResponseType.READY
            }
            lock.unlock()
        } else {
            object.status = Status.edited
        }
    }
    
    func delete(var object:Object) {
        object.status = Status.deleting
        
        if (object.id == nil) {
            object.status = Status.deleted
        } else {
            let dict = NSMutableDictionary()
            dict["object"] = object.json(false)
            dict["class"] = object.getClass()
            dict["type"] = PlannerType.DELETE.rawValue
            
            lock.lock()
            if (send(MessageType.PLANNER, dict: dict, requiresLock: true)) {
                if (wait() == Result.YES) {
                    if (response == ResponseType.ACKNOWLEDGE) {
                        object.status = Status.deleted
                    } else if (response == ResponseType.ERROR) {
                        object.status = Status.delete
                    }
                    response = ResponseType.READY
                }
            }
            lock.unlock()
        }
    }
}