//
//  ServerManager+User.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 1/29/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit
import CoreData

extension ServerManager {
    /* Login functions. */
    
    func signup(traveler:Traveler, withUser user:User) -> Result {
        disconnect()
        connect()
        
        /* Establishing connection. */
        var count = 0
        while (self.status == Connection.CONNECTING) {
            if (count > 5) {
                disconnect()
                break
            } else {
                sleep(1)
                count++
            }
        }
        
        var result:Result = Result.NO
        if (self.status == Connection.CONNECTED) {
            let dict = NSMutableDictionary()
            dict["type"] = UserType.SIGNUP.rawValue
            dict["user"] = user.json(true)
            dict["traveler"] = traveler.json(true)
            dict["version"] = serverVersion
            
            let data:NSMutableData = NSMutableData()
            let messageType = (MessageType.USER.rawValue + "\n").dataUsingEncoding(encoding)!
            let dataToSend = (self.dictionaryToString(dict) + "\n").dataUsingEncoding(encoding)!
            
            lock.lock()
            data.appendBytes(UnsafePointer<UInt8>(self.start.bytes), length: self.start.length)
            data.appendBytes(UnsafePointer<UInt8>(messageType.bytes), length: messageType.length)
            data.appendBytes(UnsafePointer<UInt8>(dataToSend.bytes), length: dataToSend.length)
            data.appendBytes(UnsafePointer<UInt8>(self.end.bytes), length: self.end.length)
            self.outputstream?.write(UnsafePointer<UInt8>(data.bytes), maxLength: data.length)
            
            result = wait()
            if (result == Result.YES) {
                
                if (response == ResponseType.ACKNOWLEDGE) {
                    
                    let travelerDict:NSDictionary = jsonResponse?.objectForKey("object") as! NSDictionary
                    traveler.synced = dateFromString(travelerDict.objectForKey("updatedAt") as? String)
                    traveler.status = Status.synced
                    self.traveler = traveler
                    
                    user.logged = true
                    //CoredataManager.sharedInstance.saveContext()
                    
                    loggedUser = user
                    serverQueue.addOperation(push(edit: edit, delete: delete))
                    NSNotificationCenter.defaultCenter().addObserver(self, selector: "startPush", name: AddObject, object: nil)
                    
                    status = Connection.LOGGED
                    response = ResponseType.READY
                } else if (response == ResponseType.ERROR) {
                    let codeError = jsonResponse!.objectForKey("Code") as! NSNumber
                    if codeError == 0 {
                        result = Result.DEPRECATED
                    } else if codeError == 19 {
                        result = Result.NO
                    } else {
                        result = Result.ERROR
                    }
                    
                    disconnect()
                } else {
                    result = Result.ERROR
                    disconnect()
                }
            
            }
            
            lock.unlock()
        } else {
            disconnect()
            result = Result.UNCONNECTED
        }
        
        return result
    }
    
    func login(user:User, retry:Bool) ->  (Result, Traveler?) {
        disconnect()
        connect()
        
        /* Establishing connection. */
        if (retry) {
            while (self.status == Connection.CONNECTING) {
                sleep(5)
            }
        } else {
            var count = 0
            while (self.status == Connection.CONNECTING) {
                if (count > 5) {
                    disconnect()
                    break
                } else {
                    sleep(1)
                    count++
                }
            }
        }
        
        var result:Result = Result.NO
        if (self.status == Connection.CONNECTED) {
            let dict = NSMutableDictionary()
            dict["type"] = UserType.LOGIN.rawValue
            dict["user"] = user.json(true)
            dict["version"] = serverVersion
            
            let data:NSMutableData = NSMutableData()
            let messageType = (MessageType.USER.rawValue + "\n").dataUsingEncoding(encoding)!
            let dataToSend = (self.dictionaryToString(dict) + "\n").dataUsingEncoding(encoding)!
            
            lock.lock()
            
            data.appendBytes(UnsafePointer<UInt8>(self.start.bytes), length: self.start.length)
            data.appendBytes(UnsafePointer<UInt8>(messageType.bytes), length: messageType.length)
            data.appendBytes(UnsafePointer<UInt8>(dataToSend.bytes), length: dataToSend.length)
            data.appendBytes(UnsafePointer<UInt8>(self.end.bytes), length: self.end.length)
            self.outputstream?.write(UnsafePointer<UInt8>(data.bytes), maxLength: data.length)
            
            result = wait()
            if (result == Result.YES) {
                
                if (response == ResponseType.ACKNOWLEDGE) {
                    
                    let travelerDict:NSDictionary = jsonResponse?.objectForKey("object") as! NSDictionary
                    if (traveler == nil) {
                        traveler = Traveler()
                    }
                    traveler!.setInfo(travelerDict, complete: true)
                    traveler!.status = Status.synced
                    
                    loggedUser = user
                    loggedUser!.logged = true
                    //CoredataManager.sharedInstance.saveContext()
                    
                    serverQueue.addOperation(push(edit: self.edit, delete: self.delete))
                    NSNotificationCenter.defaultCenter().addObserver(self, selector: "startPush", name: AddObject, object: nil)
                    
                    status = Connection.LOGGED
                    response = ResponseType.READY
                } else if (response == ResponseType.ERROR) {
                    let codeError = jsonResponse!.objectForKey("Code") as! NSNumber
                    if codeError == 0 {
                        result = Result.DEPRECATED
                    } else if codeError == 19 {
                        result = Result.NO
                    } else {
                        result = Result.ERROR
                    }
                    
                    disconnect()
                } else {
                    result = Result.ERROR
                    disconnect()
                }
            }
            lock.unlock()
            
        } else {
            result = Result.UNCONNECTED
            disconnect()
        }
        
        return (result, traveler)
        
    }
    
    func logout() {
        disconnect()
        loggedUser?.logged = false
        loggedUser?.sync()
        loggedUser = nil
        
        var fetchRequest:NSFetchRequest
        var deleteRequest:NSBatchDeleteRequest
        
        do {
            fetchRequest = NSFetchRequest(entityName: "User")
            deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            try CoredataManager.sharedInstance.persistentStoreCoordinator.executeRequest(deleteRequest, withContext: CoredataManager.sharedInstance.managedObjectContext)
            
            fetchRequest = NSFetchRequest(entityName: "Traveler")
            deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
            try CoredataManager.sharedInstance.persistentStoreCoordinator.executeRequest(deleteRequest, withContext: CoredataManager.sharedInstance.managedObjectContext)
        } catch {
        }
        
    }
    
}
