//
//  ServerManager+Invite.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 1/29/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

extension ServerManager {
    
    func inviteTravelerToTravel(traveler:Traveler, travel:Travel) -> Result {
        var result:Result = Result.ERROR
        
        let dict = NSMutableDictionary()
        dict["traveler"] = traveler.json(false)
        dict["travel"] = travel.json(false)
        dict["type"] = PlannerType.INVITE.rawValue
        
        lock.lock()
        if (send(MessageType.PLANNER, dict: dict, requiresLock: true)) {
            result = wait()
            if (result == Result.YES) {
                if (response == ResponseType.ACKNOWLEDGE && jsonResponse?.objectForKey("class") as! String == "Response") {
                    result = Result(rawValue: jsonResponse?.objectForKey("result") as! Int)!
                    
                    
                    dispatch_sync(dispatch_get_main_queue()) {
                        switch (result) {
                        case .YES:
                            travel.addTravelerObject(traveler)
                            break
                        case .WAIT:
                            travel.addWaitingTraveler(traveler)
                            break
                        default:
                            break
                        }
                    }
                    
                    //CoredataManager.sharedInstance.saveContext()
                } else if (response == ResponseType.ERROR) {
                    result = Result.ERROR
                }
                response = ResponseType.READY
            }
        }
        
        lock.unlock()
        return result
    }
    
    func acceptInvite(travel:Travel) -> Result {
        var result:Result = Result.ERROR
        
        let dict = NSMutableDictionary()
        dict["travel"] = travel.json(false)
        dict["type"] = PlannerType.ACCEPT.rawValue
        
        lock.lock()
        if (send(MessageType.PLANNER, dict: dict, requiresLock: false)) {
            result = wait()
            if (result == Result.YES) {
                if (response == ResponseType.ACKNOWLEDGE && jsonResponse?.objectForKey("class") as! String == "Response") {
                    result = Result(rawValue: jsonResponse?.objectForKey("result") as! Int)!
                    
                    dispatch_sync(dispatch_get_main_queue()) {
                        switch (result) {
                        case .YES:
                            travel.addTravelerObject(Traveler.traveler(self.loggedUser!.id!))
                            break
                        default:
                            break
                        }
                    }
                    
                    //CoredataManager.sharedInstance.saveContext()
                } else if (response == ResponseType.ERROR) {
                    result = Result.ERROR
                }
                
                response = ResponseType.READY
            }
            
        }
        
        lock.unlock()
        return result
        
    }
    
    func untakePartTravel(travel:Travel) -> Result {
        var result:Result = Result.ERROR
        
        let dict = NSMutableDictionary()
        dict["travel"] = travel.json(false)
        dict["type"] = PlannerType.UNTAKEPART.rawValue
        
        lock.lock()
        if (send(MessageType.PLANNER, dict: dict, requiresLock: false)) {
            result = wait()
            if (result == Result.YES) {
                if (response == ResponseType.ACKNOWLEDGE && jsonResponse?.objectForKey("class") as! String == "Response") {
                    
                    result = Result(rawValue: jsonResponse?.objectForKey("result") as! Int)!
                    
                    dispatch_sync(dispatch_get_main_queue()) {
                        switch (result) {
                        case .YES:
                            travel.removeTravelerObject(Traveler.traveler(self.loggedUser!.id!))
                            break
                        default:
                            break
                        }
                    }
                    
                    //CoredataManager.sharedInstance.saveContext()
                } else if (response == ResponseType.ERROR) {
                    result = Result.ERROR
                }
                response = ResponseType.READY
            }
        }
        
        lock.unlock()
        return result
    }
    
}