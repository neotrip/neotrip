//
//  ServerManager+Search.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 1/29/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

extension ServerManager {
    /* Get travel functions. */
    
    func getTravels(forTraveler traveler:Traveler) -> Result {
        var result:Result = Result.ERROR
        
        let dict = NSMutableDictionary()
        dict["traveler"] = traveler.json(false)
        dict["type"] = SearchType.TRAVELSOWNED.rawValue
        
        lock.lock()
        if (send(MessageType.SEARCH, dict: dict, requiresLock: false)) {
            result = wait()
            
            if (result == Result.YES) {
                if (response == ResponseType.ACKNOWLEDGE && (jsonResponse?.objectForKey("class")) as! String == "Travel") {
                    
                    dispatch_sync(dispatch_get_main_queue()) {
                        let ownTravelDict = self.jsonResponse?.objectForKey("own") as! [NSDictionary]
                        for object in ownTravelDict {
                            let travel:Travel = Travel.travel(object.objectForKey("id") as! String)
                            travel.setInfo(object)
                            travel.travelerOwner = traveler
                        }
                        
                    }
                    dispatch_sync(dispatch_get_main_queue()) {
                        let takeTravelDict = self.jsonResponse?.objectForKey("part") as! [NSDictionary]
                        for object in takeTravelDict {
                            let travel:Travel = Travel.travel(object.objectForKey("id") as! String)
                            travel.setInfo(object)
                            travel.addTravelerObject(traveler)
                        }
                    }
                    //CoredataManager.sharedInstance.saveContext()
                } else if (response == ResponseType.ERROR) {
                    result = Result.ERROR
                }
                
                response = ResponseType.READY
            }
            
        }
        
        lock.unlock()
        return result
    }
    
    func getTravelInfo(forTravel travel:Travel) {
        if (travel.id == nil) { return }
        
        let dict = NSMutableDictionary()
        dict["travel"] = travel.json(false)
        dict["type"] = SearchType.TRAVELINFO.rawValue
        
        lock.lock()
        if (send(MessageType.SEARCH, dict: dict, requiresLock: true)) {
            var result:Result = wait()
            if (result == Result.YES) {
                if (response == ResponseType.ACKNOWLEDGE && (jsonResponse?.objectForKey("class")) as! String == "Travel") {
                    
                    let dict = jsonResponse?.copy() as? NSDictionary
                    dispatch_sync(dispatch_get_main_queue()) {
                        let dictTravelers = dict?.objectForKey("travelers") as! NSDictionary
                        if let dictTakingPart = dictTravelers.objectForKey("takingpart") as? [NSDictionary] {
                            
                            for dict in dictTakingPart {
                                let t = Traveler.traveler(dict.objectForKey("id") as! String)
                                t.setInfo(dict, complete: false)
                                travel.addTravelerObject(t)
                                if (dict.objectForKey("follows") as! Bool) {
                                    self.traveler!.followTraveler(t)
                                }
                            }
                            
                        }
                    }
                    dispatch_sync(dispatch_get_main_queue()) {
                        let dictPlaces = dict?.objectForKey("places") as! [NSDictionary]
                        if dictPlaces.count > 0 {
                            
                            for dict in dictPlaces {
                                if (dict.objectForKey("deleted") as! Bool) {
                                    Place.remove(dict.objectForKey("id") as! String)
                                } else {
                                    let category = PlaceType(rawValue: dict.objectForKey("category") as! Int)!
                                    let place = Place.place(dict.objectForKey("id") as! String, category: category)
                                    place.setInfo(dict)
                                    place.itinerary = travel.itinerary
                                    place.status = Status.synced
                                }
                                
                            }
                            
                        }
                    }
                    dispatch_sync(dispatch_get_main_queue()) {
                        let dictTransport = dict?.objectForKey("transports") as! [NSDictionary]
                        if dictTransport.count > 0 {
                            
                            for dict in dictTransport {
                                if (dict.objectForKey("deleted") as! Bool) {
                                    Transport.remove(dict.objectForKey("id") as! String)
                                } else {
                                    let transport = Transport.transport(dict.objectForKey("id") as! String)
                                    transport.setInfo(dict)
                                    transport.itinerary = travel.itinerary
                                    transport.status = Status.synced
                                }
                            }
                            
                        }
                    }
                    
                    travel.infoSynced = dateFromString(jsonResponse?.objectForKey("updatedAt") as? String)
                    //CoredataManager.sharedInstance.saveContext()
                    
                    NSNotificationCenter.defaultCenter().postNotificationName(TravelRefreshed, object: nil)
                    
                    
                } else if (response == ResponseType.ERROR) {
                    result = Result.ERROR
                }
                
                response = ResponseType.READY
            }
            
        }
        
        lock.unlock()
        
    }
    
    func search(type: SearchType, options:NSDictionary) -> (Result, NSArray) {
        var result:Result = Result.ERROR
        var objects:[AnyObject] = [AnyObject]()
        
        let dict = NSMutableDictionary()
        dict["type"] = type.rawValue
        dict["queries"] = options
        
        lock.lock()
        if (send(MessageType.SEARCH, dict: dict, requiresLock: false)) {
            result = wait()
            if (result == Result.YES) {
                if (response == ResponseType.ACKNOWLEDGE && jsonResponse?.objectForKey("class") as! String == "Response") {
                    let dictObject = jsonResponse?.objectForKey("objects") as! [NSDictionary]
                    if dictObject.count > 0 {
                        
                        switch (type) {
                        case .PLACE:
                            /*
                            for dict in dictObject {
                                let country = Country.country(dict.objectForKey("id") as! String)
                                country.setInfo(dict)
                            }*/
                            
                            break
                        case .TRAVELERS:
                            
                            for dict in dictObject {
                                let t = Traveler.traveler(dict.objectForKey("id") as! String)
                                t.setInfo(dict, complete: false)
                                if (dict.objectForKey("follows") as! Bool) {
                                    traveler!.followTraveler(t)
                                }
                            }
                            
                            break
                        default:
                            break
                        }
                        
                        //CoredataManager.sharedInstance.saveContext()
                    }
                    
                } else if (response == ResponseType.ERROR) {
                    result = Result.ERROR
                }
                
                switch (type) {
                case .CITY:
                    /*
                    objects = City.search(options["query"] as! String, queries: options)
                    */
                    break
                case .PLACE:
                    objects = Place.search(options["query"] as! String, queries: options)
                    break
                default:
                    objects = Traveler.search(options["query"] as! String)
                    break
                }
                
                response = ResponseType.READY
            }
            
        }
        
        lock.unlock()
        return (result, objects)
        
    }
    
    func getTravelerInfo(forTraveler traveler:Traveler) -> Result {
        if (traveler.id == nil) { return Result.NO }
        var result:Result = Result.ERROR
        
        let dict = NSMutableDictionary()
        dict["type"] = SearchType.TRAVELERINFO.rawValue
        dict["user"] = traveler.json(false)
        
        lock.lock()
        if (send(MessageType.SEARCH, dict: dict, requiresLock: false)) {
            result = wait()
            if (result == Result.YES) {
                if (response == ResponseType.ACKNOWLEDGE && jsonResponse?.objectForKey("class") as! String == "Response") {
                    
                    let travelerDict:NSDictionary = jsonResponse?.objectForKey("object") as! NSDictionary
                    traveler.setInfo(travelerDict, complete: true)
                    traveler.status = Status.synced
                    
                    if let follow = travelerDict.objectForKey("follow") as? [NSDictionary] {
                        for dict in follow {
                            let myone = Traveler.traveler(dict.objectForKey("id") as! String)
                            myone.setInfo(dict, complete: false)
                            
                            let follows = dict.objectForKey("follows") as! Int
                            if (follows < 0) {
                                myone.followTraveler(traveler)
                            }
                            if (abs(follows) == 1) {
                                traveler.followTraveler(myone)
                            }
                        }
                    }
                } else if (response == ResponseType.ERROR) {
                    result = Result.NO
                }
                response = ResponseType.READY
            }
        }
        
        lock.unlock()
        return result
    }
    
}