//
//  ServerManager+Connection.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 1/29/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

extension ServerManager : NSStreamDelegate {
    
    func disconnect() {
//        inputstream?.removeFromRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
//        outputstream?.removeFromRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
        
        
        status = Connection.DISCONNECTED
        response = ResponseType.STOPPED
  
        serverQueue.cancelAllOperations()
        NSNotificationCenter.defaultCenter().removeObserver(self)
        
        inputstream?.close()
        outputstream?.close()
    }
    
    func stream(aStream: NSStream, handleEvent eventCode: NSStreamEvent) {
        
        switch (eventCode){
            
        case NSStreamEvent.ErrorOccurred:
            NSLog("ErrorOccurred")
            serverQueue.cancelAllOperations()
            NSNotificationCenter.defaultCenter().removeObserver(self)
            
            status = Connection.DISCONNECTED
            response = ResponseType.STOPPED
            
            if (aStream == inputstream) {
                let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(10 * Double(NSEC_PER_SEC)))
                dispatch_after(delayTime, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                    NSLog("Trying to establish connection...")
                    if (self.loggedUser != nil) {
                        self.login(self.loggedUser!, retry: true)
                    }
                }
            }
            
            break
        case NSStreamEvent.OpenCompleted:
            NSLog("OpenCompleted")
            
            status = Connection.CONNECTED
            response = ResponseType.STOPPED
            break
        case NSStreamEvent.HasBytesAvailable:
            //NSLog("HasBytesAvaible")
            var buffer = [UInt8](count: 16384, repeatedValue: 0)
            if ( aStream == inputstream){
                let output = NSMutableString()
                
                while (inputstream!.hasBytesAvailable){
                    
                    let len = inputstream!.read(&buffer, maxLength: buffer.count-1)
                    if(len > 0){
                        
                        var current = NSString(bytes: &buffer, length: buffer.count-1, encoding: encoding)
                        if buffer[0] == 10 {
                            return
                        }
                        current = current!.substringToIndex(len)
                        output.appendString(current!.description)
                        
                    }
                    
                }
                
                if ((output.hasPrefix(ResponseType.ACKNOWLEDGE.rawValue))) {
                    do {
                        let transform = "Any-Hex/Java"
                        CFStringTransform(output, nil, transform as NSString, true)
                        
                        jsonResponse = try NSJSONSerialization.JSONObjectWithData(output.substringFromIndex(ResponseType.ACKNOWLEDGE.rawValue.lengthOfBytesUsingEncoding(encoding)).stringByReplacingOccurrencesOfString("\\", withString: "").dataUsingEncoding(encoding)!, options: [NSJSONReadingOptions.MutableContainers]) as? NSDictionary
                        
                        response = ResponseType.ACKNOWLEDGE
                    } catch let error as NSError {
                        print(error.description)
                        jsonResponse = nil
                        response = ResponseType.ERROR
                    }
                } else if (output.hasPrefix(ResponseType.PUSH.rawValue)) {
                    do {
                        let transform = "Any-Hex/Java"
                        CFStringTransform(output, nil, transform as NSString, true)
                        
                        let pushObject:NSDictionary = try NSJSONSerialization.JSONObjectWithData(output.substringFromIndex(ResponseType.PUSH.rawValue.lengthOfBytesUsingEncoding(encoding)).stringByReplacingOccurrencesOfString("\\", withString: "").dataUsingEncoding(encoding)!, options: [NSJSONReadingOptions.MutableContainers]) as! NSDictionary
                        
                        pushObjectReceived(pushObject)
                        //response = ResponseType.ACKNOWLEDGE
                    } catch {
                    }
                } else if (output.hasPrefix(ResponseType.READY.rawValue)) {
                    response = ResponseType.AUTHORIZED
                } else if (output.hasPrefix(ResponseType.ERROR.rawValue)) {
                    disconnect()
                    do {
                        jsonResponse = try NSJSONSerialization.JSONObjectWithData(output.substringFromIndex(ResponseType.ERROR.rawValue.lengthOfBytesUsingEncoding(encoding)).dataUsingEncoding(NSUnicodeStringEncoding)!, options: [NSJSONReadingOptions.MutableContainers]) as? NSDictionary
                    } catch let error as NSError {
                        print(error.description)
                        jsonResponse = nil
                    }
                    response = ResponseType.ERROR
                }
                
                
            }
            break
        case NSStreamEvent.EndEncountered:
            NSLog("EndEncountered")
            serverQueue.cancelAllOperations()
            NSNotificationCenter.defaultCenter().removeObserver(self)
            
            status = Connection.DISCONNECTED
            response = ResponseType.STOPPED
            
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(10 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                NSLog("Trying to establish connection...")
                if (self.loggedUser != nil) {
                    self.login(self.loggedUser!, retry: true)
                }
            }
            
            break
        case NSStreamEvent.None:
            NSLog("None")
            break
        case NSStreamEvent.HasSpaceAvailable:
            //NSLog("HasSpaceAvailable")
            break
        default:
            NSLog("Unknown")
            break
        }
    }
    
    func send(type:MessageType, dict:NSDictionary?, requiresLock:Bool) -> Bool {
        if status != Connection.LOGGED || response != ResponseType.READY {
            return false
        }
        
        response = ResponseType.WAITING
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            /*
            if (requiresLock) {
                self.requestLock()
                while self.response != ResponseType.AUTHORIZED {
                    sleep(1)
                }
            }*/
            
            let data:NSMutableData = NSMutableData()
            data.appendBytes(UnsafePointer<UInt8>(self.start.bytes), length: self.start.length)
            
            let messageType = (type.rawValue + "\n").dataUsingEncoding(encoding)!
            data.appendBytes(UnsafePointer<UInt8>(messageType.bytes), length: messageType.length)
            
            if dict != nil {
                let dataToSend = (self.dictionaryToString(dict!) + "\n").dataUsingEncoding(encoding)!
                data.appendBytes(UnsafePointer<UInt8>(dataToSend.bytes), length: dataToSend.length)
            }
            
            data.appendBytes(UnsafePointer<UInt8>(self.end.bytes), length: self.end.length)
            
            self.outputstream?.write(UnsafePointer<UInt8>(data.bytes), maxLength: data.length)
        }
        
        return true
    }
    
    func connect() {
        status = Connection.CONNECTING
        dispatch_async(dispatch_get_main_queue()) {
            var readstream : Unmanaged<CFReadStream>?
            var writestream : Unmanaged<CFWriteStream>?
            
            let sslSettings = [
                NSString(format: kCFStreamPropertySocketSecurityLevel): kCFStreamSocketSecurityLevelNegotiatedSSL,
                NSString(format: kCFStreamSSLValidatesCertificateChain): kCFBooleanFalse
            ]
            
            CFStreamCreatePairWithSocketToHost(kCFAllocatorDefault, self.host, self.port, &readstream, &writestream)
            
            self.inputstream = readstream!.takeRetainedValue()
            self.outputstream = writestream!.takeRetainedValue()
            
            CFReadStreamSetProperty(self.inputstream, kCFStreamPropertySSLSettings, sslSettings)
            CFWriteStreamSetProperty(self.outputstream, kCFStreamPropertySSLSettings, sslSettings)
            
            self.inputstream!.delegate = self
            self.outputstream!.delegate = self
            
            self.inputstream!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
            self.outputstream!.scheduleInRunLoop(NSRunLoop.currentRunLoop(), forMode: NSDefaultRunLoopMode)
            
            self.inputstream!.open()
            self.outputstream!.open()
        }
    }
    
    func dictionaryToString(dictionary:NSDictionary) -> String {
        var int: Int
        do {
            int = try NSPropertyListSerialization.dataWithPropertyList(dictionary, format: .BinaryFormat_v1_0, options: 0).length
        } catch {
            int = 110000
        }
        
        var buffer = [UInt8](count: Int(Double(int) * 1.2), repeatedValue: 0)
        let output:NSOutputStream = NSOutputStream(toBuffer: &buffer, capacity: buffer.count)
        
        output.open()
        
        NSJSONSerialization.writeJSONObject(
            dictionary,
            toStream: output,
            options: NSJSONWritingOptions.PrettyPrinted,
            error: nil)
        output.close()
        
        let json = NSString(bytes: &buffer, length: buffer.count, encoding: encoding)
        return (json?.substringToIndex((json!.rangeOfString("\0")).location))!
    }
    
}