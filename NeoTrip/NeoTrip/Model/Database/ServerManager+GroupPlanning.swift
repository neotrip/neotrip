//
//  ServerManager+GroupPlanning.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 1/29/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

extension ServerManager {
    
    func register(travel:Travel) -> Result {
        var result:Result = Result.ERROR
        
        let dict = NSMutableDictionary()
        dict["travel"] = travel.json(false)
        dict["type"] = PlannerType.REGISTER.rawValue
        
        lock.lock()
        if (send(MessageType.PLANNER, dict: dict, requiresLock: false)) {
            result = wait()
            if (result == Result.YES) {
                if (response == ResponseType.ACKNOWLEDGE) {
                    result = Result(rawValue: jsonResponse?.objectForKey("result") as! Int)!
                } else if (response == ResponseType.ERROR) {
                    result = Result.ERROR
                }
                response = ResponseType.READY
            }
        }
        
        lock.unlock()
        return result
    }
    
    func unregister() -> Result {
        var result:Result = Result.ERROR
        
        let dict = NSMutableDictionary()
        dict["type"] = PlannerType.UNREGISTER.rawValue
        
        lock.lock()
        if (send(MessageType.PLANNER, dict: dict, requiresLock: false)) {
            result = wait()
            
            if (result == Result.YES) {
                if (response == ResponseType.ACKNOWLEDGE) {
                    // nothing, yeah, nothing
                } else if (response == ResponseType.ERROR) {
                    result = Result.ERROR
                }
                response = ResponseType.READY
            }
        }
        
        lock.unlock()
        return result
    }
    
    func pushObjectReceived(pushObject:NSDictionary) {
        let classObject = pushObject.objectForKey("class") as! String
        let aaa = PlannerType(rawValue: pushObject.objectForKey("push") as! Int)
        
        lock.lock()
        if (aaa == PlannerType.DELETE) {
            switch classObject {
            case "Place":
                
                
                    let place = Place.place(pushObject.objectForKey("id") as! String, category: PlaceType.NONE)
                    place.status = Status.deleted
                    place.itinerary?.travel?.synced = dateFromString(pushObject.objectForKey("updatedAt") as? String)
                    CoredataManager.sharedInstance.managedObjectContext.deleteObject(place)
                    
                    NSNotificationCenter.defaultCenter().postNotificationName(TravelRefreshed, object: nil)
                
                break
                
            case "Transport":
                
                    let transport = Transport.transport(pushObject.objectForKey("id") as! String)
                    transport.status = Status.deleted
                    transport.itinerary?.travel?.synced = dateFromString(pushObject.objectForKey("updatedAt") as? String)
                    CoredataManager.sharedInstance.managedObjectContext.deleteObject(transport)
                    
                    NSNotificationCenter.defaultCenter().postNotificationName(TravelRefreshed, object: nil)
                
                break
                
            default:
                break
                
            }
        } else {
            switch classObject {
            case "Place":
                
                    let category = PlaceType(rawValue: pushObject.objectForKey("category") as! Int)!
                    let place = Place.place(pushObject.objectForKey("id") as! String, category: category)
                    place.setInfo(pushObject)
                    place.itinerary?.travel?.synced = dateFromString(pushObject.objectForKey("updatedAt") as? String)
                    let itinerary = Itinerary.itinerary(pushObject.objectForKey("travelId") as! String)
                    place.itinerary = itinerary
                    
                    NSNotificationCenter.defaultCenter().postNotificationName(TravelRefreshed, object: nil)
                
                break
                
            case "Transport":
                
                    let transport = Transport.transport(pushObject.objectForKey("id") as! String)
                    transport.setInfo(pushObject)
                    transport.itinerary?.travel?.synced = dateFromString(pushObject.objectForKey("updatedAt") as? String)
                    let itinerary = Itinerary.itinerary(pushObject.objectForKey("travelId") as! String)
                    transport.itinerary = itinerary
                    
                    NSNotificationCenter.defaultCenter().postNotificationName(TravelRefreshed, object: nil)
                
                break
                
            default:
                break
                
            }
        }
        lock.unlock()
        
        
    }
    
}
