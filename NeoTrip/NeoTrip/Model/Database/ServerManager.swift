//
//  ServerManager.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 12/2/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//


import UIKit
import CoreData

let lht:CFString = "172.16.1.241"
let mko: CFString = "187.64.229.220"
let bpd: CFString = "172.16.1.241"
let neotrip: CFString = "www.neotripapp.com"

class ServerManager: NSObject, DatabaseManager {
    let lock:NSRecursiveLock = NSRecursiveLock()
    
    var host: CFString = neotrip
    let port : UInt32 = 2222
    
    var loggedUser:User? 
    var traveler:Traveler?
    
    let managedObjectContext = CoredataManager.sharedInstance.managedObjectContext
    let serverVersion = 0.1
    
    var status:Connection = Connection.DISCONNECTED {
        didSet {
            NSNotificationCenter.defaultCenter().postNotificationName(ConnectionChanged, object: status.rawValue)
        }
    }
    var response:ResponseType = ResponseType.STOPPED
    
    var jsonResponse:NSDictionary?
    
    var inputstream:NSInputStream?
    var outputstream:NSOutputStream?
    
    let start = "START\n".dataUsingEncoding(encoding)!
    let end = "END\n".dataUsingEncoding(encoding)!
    let flush = "\n\r".dataUsingEncoding(encoding)!
    
    override init() {
        super.init()
    }
    
    convenience init(user:User) {
        self.init()
        loggedUser = user
        traveler = Traveler.traveler(user.id!)
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            let (result, _) = self.login(user, retry: true)
            if (result == Result.NO) {
                print("usuário não existe")
            }
        }
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func wait() -> Result {
        /* Wait for connection and response.  */
        while (
            (status == Connection.CONNECTED || status == Connection.LOGGED) &&
                !(response == ResponseType.READY
                    || response == ResponseType.ACKNOWLEDGE
                    || response == ResponseType.ERROR)
            ) {
                sleep(1)
        }
        
        if (response == ResponseType.ACKNOWLEDGE || response == ResponseType.ERROR) {
            return Result.YES
        } else {
            if (status != Connection.CONNECTED || status != Connection.LOGGED) {
                return Result.UNCONNECTED
            }
            return Result.ERROR
        }
    }
    
    lazy var serverQueue:NSOperationQueue = {
        var queue = NSOperationQueue()
        queue.name = "Server Push"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    
    func startPush() {
        //if serverQueue.operationCount == 0 {
            serverQueue.addOperation(push(edit: edit, delete: delete))
        //}
    }
    
    class push : NSOperation {
        
        let edit:((Object)->())
        let delete:((Object)->())
        
        init(edit:((Object)->()), delete:((Object)->())) {
            self.edit = edit
            self.delete = delete
        }
        
        override func main() {
            for current in set {
                if (current is Object && !self.cancelled) {
                    let object = current as! Object
                    switch (object.status) {
                    case Status.synced:
                        set.removeObject(current)
                    case Status.edited:
                        self.edit(object)
                    case Status.delete:
                        self.delete(object)
                    case Status.deleted:
                        object.setNil()
                        set.removeObject(current)
                        CoredataManager.sharedInstance.managedObjectContext.deleteObject(current as! NSManagedObject)
                        //CoredataManager.sharedInstance.saveContext()
                        break
                    default:
                        break
                    }
                }
            }
        }
    }
    
    func clearCache() {
        var fetchRequest:NSFetchRequest
        var deleteRequest:NSBatchDeleteRequest
        
        do {
            if traveler != nil {
                let array = NSMutableSet()
                array.addObject(traveler!.id!)
                for t in traveler!.travels! {
                    array.addObject((t as! Travel).travelerOwner!.id!)
                }
                
                fetchRequest = NSFetchRequest(entityName: "Traveler")
                fetchRequest.predicate = NSPredicate(format: "NOT (id IN %@)", array)
                
                deleteRequest = NSBatchDeleteRequest(fetchRequest: fetchRequest)
                try CoredataManager.sharedInstance.persistentStoreCoordinator.executeRequest(deleteRequest, withContext: CoredataManager.sharedInstance.managedObjectContext)
                
            }
        } catch {
        }
    }
    
    func newManagedObjectContext() -> NSManagedObjectContext {
        let objectContext = NSManagedObjectContext(concurrencyType: .PrivateQueueConcurrencyType)
        objectContext.persistentStoreCoordinator = CoredataManager.sharedInstance.persistentStoreCoordinator
        objectContext.undoManager = nil
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "merge:", name: NSManagedObjectContextDidSaveNotification, object: objectContext)
        
        return objectContext
        
    }
    
    func merge(notification:NSNotification) {
        managedObjectContext.performSelectorOnMainThread("mergeChangesFromContextDidSaveNotification:", withObject: notification, waitUntilDone: true)
    }
    
}

