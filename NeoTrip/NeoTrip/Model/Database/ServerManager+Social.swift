//
//  ServerManager+Social.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 1/29/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

extension ServerManager {
    
    func followTraveler(traveler:Traveler) -> Result {
        var result:Result = Result.ERROR
        
        let dict = NSMutableDictionary()
        dict["type"] = SocialType.FOLLOW.rawValue
        dict["user"] = traveler.json(false)
        
        lock.lock()
        if (send(MessageType.SOCIAL, dict: dict, requiresLock: false)) {
            result = wait()
            if (result == Result.YES) {
                if (response == ResponseType.ACKNOWLEDGE && jsonResponse?.objectForKey("class") as! String == "Response") {
                    result = Result(rawValue: jsonResponse?.objectForKey("result") as! Int)!
                    
                    dispatch_sync(dispatch_get_main_queue()) {
                        Traveler.traveler(self.loggedUser!.id!).followTraveler(traveler)
                    }
                    //CoredataManager.sharedInstance.saveContext()
                } else {
                    result = Result.NO
                }
                response = ResponseType.READY
            }
            
        }
        
        lock.unlock()
        return result
    }
    
    
    func unfollowTraveler(traveler:Traveler) -> Result {
        var result:Result = Result.ERROR
        
        let dict = NSMutableDictionary()
        dict["type"] = SocialType.UNFOLLOW.rawValue
        dict["user"] = traveler.json(false)
        
        lock.lock()
        if (send(MessageType.SOCIAL, dict: dict, requiresLock: false)) {
            result = wait()
            if (result == Result.YES) {
                if (response == ResponseType.ACKNOWLEDGE && jsonResponse?.objectForKey("class") as! String == "Response") {
                    
                    result = Result(rawValue: jsonResponse?.objectForKey("result") as! Int)!
                    dispatch_sync(dispatch_get_main_queue()) {
                        Traveler.traveler(self.loggedUser!.id!).unfollowTraveler(traveler)
                    }
                    //CoredataManager.sharedInstance.saveContext()
                }
            } else {
                result = Result.NO
            }
            response = ResponseType.READY
        }
        
        lock.unlock()
        return result
    }
    
}
