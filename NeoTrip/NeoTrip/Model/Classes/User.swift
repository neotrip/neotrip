//
//  User.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 10/26/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import Foundation
import CoreData


class User: NSManagedObject {
    
    @NSManaged var id: String?
    @NSManaged var password: String?
    @NSManaged var logged: NSNumber?
    
    @NSManaged var status: Status
    @NSManaged var synced: NSDate?
    
}

extension User : Object {
    
    static func user() -> User? {
        let managedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        
        let request = NSFetchRequest()
        request.entity = NSEntityDescription.entityForName("User", inManagedObjectContext: managedObjectContext)
        request.predicate = NSPredicate(format: "logged == 1")
        
        do {
            let list = try managedObjectContext.executeFetchRequest(request) as! [User]
            if list.count > 0 {
                return list[0]
            }
        } catch {}
        
        return nil
    }
    
    convenience init () {
        let managedObjectContext: NSManagedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        let entityDescription = NSEntityDescription.entityForName("User", inManagedObjectContext: managedObjectContext)
        self.init(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext)
    }
    
    func save() {
        
    }
    
    func sync() {
        CoredataManager.sharedInstance.saveContext()
    }
    
    func delete() {
        
    }
    
    func setNil() {
        
    }
    
    func canSync() -> Bool {
        return true
    }
    
    func getClass() -> String {
        return "User"
    }
    
    func json(complete:Bool) -> NSDictionary {
        
        let user:NSMutableDictionary = NSMutableDictionary()
        
        if (complete) {
        }
        user["id"] = id
        user["password"] = password
        
        return user
        
    }
    
}