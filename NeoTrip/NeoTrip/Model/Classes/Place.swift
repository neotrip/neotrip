//
//  Local.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 10/26/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import Foundation
import CoreData

@objc enum PlaceType : Int {
    case NONE = 0
    case PORT
    case ATTRACTION
    case ACCOMODATION
    
    case ERROR
    
    case ROOT
}

class Place: NSManagedObject {
    
    @NSManaged var name: String?
    @NSManaged var price: NSNumber?
    @NSManaged var startTime: NSDate?
    @NSManaged var interval: NSNumber?
    @NSManaged var address: String?
    
    @NSManaged var placeId: NSNumber?
    @NSManaged var number: NSNumber?
    @NSManaged var category: PlaceType
    
    @NSManaged var city: City?
    @NSManaged var itinerary: Itinerary?
    @NSManaged var startPoints: NSSet?
    @NSManaged var endPoints: NSSet?
    
    /* For database purposes */
    @NSManaged var id: String?
    @NSManaged var synced: NSDate?
    @NSManaged var status: Status
    
    static let CITY = "city"
    static let COUNTRY = "country"
}

extension Place {
    
    static func place(id:String, category:PlaceType) -> Place {
        let place:Place
        let managedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        
        let request = NSFetchRequest()
        request.entity = NSEntityDescription.entityForName("Place", inManagedObjectContext: managedObjectContext)
        request.predicate = NSPredicate(format: "id == %@", id)
        
        do {
            let list = try managedObjectContext.executeFetchRequest(request) as! [Place]
            if list.count > 0 {
                place = list[0]
                if  ((place.category == PlaceType.NONE || place.category == PlaceType.ROOT)
                    && (category == PlaceType.NONE || category == PlaceType.ROOT)) {
                        place.category = category
                }
                return place
            }
        } catch {}
        
        switch category {
        case .NONE:
            place = Place()
            place.category = PlaceType.NONE
        case .PORT:
            place = Port()
        case .ACCOMODATION:
            place = Accomodation()
        case .ATTRACTION:
            place = Attraction()
        case .ROOT:
            place = Place()
            place.category = PlaceType.ROOT
        case .ERROR:
            fatalError("MISSING SOMETHING")
        }
        return place
        
    }
    
    static func remove(id:String) {
        let managedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        
        let request = NSFetchRequest()
        request.entity = NSEntityDescription.entityForName("Place", inManagedObjectContext: managedObjectContext)
        request.predicate = NSPredicate(format: "id == %@", id)
        
        do {
            let list = try managedObjectContext.executeFetchRequest(request) as! [Place]
            if list.count > 0 {
                CoredataManager.sharedInstance.managedObjectContext.deleteObject(list[0])
            }
        } catch {}
    }
    
    static func search(name:String, queries:NSDictionary?) -> [Place] {
        let managedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        
        let request = NSFetchRequest()
        request.entity = NSEntityDescription.entityForName("Place", inManagedObjectContext: managedObjectContext)
        
        let query = NSMutableString()
        query.appendFormat("name contains[cd] \"%@\"", name)
        if queries != nil {
            if let city = queries!.objectForKey(CITY) as? NSNumber {
                query.appendFormat(" && city.id == %d", city.integerValue)
            }
            if let country = queries!.objectForKey(COUNTRY) as? NSNumber {
                query.appendFormat(" && city.country.id == %d", country.integerValue)
            }
        }
        request.predicate = NSPredicate(format: query.description)
        
        let list:[Place]
        do {
            list = try managedObjectContext.executeFetchRequest(request) as! [Place]
        } catch {
            list = [Place]()
        }
        
        return list
    }
    
    convenience init () {
        let managedObjectContext: NSManagedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        let entityDescription = NSEntityDescription.entityForName("Place", inManagedObjectContext: managedObjectContext)
        self.init(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext)
        
        interval = 0
        price = 0
        name = fillName()
        price = nil
    }
    
    func fillName() -> String{
        if (name != "" && name != nil){
            return name!
        }
        
        return "(sem nome)"
    }
    
    func sumPrices() -> CGFloat{
        let itinerary = self.itinerary!
        let city = self.city!
        
        let accommodations = itinerary.accommodationsIn(city)
        let attractions = itinerary.attractionsIn(city)
        let ports = itinerary.portsIn(city)
        let transports = itinerary.transportsIn(city)
        
        
        var sum: CGFloat = 0.0
        for accommodation in accommodations{
            if (accommodation.category != .NONE){
                sum += CGFloat(accommodation.getPrice())
            }
        }
        
        for attraction in attractions{
            if (attraction.category != .NONE){
                sum += CGFloat(attraction.getPrice())
            }
        }
        
        for port in ports{
            if (port.category != .NONE){
                sum += CGFloat(port.getPrice())
            }
        }
        
        for transport in transports{
            sum += CGFloat(transport.getPrice())
        }
        
        return sum
    }
    
    func usedDays() -> Int{
        var total = 0
        for a in itinerary!.accommodationsIn(self.city!) {
            if (a != self && a.category != .NONE){
                total = total + Int(a.interval!)
            }
        }
        
        return total
    }
    
    /*  getPrice (VIP)
    retorna o preço atual desse place */
    func getPrice() -> NSNumber {
        if price != nil {
            return price!
        }
        else {
            price = 0.0
            return price!
        }
    }
    
    /*  getInterval (VIP)
    retorna o intervalo desse place */
    func getInterval() -> NSNumber {
        if interval != nil {
            return interval!
        }
        else {
            interval = 0
            return interval!
        }
    }
    
    /*  isEmpty (VIP)
    retorna se true se atracao estiver vazia ou false se nao estiver */
    func isEmpty() -> Bool {
        // provisorio
        if (price == nil) {
            return true
        }
        else {
            return false
        }
    }
    
    func beginDate() -> NSDate? {
        if (itinerary != nil) {
            if (itinerary!.transports != nil) {
                if (self.interval == 0) {
                    // CASO ROOT
                    if (self.startTime != itinerary!.startTime!) {
                        self.startTime = itinerary!.startTime!
                        self.save()
                    }
                    return itinerary!.startTime!
                }
                else {
                    // CASO OUTRO
                    for t in itinerary!.transports!.allObjects as! [Transport] {
                        if (t.endPoint == self) {
                            if (t.startPoint != nil) {
                                if (t.startPoint!.beginDate() != nil) {
                                    if (t.startPoint!.interval! == 0) {
                                        if (self.startTime != t.startPoint!.beginDate()) {
                                            self.startTime = t.startPoint!.beginDate()
                                            self.save()
                                        }
                                        return t.startPoint!.beginDate()
                                    }
                                    if (self.startTime != t.startPoint!.endDate()) {
                                        self.startTime = t.startPoint!.endDate()
                                        self.save()
                                    }
                                    return t.startPoint!.endDate()
                                }
                                else {
                                    if (self.startTime != nil) {
                                        self.startTime = nil
                                        self.save()
                                    }
                                    return nil
                                }
                            }
                        }
                    }
                }
            }
        }
        if (self.startTime != nil) {
            self.startTime = nil
            self.save()
        }
        return nil
    }
    
    func endDate() -> NSDate? {
        let begin : NSDate? = beginDate()
        if (begin != nil) {
            if (self.interval != nil) {
                return begin!.dateByAddingTimeInterval(Double(self.interval!) * 86400.00)
            }
            return begin!
        }
        return nil
    }
}

extension Place : Object {
    
    func setInfo(info:NSDictionary) {
        placeId = info.objectForKey("placeId") as? NSNumber
        name = info.objectForKey("name") as? String
        interval = info.objectForKey("interval") as? NSNumber
        price = info.objectForKey("price") as? NSNumber
        
        id = info.objectForKey("id") as? String
        address = info.objectForKey("address") as? String
        
        city = City.city(info.objectForKey("cityId") as! NSNumber)
        
        startTime = dateFromString(info.objectForKey("startTime") as? String)
        synced = dateFromString(info.objectForKey("updatedAt") as? String)
    }
    
    func sync() {
        //CoredataManager.sharedInstance.saveContext()
    }
    
    func save() {
        
        if (status == Status.syncing) {
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(5 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.save()
            }
        } else if (status == Status.delete || status == Status.deleting || status == Status.deleted) {
            fatalError("PORRA VOCÊ DELETOU E TA SALVANDO DE NOVO SUA ANTA")
        } else {
            status = Status.edited
            set.addObjectAndNotify(self)
        }
        
    }
    
    func delete() {
        /* Delete transport. */
        if (status == Status.syncing) {
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(5 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.delete()
            }
        } else if (status == Status.delete || status == Status.deleting || status == Status.deleted) {
            //fatalError("VOCÊ DELETOU E TA DELETANDO DE NOVO SUA ANTA")
        } else {
            
            status = Status.wait
            for object in (startPoints?.setByAddingObjectsFromSet(endPoints as! Set<NSObject>))! {
                (object as! Transport).status = Status.deleted
            }
            
            status = Status.delete
            set.addObjectAndNotify(self)
        }
        
    }
    
    func setNil() {
        //itinerary = nil
        //city = nil
    }
    
    
    func canSync() -> Bool {
        return itinerary?.id != nil
    }
    
    func getClass() -> String {
        return "Place"
    }
    
    func json(complete:Bool) -> NSDictionary {
        
        let place:NSMutableDictionary = NSMutableDictionary()
        
        if (complete) {
            place["name"] = name
            place["price"] = price
            place["startTime"] = stringFromDate(startTime)
            place["interval"] = interval
            
            place["number"] = number
            place["placeId"] = placeId
            
            //place["addressId"] = address
            place["cityId"] = city?.id
            place["category"] = category.rawValue
            place["subcategory"] = PlaceType.NONE.rawValue
        }
        place["id"] = id
        place["synced"] = stringFromDate(synced)
        place["travelId"] = itinerary!.id
        
        return place
        
    }
    
    func cityToString() -> String{
        return self.city!.name!
    }
    
}

