//
//  Message.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 10/26/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import Foundation
import CoreData


class Message: NSManagedObject {
    
    @NSManaged var date: NSDate?
    @NSManaged var message: String?
    @NSManaged var travel: Travel?
    @NSManaged var traveler: Traveler?

}

extension Message {
    
    convenience init () {
        let managedObjectContext: NSManagedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        let entityDescription = NSEntityDescription.entityForName("Message", inManagedObjectContext: managedObjectContext)
        self.init(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext)
    }
    
    func delete() {
        travel = nil
        traveler = nil
    }
}