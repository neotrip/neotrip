//
//  Itinerary.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 10/26/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import Foundation
import CoreData


class Itinerary: NSManagedObject {

    @NSManaged var price: NSNumber?
    @NSManaged var startTime: NSDate?
    @NSManaged var interval: NSNumber?
    @NSManaged var travel: Travel?
    @NSManaged var places: NSSet?
    @NSManaged var transports: NSSet?
    
    /* For database purposes */
    @NSManaged var id: String?
    @NSManaged var synced: NSDate?
    @NSManaged var status: Status
    
}

extension Itinerary {
    
    convenience init () {
        let managedObjectContext: NSManagedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        let entityDescription = NSEntityDescription.entityForName("Itinerary", inManagedObjectContext: managedObjectContext)
        self.init(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext)
        
        status = Status.edited
        price = 0
        interval = 0
        startTime = NSDate()
    }
    
    func push() {
        let query = NSMutableString()
        query.appendFormat("status != %d ", Status.synced.rawValue)
        query.appendFormat("&& status != %d ", Status.notSynced.rawValue)
        
        for place in places!.filteredSetUsingPredicate(NSPredicate(format: query.description)) {
            switch ((place as! Place).status) {
            case .syncing:
                (place as! Place).status = .edited
                break
            case .deleting:
                (place as! Place).status = .delete
                break
            default:
                break
            }
            set.addObjectAndNotify(place)
        }
        
        for transport in transports!.filteredSetUsingPredicate(NSPredicate(format: query.description)) {
            switch ((transport as! Transport).status) {
            case .syncing:
                (transport as! Transport).status = .edited
                break
            case .deleting:
                (transport as! Transport).status = .delete
                break
            default:
                break
            }
            set.addObjectAndNotify(transport)
        }
    }
    
    //retorna um array de cidades sem repetir
    func cities() -> [City]? {
        let citiesArray: NSMutableSet = NSMutableSet()
        let placesArray = places?.allObjects as! [Place]
        
        for place in placesArray {
            switch place.status {
            case .delete, .deleted, .deleting, .wait:
                break
            default:
                citiesArray.addObject(place.city!)
                break
            }
            
        }
        
        return citiesArray.allObjects as? [City]
    }

    
    /*  updatePrice (VIP)
        verifica os subniveis para atualizar o preço da viagem */
    func updatePrice() {
        // pode dar problema no coredata (feedback Susin)
        var p : NSNumber = 0.0
        if places != nil {
            for place in places!.allObjects as! [Place] {
                p = p.doubleValue + place.getPrice().doubleValue
            }
        }
        
        if transports != nil {
            for transport in transports! {
                p = p.doubleValue + transport.getPrice().doubleValue
            }
        }
        price = p
    }
    
    /*  getPrice (VIP)
        retorna o preço atual desse itinerary */
    func getPrice() -> NSNumber {
        updatePrice()
        return price!
    }
    
    /*  updateInterval (VIP)
        verifica os subniveis para atualizar o intervalo da viagem */
    
    func updateInterval() {
        var i : NSNumber = 0
        if places != nil {
            for place in places?.allObjects as! [Place] {
                if (place.category != .ATTRACTION && place.category != .ACCOMODATION && place.category != .PORT){
                    i = i.doubleValue + place.getInterval().doubleValue
                }
            }
        }
        
        if transports != nil {
            for transport in transports! {
                i = i.doubleValue + transport.getInterval().doubleValue
            }
        }
        
        interval = i
    }

    
    /*  getInterval (VIP)
        retorna o intervalo desse itinerary */
    func getInterval() -> NSNumber {
        updateInterval()
        
        return interval!
    }
    
    /*  removePlaceObject (VIP)
        remove um objeto place no conjunto de places do itinerario */
    func removePlaceObject(place: Place) {
        let items = self.mutableSetValueForKey("places");
        items.removeObject(place)
    }
    
    /*  getRoot (JAO) */
    func root() -> Place? {
        if (places != nil){
            for place in places!.allObjects as! [Place] {
                if ( place.category == PlaceType.ROOT ){
                    switch (place.status) {
                    case .wait, .delete, .deleted, .deleting:
                        return nil
                    default:
                        return place
                    }
                }
            }
        }
        
        return nil
    }
    
    func placesAtCity(city: City) -> [Place] {
        var array:[Place] = []
        
        for place in places as! Set<Place> {
            if (place.city == city) {
                array.append(place)
            }
        }
        
        return array
    }
    
    /*  getAttractionsIn (VIP)
        */
    
    func getPlaces(city:City) -> [Place] {
        let predicate:NSPredicate = NSPredicate(format: "city.id == %d && category == %d", city.id!, PlaceType.ACCOMODATION.rawValue)
        return (places!.filteredSetUsingPredicate(predicate) as NSSet).allObjects as! [Place]
    }

    func attractionsIn(city: City) -> [Attraction] {
        var attractions : [Attraction] = []
        if places != nil {
            for place in places! {
                if (place.isKindOfClass(Attraction)) {
                    let attraction = place as! Attraction
                    switch (attraction.status) {
                    case .wait, .delete, .deleted, .deleting:
                        break
                    default:
                        if (attraction.city == city) {
                            attractions.append(attraction)
                        }
                        break
                    }
                }
            }
        }
        
        return attractions
    }
    
    /*  getTransportsIn (JAO)
    */
    func transportsIn(city: City) -> [Transport] {
        var cityTransports : [Transport] = []
        
        if (transports != nil){
            let allTransports = transports!.allObjects as! [Transport]
            
            for transport in allTransports{
                let startCity = transport.startPoint?.city
                let endCity = transport.endPoint?.city
                
                if (startCity != nil && endCity != nil){
                    if (startCity == city && endCity == city){
                        cityTransports.append(transport)
                    }
                }
            }
            
        }
        
        return cityTransports
    }
    
    /*  getAccommodationsIn (VIP)
        */
    func accommodationsIn(city: City) -> [Accomodation] {
        var accomodations : [Accomodation] = []
        if places != nil {
            for place in places! {
                if (place.isKindOfClass(Accomodation)) {
                    let accomodation = place as! Accomodation
                    switch (accomodation.status) {
                    case .wait, .delete, .deleted, .deleting:
                        break
                    default:
                        if (accomodation.city == city) {
                            accomodations.append(accomodation)
                        }
                        break
                    }
                }
            }
        }
        return accomodations
    }
    
    /*  getPort from city (KIQ)
        */
    /* QUE PORRA EH ESSA? */
    func portIn(city: City) -> Port {
        var portIn = Port()
        if places != nil {
            for place in places! {
                if (place.isKindOfClass(Port)){
                    switch (status) {
                    case .wait, .delete, .deleted, .deleting:
                        break
                    default:
                        if((place as! Port).city == city){
                            portIn = place as! Port
                        }
                        
                        break
                        
                    }
                }
            }
        }
        return portIn
    }
    
    func portsIn (city: City) -> [Port] {
        var ports : [Port] = []
        if places != nil {
            for place in places! {
                if (place.isKindOfClass(Port)) {
                    let port = place as! Port
                    switch (port.status) {
                    case .wait, .delete, .deleted, .deleting:
                        break
                    default:
                        if ((place as! Port).city == city) {
                            ports.append(place as! Port)
                        }
                        break
                    }
                }
            }
        }
        return ports
    }
    
    func placeThatRepresents (city: City) -> Place? {
        var placeThatRepresents : Place? = nil
        if places != nil {
            for place in places! {
                let p = place as! Place
                if ((p.category == PlaceType.ROOT || p.category == PlaceType.NONE) && p.city == city) {
                    placeThatRepresents = p
                }
            }
        }
        return placeThatRepresents
    }
    
    func setTimeInterval(endDate:NSDate){
        let calendar: NSCalendar = NSCalendar.currentCalendar()
        let date1 = calendar.startOfDayForDate((self.startTime)!)
        let date2 = calendar.startOfDayForDate(endDate)
        let flags = NSCalendarUnit.Day
        let components = calendar.components(flags, fromDate: date1, toDate: date2, options: [])
        self.interval = NSNumber(integer: components.day)
    }
    
    func returnEndDate()-> NSDate{
        let dayComponent = NSDateComponents()
        dayComponent.day = (self.interval?.integerValue)!
        let calendar = NSCalendar.currentCalendar()
        let nextDate = calendar.dateByAddingComponents(dayComponent, toDate: self.startTime!, options: NSCalendarOptions())
        return nextDate!
    }
    
 }

extension Itinerary  {
    
    static func itinerary(id:String) -> Itinerary {
        let managedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        
        let request = NSFetchRequest()
        request.entity = NSEntityDescription.entityForName("Itinerary", inManagedObjectContext: managedObjectContext)
        request.predicate = NSPredicate(format: "id == %@", id)
        
        do {
            let list = try managedObjectContext.executeFetchRequest(request) as! [Itinerary]
            if list.count > 0 {
                return list[0]
            }
        } catch {}
        
        return Itinerary()
    }
    
    func setInfo(info:NSDictionary) {
        //id = info.objectForKey("id") as? String
        interval = info.objectForKey("interval") as? NSNumber
        price = info.objectForKey("price") as? NSNumber
        startTime = dateFromString(info.objectForKey("startTime") as? String)
        synced = dateFromString(info.objectForKey("updatedAt") as? String)
    }
    
    func sync() {
        //CoredataManager.sharedInstance.saveContext()
    }
    
    func save() {
        
        fatalError("you are not supposed to save a itinerary, only travel")
        
    }
    
    func delete() {
        
        for object in places! {
            (object as! Place).status = Status.deleted
        }
        for object in transports! {
            (object as! Transport).status = Status.deleted
        }
        
    }
    
    func setNil() {
    }
    
    func canSync() -> Bool {
        return travel?.id != nil
    }
    
    func getClass() -> String {
        return "Itinerary"
    }
    
    func json(complete:Bool) -> NSDictionary {
        
        let itinerary:NSMutableDictionary = NSMutableDictionary()
        
        if (complete) {
            itinerary["price"] = price
            itinerary["startTime"] = stringFromDate(startTime)
            itinerary["interval"] = interval
            itinerary["travelId"] = travel?.id
        }
        itinerary["id"] = id
        itinerary["synced"] = stringFromDate(synced)
        
        return itinerary
        
    }


}
