//
//  Transport.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 10/26/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import Foundation
import CoreData

@objc enum TransportType : Int {
    case NONE
    case AIRPLANE
    case SHIP
    case TRAIN
    case SUBWAY
    case BUS
    case TAXI
    case RIDE
    case CAR
    case BICYCLE
    case FOOT
    case UBER
}

class Transport: NSManagedObject {
    
    @NSManaged var name: String?
    @NSManaged var price: NSNumber?
    @NSManaged var startTime: NSDate?
    @NSManaged var interval: NSNumber?
    @NSManaged var category: TransportType
    
    @NSManaged var startPoint: Place?
    @NSManaged var endPoint: Place?
    @NSManaged var itinerary: Itinerary?
    
    /* For database purposes */
    @NSManaged var id: String?
    @NSManaged var synced: NSDate?
    @NSManaged var status: Status

}

extension Transport {
    
    convenience init () {
        let managedObjectContext: NSManagedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        let entityDescription = NSEntityDescription.entityForName("Transport", inManagedObjectContext: managedObjectContext)
        self.init(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext)
        
        interval = 0
    }
    
    /*  getPrice (VIP)
        retorna o preço atual desse transport */
    func getPrice() -> NSNumber {
        if price != nil {
            return price!
        }
        else {
            price = 0.0
            return price!
        }
    }
    
    /*  getInterval (VIP)
        retorna o intervalo desse transport */
    func getInterval() -> NSNumber {
        if interval != nil {
            return interval!
        }
        else {
            interval = 0
            return interval!
        }
    }
}

extension Transport : Object {
    
    static func transport(id:String) -> Transport {
        let managedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        
        let request = NSFetchRequest()
        request.entity = NSEntityDescription.entityForName("Transport", inManagedObjectContext: managedObjectContext)
        request.predicate = NSPredicate(format: "id == %@", id)
        
        do {
            let list = try managedObjectContext.executeFetchRequest(request) as! [Transport]
            if list.count > 0 {
                return list[0]
            }
        } catch {}
        
        return Transport()
    }
    
    static func remove(id:String) {
        let managedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        
        let request = NSFetchRequest()
        request.entity = NSEntityDescription.entityForName("Transport", inManagedObjectContext: managedObjectContext)
        request.predicate = NSPredicate(format: "id == %@", id)
        
        do {
            let list = try managedObjectContext.executeFetchRequest(request) as! [Transport]
            if list.count > 0 {
                CoredataManager.sharedInstance.managedObjectContext.deleteObject(list[0])
            }
        } catch {}
    }
    
    func setInfo(info:NSDictionary) {
        name = info.objectForKey("name") as? String
        id = info.objectForKey("id") as? String
        interval = info.objectForKey("interval") as? NSNumber
        price = info.objectForKey("price") as? NSNumber
        startTime = dateFromString(info.objectForKey("startTime") as? String)
        category = TransportType(rawValue: (info.objectForKey("category") as! NSNumber).integerValue)!
        
        synced = dateFromString(info.objectForKey("updatedAt") as? String)
        
        startPoint = Place.place(info.objectForKey("startPoint") as! String, category: PlaceType.ERROR)
        endPoint = Place.place(info.objectForKey("endPoint") as! String, category: PlaceType.ERROR)
    }
    
    func sync() {
        //CoredataManager.sharedInstance.saveContext()
    }
    
    func save() {
        
        if (status == Status.syncing) {
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(5 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.save()
            }
        } else if (status == Status.delete || status == Status.deleting || status == Status.deleted) {
            fatalError("PORRA VOCÊ DELETOU E TA SALVANDO DE NOVO SUA ANTA")
        } else {
            status = Status.edited
            set.addObjectAndNotify(self)
        }
        
    }
    
    func delete() {
        /* Delete transport. */
        if (status == Status.syncing) {
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(5 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.delete()
            }
        } else if (status == Status.delete || status == Status.deleting || status == Status.deleted) {
            //fatalError("VOCÊ DELETOU E TA DELETANDO DE NOVO SUA ANTA")
        } else {
            status = Status.delete
            set.addObjectAndNotify(self)
        }
        
    }
    
    func setNil() {
        startPoint = nil
        endPoint = nil
        itinerary = nil
    }
    
    func canSync() -> Bool {
        return itinerary?.id != nil && startPoint?.id != nil && endPoint?.id != nil
    }
    
    func getClass() -> String {
        return "Transport"
    }
    
    func json(complete:Bool) -> NSDictionary {
        
        let transport:NSMutableDictionary = NSMutableDictionary()
        
        if (complete) {
            transport["name"] = name
            transport["price"] = price
            transport["startTime"] = stringFromDate(startTime)
            transport["interval"] = interval
            transport["startPointId"] = startPoint?.id
            transport["endPointId"] = endPoint?.id
            transport["category"] = category.rawValue
        }
        transport["travelId"] = itinerary?.id
        transport["id"] = id
        transport["synced"] = stringFromDate(synced)
        
        return transport
        
    }

}