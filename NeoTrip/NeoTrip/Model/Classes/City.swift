//
//  City.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 10/26/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import Foundation
import CoreData


class City: NSManagedObject {
    
    @NSManaged var general: String?
    @NSManaged var name: String?
    @NSManaged var region: String?
    @NSManaged var transport: String?
    @NSManaged var country: Country?
    @NSManaged var latitude: NSNumber?
    @NSManaged var longitude: NSNumber?
    
    @NSManaged var places: NSSet?
    @NSManaged var travelers: NSSet?
    
    @NSManaged var id: NSNumber?
    
    static let COUNTRY = "country"
}

extension City {
    
    static func city(id:NSNumber) -> City {
        let managedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        
        let request = NSFetchRequest()
        request.entity = NSEntityDescription.entityForName("City", inManagedObjectContext: managedObjectContext)
        request.predicate = NSPredicate(format: "id == %@", id)
        
        do {
            let list = try managedObjectContext.executeFetchRequest(request) as! [City]
            if list.count > 0 {
                return list[0]
            }
        } catch {}
        
        return City()
    }
    
    static func search(name:String, queries:NSDictionary?) -> [City] {
        let managedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        
        let request = NSFetchRequest()
        
        request.fetchLimit = 20
        request.entity = NSEntityDescription.entityForName("City", inManagedObjectContext: managedObjectContext)
        
        let query = NSMutableString()
        query.appendFormat("name contains[cd] \"%@\"", name)
        if queries != nil {
            if let country = queries!.objectForKey(COUNTRY) as? NSNumber {
                query.appendFormat(" && country.id == %d", country.integerValue)
            }
        }
        request.predicate = NSPredicate(format: query.description)

        let list:[City]
        do {
            list = try managedObjectContext.executeFetchRequest(request) as! [City]
        } catch {
            list = [City]()
        }
        
        return list
    }
    
    func setInfo(info:NSDictionary) {
        id = info.objectForKey("id") as? NSNumber
        name = info.objectForKey("name") as? String
        latitude = info.objectForKey("latitude") as? NSNumber
        longitude = info.objectForKey("longitude") as? NSNumber
        
        if let dictAddress = info.objectForKey("addressSet") as? [NSDictionary] {
            for dict in dictAddress {
                let address = info.objectForKey("name") as? String
                if let dictPlace = dict.objectForKey("placeSet") as? [NSDictionary] {
                    for dict in dictPlace {
                        let category = PlaceType(rawValue: dict.objectForKey("category") as! Int)!
                        let place = Place.place(dict.objectForKey("id") as! String, category: category)
                        place.id = dict.objectForKey("id") as? String
                        place.name = dict.objectForKey("name") as? String
                        place.city = self
                        place.address = address
                        place.number = dict.objectForKey("number") as? NSNumber
                    }
                }
            }
        }
    }
    
    convenience init () {
        let managedObjectContext: NSManagedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        let entityDescription = NSEntityDescription.entityForName("City", inManagedObjectContext: managedObjectContext)
        self.init(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext)
    }
    
    func save() {
        CoredataManager.sharedInstance.saveContext()
    }
    
    class func cities(completion: (cities: [City]) -> Void){
        let request: NSFetchRequest = NSFetchRequest(entityName: "City");
        completion(cities: (try! CoredataManager.sharedInstance.managedObjectContext.executeFetchRequest(request)) as! [City])
    }
    
    func direction(root: City) -> Direction{
        if (abs(self.latitude!.doubleValue - root.latitude!.doubleValue) >= abs(self.longitude!.doubleValue - root.longitude!.doubleValue)){
            if (self.latitude!.doubleValue >= root.latitude!.doubleValue){
                return Direction.NORTH
            } else {
                return Direction.SOUTH
            }
        } else {
            if (self.longitude!.doubleValue >= root.longitude!.doubleValue){
                return Direction.EAST
            } else {
                return Direction.WEST
            }
        }
    }
    
    /* TEMPORARIO */
    class func createCity(name: String, lat: Double, long: Double, country: Country){
        let newCity = City()
        newCity.name = name
        newCity.country = country
        newCity.latitude = lat
        newCity.longitude = long
        newCity.save()
    }
    
}