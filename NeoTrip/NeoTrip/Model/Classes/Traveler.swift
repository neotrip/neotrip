//
//  Traveler.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 10/26/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import Foundation
import CoreData


class Traveler: NSManagedObject {
    
    @NSManaged var countries:String?
    
    @NSManaged var id: String?
    @NSManaged var firstName: String?
    @NSManaged var lastName: String?
    @NSManaged var mail: String?
    @NSManaged var city: City?
    
    @NSManaged var messages: NSSet?
    @NSManaged var posts: NSSet?
    @NSManaged var travels: NSSet?
    @NSManaged var travelsOwned: NSSet?
    @NSManaged var travelsWaiting: NSSet?
    
    @NSManaged var avatar: Image!
    @NSManaged var bgImage: Image!
    
    @NSManaged var following: NSSet?
    @NSManaged var followers: NSSet?
    
    /* For database purposes */
    @NSManaged var synced: NSDate?
    @NSManaged var status: Status
    
}

extension Traveler {
    
    convenience init () {
        let managedObjectContext: NSManagedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        let entityDescription = NSEntityDescription.entityForName("Traveler", inManagedObjectContext: managedObjectContext)
        self.init(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext)
        
        status = Status.notSynced
        
        avatar = Image()
        bgImage = Image()
        avatar.image = UIImagePNGRepresentation(UIImage(named: "avatar-backpacker")!)
        bgImage.image = UIImagePNGRepresentation(UIImage(named: "background-travel")!)
    }
    
    func followTraveler(value:Traveler) {
        let items = self.mutableSetValueForKey("following");
        items.addObject(value)
    }
    
    func unfollowTraveler(value:Traveler) {
        let items = self.mutableSetValueForKey("following");
        items.removeObject(value)
    }
    
    func following(traveler:Traveler) -> Bool {
        return self.following!.containsObject(traveler)
    }
        
}

extension Traveler : Object {
    
    static func traveler(id:String) -> Traveler {
        let managedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        
        let request = NSFetchRequest()
        request.entity = NSEntityDescription.entityForName("Traveler", inManagedObjectContext: managedObjectContext)
        request.predicate = NSPredicate(format: "id == %@", id)
        
        do {
            let list = try managedObjectContext.executeFetchRequest(request) as! [Traveler]
            if list.count > 0 {
                return list[0]
            }
        } catch {}
        
        return Traveler()
    }
    
    static func search(name:String) -> [Traveler] {
        let managedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        
        let request = NSFetchRequest()
        request.entity = NSEntityDescription.entityForName("Traveler", inManagedObjectContext: managedObjectContext)
        
        let query = NSMutableString()
        query.appendFormat("id contains[cd] \"%@\"", name)
        query.appendFormat(" || firstName contains[cd] \"%@\"", name)
        request.predicate = NSPredicate(format: query.description)
        
        
        let list:[Traveler]
        do {
            list = try managedObjectContext.executeFetchRequest(request) as! [Traveler]
        } catch {
            list = [Traveler]()
        }
        
        return list
    }
    
    func setInfo(info:NSDictionary, complete: Bool) {
        id = info.objectForKey("id") as? String
        firstName = info.objectForKey("firstName") as? String
        lastName = info.objectForKey("lastName") as? String
        
        if (complete) {
            mail = info.objectForKey("mail") as? String
            countries = info.objectForKey("countries") as? String
            synced = dateFromString(info.objectForKey("updatedAt") as? String)
            
            if let cityId = info.objectForKey("cityId") as? NSNumber {
                city = City.city(cityId)
            }
        }
        
    }
    
    func sync() {
        status = Status.synced
        CoredataManager.sharedInstance.saveContext()
    }
    
    func save() {
        
        if (status == Status.syncing) {
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(5 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.save()
            }
        } else if (status == Status.delete || status == Status.deleting || status == Status.deleted) {
            fatalError("PORRA VOCÊ DELETOU E TA SALVANDO DE NOVO SUA ANTA")
        } else {
            status = Status.edited
            set.addObjectAndNotify(self)
            CoredataManager.sharedInstance.saveContext()
        }
        
    }
    
    func delete() {
        
    }
    
    
    func setNil() {
        
    }
    
    func canSync() -> Bool {
        return true
    }
    
    func getClass() -> String {
        return "Traveler"
    }
    
    func json(complete:Bool) -> NSDictionary {
        
        let traveler:NSMutableDictionary = NSMutableDictionary()
        
        if (complete) {
            traveler["firstName"] = firstName
            traveler["lastName"] = lastName
            traveler["countries"] = countries
            traveler["mail"] = mail
            traveler["cityId"] = city?.id
        }
        traveler["id"] = id
        traveler["synced"] = stringFromDate(synced)
        
        return traveler
        
    }

}

