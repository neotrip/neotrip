//
//  Country.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 10/26/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import Foundation
import CoreData


class Country: NSManagedObject {
    
    @NSManaged var flag: String?
    @NSManaged var general: String?
    @NSManaged var name: String?
    @NSManaged var cities: NSSet?

    @NSManaged var id: String?

}

extension Country {
    
    static func country(id:String) -> Country {
        let managedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        
        let request = NSFetchRequest()
        request.entity = NSEntityDescription.entityForName("Country", inManagedObjectContext: managedObjectContext)
        request.predicate = NSPredicate(format: "id == %@", id)
        
        do {
            let list = try managedObjectContext.executeFetchRequest(request) as! [Country]
            if list.count > 0 {
                return list[0]
            }
        } catch {}

        return Country()
    }

    func setInfo(info:NSDictionary) {
        id = info.objectForKey("id") as? String
        name = info.objectForKey("name") as? String
        
        if let dictCity = info.objectForKey("citySet") as? [NSDictionary] {
            for dict in dictCity {
                let city:City = City.city(dict.objectForKey("id") as! NSNumber)
                city.country = self
                city.setInfo(dict)
            }
        }
    
    }
    
    convenience init () {
        let managedObjectContext: NSManagedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        let entityDescription = NSEntityDescription.entityForName("Country", inManagedObjectContext: managedObjectContext)
        self.init(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext)
    }
    
    func save() {
        CoredataManager.sharedInstance.saveContext()
    }
    
}