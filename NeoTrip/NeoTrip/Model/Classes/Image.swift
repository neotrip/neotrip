//
//  Image.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 1/26/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import Foundation
import CoreData


class Image: NSManagedObject {
    
    @NSManaged var image: NSData?
    
    /* For database purposes */
    @NSManaged var id: String?
    @NSManaged var synced: NSDate?
    @NSManaged var status: Status

}

extension Image {
    
    convenience init () {
        let managedObjectContext: NSManagedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        let entityDescription = NSEntityDescription.entityForName("Image", inManagedObjectContext: managedObjectContext)
        self.init(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext)
    }

}

extension Image : Object {
    
    func sync() {
        CoredataManager.sharedInstance.saveContext()
    }
    
    func save() {
        
        if (status == Status.syncing) {
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(5 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.save()
            }
        } else if (status == Status.delete || status == Status.deleting || status == Status.deleted) {
            fatalError("PORRA VOCÊ DELETOU E TA SALVANDO DE NOVO SUA ANTA")
        } else {
            
            status = Status.edited
            set.addObjectAndNotify(self)
            CoredataManager.sharedInstance.saveContext()
        }
        
    }

    func delete() {
        
    }
    
    func setNil() {
        
    }
    
    func canSync() -> Bool {
        return true
    }
    
    func getClass() -> String {
        return "Image"
    }
    
    func json(complete:Bool) -> NSDictionary {
        
        let dict:NSMutableDictionary = NSMutableDictionary()
        
        dict["image"] = image?.base64EncodedStringWithOptions(.EncodingEndLineWithLineFeed)
        
        return dict
        
    }
    
}