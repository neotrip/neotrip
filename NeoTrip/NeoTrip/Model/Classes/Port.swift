//
//  Port.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 10/26/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import Foundation
import CoreData

@objc enum PortType : Int {
    case OTHER
}

class Port: Place {
    
    @NSManaged var subCategory: PortType
    
}

extension Port {
    
    convenience init () {
        let managedObjectContext: NSManagedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        let entityDescription = NSEntityDescription.entityForName("Port", inManagedObjectContext: managedObjectContext)
        self.init(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext)
        
        category = PlaceType.PORT
    }
    
    override func json(complete: Bool) -> NSDictionary {
        let place:NSMutableDictionary = super.json(complete) as! NSMutableDictionary
        place["subcategory"] = subCategory.rawValue
        return place
    }
    
}