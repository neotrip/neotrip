//
//  Accomodation.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 10/26/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import Foundation
import CoreData

@objc enum AccomodationType : Int {
    case OTHER
}

class Accomodation: Place {
    
    @NSManaged var dailyRate: NSNumber?
    @NSManaged var subCategory: AccomodationType
    
}

extension Accomodation {

    convenience init () {
        let managedObjectContext: NSManagedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        let entityDescription = NSEntityDescription.entityForName("Accomodation", inManagedObjectContext: managedObjectContext)
        self.init(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext)
        
        category = PlaceType.ACCOMODATION
    }
    
    override func json(complete: Bool) -> NSDictionary {
        let place:NSMutableDictionary = super.json(complete) as! NSMutableDictionary
        place["subcategory"] = subCategory.rawValue
        return place
    }
    
    override func getPrice() -> NSNumber {
        if price != nil {
            let priceFloat = price!.floatValue
            let intervalFloat = getInterval().floatValue
            return NSNumber(float: priceFloat*intervalFloat)
        }
        else {
            price = 0.0
            return price!
        }
    }

}