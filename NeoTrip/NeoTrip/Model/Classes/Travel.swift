//
//  Travel.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 10/26/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import Foundation
import CoreData

public let TRAVELSOWNED = 0
public let TRAVELSTAKING = 1

class Travel: NSManagedObject {
    
    @NSManaged var name: String?
    @NSManaged var privacy: NSNumber?
    @NSManaged var travelerOwner: Traveler?
    
    @NSManaged var itinerary: Itinerary?
    @NSManaged var messages: NSSet?
    @NSManaged var travelers: NSSet?
    @NSManaged var travelersWaiting: NSSet?
    
    /* For database purposes */
    @NSManaged var id: String?
    @NSManaged var synced: NSDate?
    @NSManaged var infoSynced: NSDate?
    @NSManaged var status: Status
    
}

extension Travel {
    
    override func observeValueForKeyPath(keyPath: String?, ofObject object: AnyObject?, change: [String : AnyObject]?, context: UnsafeMutablePointer<Void>) {
        if (keyPath == "id") {
            itinerary?.id = id
            itinerary?.status = Status.synced
//            itinerary?.sync()
        }
    }
    
    static func travel(id:String) -> Travel {
        let managedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        
        let request = NSFetchRequest()
        request.entity = NSEntityDescription.entityForName("Travel", inManagedObjectContext: managedObjectContext)
        request.predicate = NSPredicate(format: "id == %@", id)
        
        do {
            let list = try managedObjectContext.executeFetchRequest(request) as! [Travel]
            if list.count > 0 {
                return list[0]
            }
        } catch {}
        
        let travel = Travel()
        travel.itinerary?.status = Status.notSynced
        return travel
    }
    
    static func push(traveler:Traveler) {
        let managedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        
        let request = NSFetchRequest()
        request.entity = NSEntityDescription.entityForName("Travel", inManagedObjectContext: managedObjectContext)
        
        
        let query = NSMutableString()
        query.appendFormat("travelerOwner.id == \"%@\" ", traveler.id!)
        query.appendFormat("&& status != %d ", Status.synced.rawValue)
        query.appendFormat("&& status != %d ", Status.notSynced.rawValue)
        
        request.predicate = NSPredicate(format: query.description)
        
        
        let list:[Travel]
        do {
            list = try managedObjectContext.executeFetchRequest(request) as! [Travel]
            for travel in list {
                switch (travel.status) {
                case .syncing:
                    travel.status = .edited
                    break
                case .deleting:
                    travel.status = .delete
                    break
                default:
                    break
                }
                set.addObjectAndNotify(travel)
            }
        } catch {
        }
    }
    
    static func search(traveler:Traveler, option:Int) -> [Travel] {
        let managedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        
        let request = NSFetchRequest()
        request.entity = NSEntityDescription.entityForName("Travel", inManagedObjectContext: managedObjectContext)
        request.sortDescriptors = [NSSortDescriptor(key: "synced", ascending: false)]
        
        
        let query = NSMutableString()
        if (option == TRAVELSOWNED) {
            query.appendFormat("travelerOwner.id == \"%@\" ", traveler.id!)
        } else if (option == TRAVELSTAKING) {
            query.appendFormat("ANY travelers.id == \"%@\" ", traveler.id!)
        } else {
            fatalError("INVALID OPTION")
        }
        
        query.appendFormat("&& status != %d ", Status.wait.rawValue)
        query.appendFormat("&& status != %d ", Status.delete.rawValue)
        query.appendFormat("&& status != %d ", Status.deleted.rawValue)
        query.appendFormat("&& status != %d ", Status.deleting.rawValue)
        
        
        request.predicate = NSPredicate(format: query.description)
        
        
        let list:[Travel]
        do {
            list = try managedObjectContext.executeFetchRequest(request) as! [Travel]
        } catch {
            list = [Travel]()
        }
        
        return list
    }
    
    convenience init () {
        let managedObjectContext: NSManagedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        let entityDescription = NSEntityDescription.entityForName("Travel", inManagedObjectContext: managedObjectContext)
        self.init(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext)
        
        itinerary = Itinerary()
        
        if (travelerOwner != nil) {
            name = travelerOwner!.firstName! + String(travelerOwner!.travels!.count+1)
        }
        else {
            name = "Minha Viagem"
        }
        
        privacy = true
        
        messages = nil
        travelers = nil
        
        addObserver(self, forKeyPath: "id", options: [], context: &id)
        
    }
    
    func addTravelerObject(value:Traveler) {
        let items = self.mutableSetValueForKey("travelers");
        items.addObject(value)
    }
    
    func removeTravelerObject(value:Traveler) {
        let items = self.mutableSetValueForKey("travelers");
        items.removeObject(value)
    }
    
    func addWaitingTraveler(value:Traveler) {
        let items = self.mutableSetValueForKey("travelersWaiting");
        items.addObject(value)
    }
    
    func removeWaitingTraveler(value:Traveler) {
        let items = self.mutableSetValueForKey("travelersWaiting");
        items.removeObject(value)
    }
    
    func fillName() -> String{
        if (name != "" && name != nil){
            return name!
        }
        
        return "(sem nome)"
    }
    
}

extension Travel : Object {
    
    func setInfo(info:NSDictionary) {
        id = info.objectForKey("id") as? String
        name = info.objectForKey("name") as? String
        privacy = info.objectForKey("privacy") as! Bool
        synced = dateFromString(info.objectForKey("updatedAt") as? String)
        
        if let travelerDict = info.objectForKey("owner") as? NSDictionary {
            travelerOwner = Traveler.traveler(travelerDict.objectForKey("id") as! String)
            travelerOwner!.setInfo(travelerDict, complete: false)
        }
    }
    
    func sync() {
        //CoredataManager.sharedInstance.saveContext()
    }
    
    func save() {
        
        if (status == Status.syncing) {
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(5 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.save()
            }
        } else if (status == Status.delete || status == Status.deleting || status == Status.deleted) {
            //fatalError("PORRA VOCÊ DELETOU E TA SALVANDO DE NOVO SUA ANTA")
        } else {
            
            status = Status.edited
            set.addObjectAndNotify(self)
            //CoredataManager.sharedInstance.saveContext()
        }
        
    }
    
    func delete() {
        if (status == Status.syncing) {
            let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(5 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                self.delete()
            }
        } else if (status == Status.delete || status == Status.deleting || status == Status.deleted) {
            //fatalError("VOCÊ DELETOU E TA DELETANDO DE NOVO SUA ANTA")
        } else {
            
            
            status = Status.wait
            itinerary?.delete()
            
            status = Status.delete
            set.addObjectAndNotify(self)
            //CoredataManager.sharedInstance.saveContext()
            
        }
        
    }
    
    func setNil() {
    }
    
    func canSync() -> Bool {
        return travelerOwner?.id != nil
    }
    
    func getClass() -> String {
        return "Travel"
    }
    
    func json(complete:Bool) -> NSDictionary {
        
        let travel:NSMutableDictionary = NSMutableDictionary()
        
        if (complete) {
            travel["name"] = name
            travel["privacy"] = privacy as! Bool
            travel["travelerOwner"] = travelerOwner?.id
        }
        travel["id"] = id
        travel["synced"] = stringFromDate(synced)
        travel["infoSynced"] = stringFromDate(infoSynced)
        
        return travel
        
    }
}


