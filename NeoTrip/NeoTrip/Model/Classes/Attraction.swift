//
//  Attraction.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 10/26/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import Foundation
import CoreData

@objc enum AttractionType : Int {
    case OTHER
    case MUSEUM
    case SHOW
    case SHOP
    case BAR
    case BALLAD
    case RESTAURANT
    case TOUR
    case PARK
    case SPORT
    case TREKKING
    case CINEMA
    case EVENT
    case SKII
    case BEACH
    case THEATER
    case PARTY
    case COFFEE
}

class Attraction: Place {
    
    @NSManaged var subCategory: AttractionType
    
}

extension Attraction {
    
    convenience init () {
        let managedObjectContext: NSManagedObjectContext = CoredataManager.sharedInstance.managedObjectContext
        let entityDescription = NSEntityDescription.entityForName("Attraction", inManagedObjectContext: managedObjectContext)
        self.init(entity: entityDescription!, insertIntoManagedObjectContext: managedObjectContext)
        
        category = PlaceType.ATTRACTION
    }
    
    override func json(complete: Bool) -> NSDictionary {
        let place:NSMutableDictionary = super.json(complete) as! NSMutableDictionary
        place["subcategory"] = subCategory.rawValue
        return place
    }
    
}