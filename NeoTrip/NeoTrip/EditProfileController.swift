 //
 //  EditProfileController.swift
 //  NeoTrip
 //
 //  Created by Carlos Henrique Cayres on 1/20/16.
 //  Copyright © 2016 NeoTrip. All rights reserved.
 //
 
 import UIKit
 
 class EditProfileController: UIViewController, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource, UINavigationControllerDelegate, UIImagePickerControllerDelegate {
    
    @IBOutlet var headerView:UIView!
    @IBOutlet weak var headerImageView: UIImageView!
    @IBOutlet weak var avatarImageView: AvatarImageView!
    @IBOutlet weak var countriesTableView: UITableView!
    
    var coverImage : UIImage?
    var avatarImage : UIImage?
    
    // TextFields
    @IBOutlet weak var livingCityTextField: AutoCompleteTextField!
    @IBOutlet weak var firstNameTextField: UITextField!
    @IBOutlet weak var lastNameTextField: UITextField!
    
    var traveler : Traveler!
    var livingCity : City!
    var cities : [City] = []
    
    let imagePicker = UIImagePickerController()
    var currentView: UIImageView!
    
    var countriesISO:[String]!
    var selectedCountries:[String] = []
    
    lazy var queue:Queue = {
        var queue = Queue()
        queue.name = "search"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    
    class SearchLivingCity: NSOperation {
        
        let getType:SearchType!
        let query:String!
        let options:NSMutableDictionary?
        
        let delegate:Search
        
        init(request:NSDictionary, search:Search) {
            getType = SearchType(rawValue: request["type"] as! Int)!
            query = request["query"] as! String
            options = request["options"] as? NSMutableDictionary
            delegate = search
        }
        
        override func main() {
            
            if (self.cancelled) {
                return
            }
            
            options!["query"] = query
            let (_, cities) = databaseManager!.search(getType, options: options!)
            delegate.searchDelegate(cities as [AnyObject])
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        //Cria um Array com o ISO de cada pais
        countriesISO = NSLocale.ISOCountryCodes()
        
        countriesTableView.delegate = self
        countriesTableView.dataSource = self
        
        firstNameTextField.text = traveler.firstName
        lastNameTextField.text = traveler.lastName
        if (traveler.city != nil) {
            livingCityTextField.text = "\(traveler.city!.name!), \(traveler.city!.country!.name!)"
            livingCity = traveler.city
        }
        
        if (traveler.bgImage.image != nil) {
            headerImageView.image = UIImage(data: traveler.bgImage.image!)
        } else {
            headerImageView.image = UIImage(named: "background-travel")
        }
        if (traveler.avatar.image != nil) {
            avatarImageView.image = UIImage(data: traveler.avatar.image!)
        } else {
            avatarImageView.image = UIImage(named: "avatar-backpacker")
        }
        
        //Image Picker setups
        imagePicker.sourceType = .SavedPhotosAlbum
        imagePicker.allowsEditing = false
        imagePicker.delegate =  self
        //Gestures for the ImagesView
        let tapAvatarImage = UITapGestureRecognizer(target: self, action: Selector("choosingImage:"))
        let tapCoverImage = UITapGestureRecognizer(target: self, action: Selector("choosingImage:"))
        headerImageView.addGestureRecognizer(tapCoverImage)
        avatarImageView.addGestureRecognizer(tapAvatarImage)
        
        setupAutoComplete()
        handleTextFieldInterfaces()
        
        livingCityTextField.setDoneButton()
        
        //Delegates
        firstNameTextField.delegate = self
        lastNameTextField.delegate = self
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        if(traveler.countries?.ISOArray != nil){
            selectedCountries = (traveler.countries?.ISOArray)!
        }else{
            selectedCountries = ["BR"]
        }
        countriesTableView.reloadData()
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        traveler.countries = selectedCountries.ISOString
        traveler.save()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    //MARK: - Tap Action
    
    func choosingImage(sender: UITapGestureRecognizer){
        currentView = sender.view as! UIImageView
        presentViewController(imagePicker, animated: true, completion: nil)
    }
    
    //MARK: - ImagePickerDelegate
    
    func imagePickerController(picker: UIImagePickerController, didFinishPickingImage image: UIImage, editingInfo: [String : AnyObject]?) {
        currentView.image = image
        if currentView == avatarImageView{
            currentView.contentMode = .ScaleAspectFill
        }
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    func imagePickerControllerDidCancel(picker: UIImagePickerController) {
        dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Navigation
    
    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
        if (segue.identifier == "closeEditProfile") {
            //            let profile = segue.destinationViewController as! ProfileController
            //            profile.coverImage = self.headerImageView.image
            //            profile.avatarImage = self.avatarImageView.image
        }
    }
    
    // MARK: - Button Actions
    @IBAction func saveProfile(sender: UIButton) {
        traveler.firstName = firstNameTextField.text!
        traveler.lastName = lastNameTextField.text!
        if (livingCityTextField.text == nil || livingCityTextField.text == "") {
            livingCity = nil
        }
        traveler.city = livingCity
        
        traveler.avatar.image = UIImagePNGRepresentation(self.avatarImageView.image!)
        traveler.bgImage.image = UIImagePNGRepresentation(self.headerImageView.image!)
        
        traveler.save()
        
        //traveler.avatar.save()
        //traveler.bgImage.save()
        performSegueWithIdentifier("closeEditProfile", sender: self)
    }
    
    // MARK: - TableView Delegate e DataSource
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return countriesISO.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell  =  tableView.dequeueReusableCellWithIdentifier("CountryTableViewCell") as! CountryTableViewCell
        cell.countryISO = countriesISO[indexPath.row]
        
        if (selectedCountries.contains(countriesISO[indexPath.row])){
            cell.backgroundColor = ColorPalette.gray()
        } else {
            cell.backgroundColor =  ColorPalette.white()
        }
        
        cell.setInfos()
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        let cell = tableView.cellForRowAtIndexPath(indexPath) as! CountryTableViewCell
        if(!selectedCountries.contains(cell.countryISO)){
            selectedCountries.append(cell.countryISO)
            cell.backgroundColor = ColorPalette.gray()
        } else {
            selectedCountries.removeAtIndex(selectedCountries.indexOf(cell.countryISO)!)
            cell.backgroundColor = ColorPalette.white()
        }
        
        tableView.reloadData()
    }
    
    // MARK: - TextField Delegate Methods
    func textFieldDidEndEditing(textField: UITextField) {
        traveler.firstName = textField.text!
        traveler.lastName = textField.text!
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    // MARK: - Autocomplete
    func setupAutoComplete(){
        livingCityTextField.autoCompleteTextColor = ColorPalette.purple()
        livingCityTextField.autoCompleteTextFont = UIFont(name: "HelveticaNeue-Light", size: 12.0)
        livingCityTextField.autoCompleteCellHeight = 45.0
        livingCityTextField.maximumAutoCompleteCount = 20
        livingCityTextField.hidesWhenSelected = true
        livingCityTextField.hidesWhenEmpty = true
        livingCityTextField.enableAttributedText = true
        var attributes = [String:AnyObject]()
        attributes[NSForegroundColorAttributeName] = UIColor.blackColor()
        attributes[NSFontAttributeName] = UIFont(name: "HelveticaNeue-Bold", size: 12.0)
        livingCityTextField.autoCompleteAttributes = attributes
    }
    
    func handleTextFieldInterfaces(){
        livingCityTextField.onTextChange = {text in
            if !text.isEmpty{
                
                self.cities = City.search(text, queries: nil)
                var citiesString: [String] = []
                
                for city in self.cities {
                    if (city.region != nil){
                        citiesString.append("\(city.name!) - \(city.region!), \(city.country!.name!)")
                    } else {
                        citiesString.append("\(city.name!), \(city.country!.name!)")
                    }
                }
                
                self.livingCityTextField.autoCompleteStrings = citiesString
            } else {
                self.livingCity = nil
            }
        }
        
        livingCityTextField.onSelect = {text, indexpath in
            self.livingCity = self.cities[indexpath.row]
        }
    }
 }
 
 extension EditProfileController : Search {
    func searchDelegate(anyObject: [AnyObject]) {
        self.cities = anyObject as! [City]
        
        var citiesString: [String] = []
        
        for city in cities {
            citiesString.append("\(city.name!), \(city.country!.name!)")
        }
        
        dispatch_sync(dispatch_get_main_queue()) {
            self.livingCityTextField.autoCompleteStrings = citiesString
        }
        
    }
 }
 
