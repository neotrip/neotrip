//
//  InformationBoxView.swift
//  NeoTrip
//
//  Created by Lucas Padilha on 11/25/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import UIKit

class InformationBoxView: UIView {
    
    override func awakeFromNib() {
        super.awakeFromNib()
        let touchInformationBox: UITapGestureRecognizer = UITapGestureRecognizer()
        self.addGestureRecognizer(touchInformationBox)
    }
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
        super.drawRect(rect)
        
        self.layer.cornerRadius = 20
        self.layer.masksToBounds = true
    }
    
    

}
