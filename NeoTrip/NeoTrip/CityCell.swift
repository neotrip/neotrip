//
//  CityCell.swift
//  NeoTrip
//
//  Created by Joao Pedro Fabiano Franco on 12/15/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import UIKit

class CityCell: UITableViewCell {

    @IBOutlet weak var cityNameLabel: UILabel!
    
    func fill(city: City){
        cityNameLabel.text = city.name
    }
}
