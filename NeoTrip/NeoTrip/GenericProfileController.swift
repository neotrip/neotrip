//
//  GenericProfileController.swift
//  NeoTrip
//
//  Created by Lucas Padilha on 1/22/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

class GenericProfileController: ProfileController{
    
    @IBOutlet weak var followButton: CustomButton!
    @IBOutlet weak var blurEffect: UIVisualEffectView!
    @IBOutlet weak var visitedCountriesGeneric: UICollectionView!
    @IBOutlet weak var loadingView: UIView!
    
    var following : Bool = false
    
    let waitingIndicator = UIActivityIndicatorView(activityIndicatorStyle: .White)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        segueIdentifier = "showFollowGeneric"
        cellIdentifier = "genericFlagCell"
        
        visitedCountriesGeneric.delegate = self
        visitedCountriesGeneric.dataSource = self
        visitedCountriesGeneric.backgroundColor = UIColor.clearColor()
        
        self.loadingView.startLoadingAnimation(ColorPalette.purple())
        
        if(traveler.countries != nil){
            self.countriesArray = (traveler.countries?.ISOArray)!
        }else{
            self.countriesArray = ["BR"]
        }
        visitedCountriesGeneric.reloadData()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        /* if selected traveler is current user, hide follow button. */
        if traveler == databaseManager!.traveler {
            followButton.hidden = true
        }
        
        if(traveler.countries != nil){
            self.countriesArray = (traveler.countries?.ISOArray)!
        }
        
        visitedCountriesGeneric.reloadData()
    }
    
    override func refresh() {
        super.refresh()
        travels = Travel.search(traveler, option: TRAVELSOWNED)
        
        // AFTER GETTING INFO
        self.blurEffect.hidden = true
        self.loadingView.stopLoadingAnimation()
        
        self.following = databaseManager!.traveler!.following(self.traveler)
        
        if (self.following) {
            self.followButton.setColorBorder(UIColor.whiteColor())
            self.followButton.backgroundColor = ColorPalette.purple()
            self.followButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
            self.followButton.setTitle("Seguindo", forState: UIControlState.Normal)
        } else {
            self.followButton.setColorBorder(ColorPalette.purple())
            self.followButton.backgroundColor = UIColor.clearColor()
            self.followButton.setTitleColor(ColorPalette.purple(), forState: UIControlState.Normal)
            self.followButton.setTitle("Seguir", forState: UIControlState.Normal)
        }
        
        if(traveler.countries != nil){
            self.countriesArray = (traveler.countries?.ISOArray)!
        }

        travelsTableView.reloadData()
        visitedCountriesGeneric.reloadData()
    }
    
    // MARK: - Button Action
    @IBAction func tapFollowButton(sender: UIButton) {
        if (following) {
            followButton.enabled = false
            waitingIndicator.color = UIColor.whiteColor()
            followButton.startWaiting(waitingIndicator)
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                let result : Result = databaseManager!.unfollowTraveler(self.traveler)
                dispatch_sync(dispatch_get_main_queue()) {
                    if (result == Result.YES) {
                        self.following = false
                        self.followButton.setColorBorder(ColorPalette.purple())
                        self.followButton.backgroundColor = UIColor.clearColor()
                        self.followButton.setTitleColor(ColorPalette.purple(), forState: UIControlState.Normal)
                        self.followButton.setTitle("Seguir", forState: UIControlState.Normal)
                        
                        self.followButton.enabled = true
                        self.followButton.stopWaiting(self.waitingIndicator)
                    }
                }
            }
        }else {
            followButton.enabled = false
            waitingIndicator.color = ColorPalette.purple()
            followButton.startWaiting(waitingIndicator)
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                let result : Result = databaseManager!.followTraveler(self.traveler)
                dispatch_sync(dispatch_get_main_queue()) {
                    if (result == Result.YES) {
                        self.following = true
                        self.followButton.setColorBorder(UIColor.whiteColor())
                        self.followButton.backgroundColor = ColorPalette.purple()
                        self.followButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                        self.followButton.setTitle("Seguindo", forState: UIControlState.Normal)
                        
                        self.followButton.enabled = true
                        self.followButton.stopWaiting(self.waitingIndicator)
                    }
                }
            }
        }
    }
    
    @IBAction func backButton(sender: UIButton) {
        navigationController?.popViewControllerAnimated(true)
    }
    
}
