//
//  SearchController.swift
//  NeoTrip
//
//  Created by Lucas Padilha on 1/25/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

class SearchController: UIViewController {

    @IBOutlet weak var searchTextField: UITextField!
    
    @IBOutlet weak var searchTableView: UITableView!
    
    var searchedTravelers : [Traveler] = []
    var selectedTraveler : Traveler?
    
    var searchedTravels : [Travel] = []
    var selectedTravel : Travel?
    
    var loading : Bool = false
    
    lazy var queue:Queue = {
        var queue = Queue()
        queue.name = "search"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    
    class SearchClass: NSOperation {
        
        let getType:SearchType!
        let query:String!
        let options:NSMutableDictionary?
        
        let delegate:Search
        
        init(request:NSDictionary, search:Search) {
            getType = SearchType(rawValue: request["type"] as! Int)!
            query = request["query"] as! String
            options = request["options"] as? NSMutableDictionary
            delegate = search
        }
        
        override func main() {
            
            if (self.cancelled) {
                return
            }
            
            options!["query"] = query
            let (_, traveler) = databaseManager!.search(getType, options: options!)
            delegate.searchDelegate(traveler as [AnyObject])
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        searchTableView.delegate = self
        searchTableView.dataSource = self
        searchTextField.delegate = self
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
    
        searchTableView.reloadData()
    
        searchTextField.text = ""
        searchTextField.placeholder = "buscar por viajantes"
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        UIApplication.sharedApplication().statusBarStyle = .Default
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showGenericSearch") {
            let genericProfile : GenericProfileController = segue.destinationViewController as! GenericProfileController
            genericProfile.traveler = selectedTraveler!
        }
    }

}

// MARK: - TextFieldDelegate Methods
extension SearchController: UITextFieldDelegate {
    
    
    @IBAction func searchValueChanged(textField:UITextField) {
        self.loading = true
        self.searchTableView.reloadData()
        if (textField.text != nil && textField.text! != "") {
            let request = NSMutableDictionary()
            
            request["type"] = SearchType.TRAVELERS.rawValue
            request["query"] = textField.text!
            request["options"] = NSMutableDictionary()
            
            self.queue.addOperation(SearchClass(request: request, search: self))
        } else {
            self.searchedTravelers = []
            self.loading = false
            self.searchTableView.reloadData()
        }

    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}


// MARK: - TableView Methods
extension SearchController : UITableViewDataSource, UITableViewDelegate {
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (loading) {
            return searchedTravelers.count + 1
        }
        else {
            return searchedTravelers.count
        }
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        if (loading) {
            if (indexPath.row == 0) {
                let result: UITableViewCell!
                let loadingCell = tableView.dequeueReusableCellWithIdentifier("loadingCell", forIndexPath: indexPath) as! LoadingCell
                loadingCell.activityBegin()
                result = loadingCell
                return result
            }
            else {
                let result: UITableViewCell!
                
                let travelerCell = tableView.dequeueReusableCellWithIdentifier("searchTravelerCell", forIndexPath: indexPath) as! TravelerCell
                travelerCell.fillCell(databaseManager!.traveler!, traveler: searchedTravelers[indexPath.row-1])
                result = travelerCell
                
                return result
            }
        }
        else {
            let result: UITableViewCell!
            
            let travelerCell = tableView.dequeueReusableCellWithIdentifier("searchTravelerCell", forIndexPath: indexPath) as! TravelerCell
            travelerCell.fillCell(databaseManager!.traveler!, traveler: searchedTravelers[indexPath.row])
            result = travelerCell
            
            return result
        }
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 62
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (!loading) {
            selectedTraveler = searchedTravelers[indexPath.row]
            performSegueWithIdentifier("showGenericSearch", sender: self)
        }
        else {
            if (indexPath.row == 0) {
                
            }
            else {
                selectedTraveler = searchedTravelers[indexPath.row-1]
                performSegueWithIdentifier("showGenericSearch", sender: self)
            }
        }
    }
}

// MARK: - Search Methods
extension SearchController : Search {
    func searchDelegate(anyObject: [AnyObject]) {
        self.searchedTravelers = anyObject as! [Traveler]
        
        dispatch_async(dispatch_get_main_queue(), { () -> Void in
            self.loading = false
            self.searchTableView.reloadData()
        })
    }
}
