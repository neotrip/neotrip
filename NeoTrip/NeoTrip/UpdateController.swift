//
//  UpdateController.swift
//  NeoTrip
//
//  Created by Lucas Padilha on 2/5/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

class UpdateController: UIViewController, NSURLSessionDownloadDelegate {
    
    @IBOutlet weak var feedback: UILabel!
    @IBOutlet weak var updateTitle: UILabel!
    @IBOutlet weak var progressBar: UIProgressView!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    
    let defaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
    var task : NSURLSessionTask!
    
    var percentageWritten:Float = 0.0
    var taskTotalBytesWritten = 0
    var taskTotalBytesExpectedToWrite = 0
    
    var data:NSData!
    
    lazy var session : NSURLSession = {
        let config = NSURLSessionConfiguration.ephemeralSessionConfiguration()
        config.allowsCellularAccess = false
        let session = NSURLSession(configuration: config, delegate: self, delegateQueue: NSOperationQueue.mainQueue())
        return session
    }()
    
    // MARK: - Overrides
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if let bool = defaults.objectForKey("downloaded") as? Bool {
            if (bool) {
                // Ja foi baixado - segue
                finishUpdate()
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        if let bool = defaults.objectForKey("downloaded") as? Bool {
            if (bool) {
                // Ja foi baixado - segue
                finishUpdate()
            }
            else {
                // Ja entrou, mas nao baixou
                beginUpdate()
            }
        }
        else {
            // Primeira vez entrando no app
            defaults.setBool(false, forKey: "downloaded")
            defaults.synchronize()
            
            beginUpdate()
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
    }
    
    // MARK: - URLSession Methods
    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten writ: Int64, totalBytesExpectedToWrite exp: Int64) {
        taskTotalBytesWritten = Int(writ)
        taskTotalBytesExpectedToWrite = Int(exp)
        percentageWritten = Float(taskTotalBytesWritten) / Float(taskTotalBytesExpectedToWrite)
        
        progressBar.progress = percentageWritten
    }
    
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
        
        if (error != nil) {
            feedback.text = "aconteceu algum erro inesperado"
            feedback.textColor = ColorPalette.red()
        } else {
            
            feedback.text = "salvando atualizações"
            activityIndicator.hidden = false
            
            let delimiter:String = ";"
            let csv = CSV(data: data)
            
            var i:Int = 0
            let batch:Int = 1000
            
            var country:Country?
            csv!.nextLine()
            
            while let line = csv!.nextLine() {
                let values = line.componentsSeparatedByString(delimiter)
                
                if (country == nil || country!.id != values[1]) {
                    //country = Country.country(values[1])
                    country = Country()
                    country!.id = values[1]
                    country!.name = values[2]
                }
                
                autoreleasepool {
                    let id = Int(values[0])!
                    //let city:City = City.city(id)
                    let city:City = City()
                    city.id = id
                    city.name = values[4]
                    city.latitude = Double(values[5])
                    city.longitude = Double(values[6])
                    city.region = values[3]
                    city.country = country
                }
                
                if (batch < i++) {
                    
                    CoredataManager.sharedInstance.saveContext()
                    CoredataManager.sharedInstance.managedObjectContext.reset()
                    i = 0
                    
                    self.progressBar.progress = Float(csv!.percentageRead())
                    
                }
            }
            
            CoredataManager.sharedInstance.saveContext()
            CoredataManager.sharedInstance.managedObjectContext.reset()
            
            finishUpdate()
        }
        
    }
    
    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didFinishDownloadingToURL location: NSURL) {
        feedback.text = "salvando atualizações"
        activityIndicator.hidden = false
        data = NSData(contentsOfURL: location)!
        
        let fileManager = NSFileManager.defaultManager()
        do {
            try fileManager.removeItemAtURL(location)
        } catch let error as NSError {
            print("Ooops! Something went wrong: \(error)")
        }
    }
    
    // MARK: - Update Functions
    func beginUpdate() {
        updateTitle.hidden = false
        
        feedback.hidden = false
        feedback.text = "atualizando conteúdo das cidades"
        feedback.textColor = ColorPalette.yellow()
        
        progressBar.progress = 0
        progressBar.hidden = false
        
        let url:NSURL = NSURL(string: "http://www.neotripapp.com/database/EN_US.csv")!
        
        let request = NSURLRequest(URL:url)
        let task = self.session.downloadTaskWithRequest(request)
        self.task = task
        task.resume()
    }
    
    func finishUpdate () {
        updateTitle.hidden = true
        feedback.hidden = true
        progressBar.hidden = true
        activityIndicator.hidden = true
        
        defaults.setBool(true, forKey: "downloaded")
        defaults.synchronize()
        
        performSegueWithIdentifier("showLogin", sender: self)
    }
}
