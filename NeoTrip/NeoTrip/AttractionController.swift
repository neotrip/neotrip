//
//  AttractionController.swift
//  NeoTrip
//
//  Created by Lucas Padilha on 11/25/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import UIKit

class AttractionController: UIViewController, UITextFieldDelegate, CategoryDelegate{
    
    @IBOutlet weak var attractionTypeIcon: UIImageView!
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var dateTextField: UITextField!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var selectButton: UIButton!
    
    var place : Place!
    var itinerary : Itinerary!
    var attraction : Attraction?
    
    var name : String = ""
    var price : NSNumber = 0.0
    var date : NSDate!
    
    var category : AttractionType = AttractionType.OTHER
    
    var categoryView : CategoryView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Button layout
        selectButton.makeCircular(5)
        
        // Gestures
        // Do any additional setup after loading the view.
        let touchOutside: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("cancelAttraction:"))
        self.view.addGestureRecognizer(touchOutside)
        
        // Delegates
        nameTextField.delegate = self
        dateTextField.delegate = self
        priceTextField.delegate = self
        
        categoryView = UIView.loadFromNibNamed("CategoryView") as! CategoryView
        categoryView.delegate = self
        categoryView.makeMeAttraction()
        
        // Default
        if (attraction != nil) {
            if (attraction!.name != nil) {
                name = attraction!.name!
                nameTextField.text = name
            }
            
            if (attraction!.startTime != nil) {
                date = attraction!.startTime!
                let dateFormatter = NSDateFormatter()
                dateFormatter.dateStyle = .MediumStyle
                dateFormatter.timeStyle = NSDateFormatterStyle.NoStyle
                dateTextField.text = dateFormatter.stringFromDate(date)
            }
            
            if (attraction!.price != nil) {
                price = attraction!.price!
                priceTextField.text = price.stringValue
            }
            
            for c in categoryView.categories {
                if (c.type == attraction!.category.rawValue) {
                    selectButton.setTitle(c.name, forState: .Normal)
                    attractionTypeIcon.image = c.image!
                    category = AttractionType(rawValue: c.type)!
                }
            }
        }
        else {
            for c in categoryView.categories {
                if (c.type == 0) {
                    selectButton.setTitle(c.name, forState: .Normal)
                    attractionTypeIcon.image = c.image!
                    category = AttractionType(rawValue: c.type)!
                }
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        UIApplication.sharedApplication().statusBarStyle = .Default
    }
    
    // MARK: - Tap Gesture
    func cancelAttraction (recognizer: UITapGestureRecognizer) {
        performSegueWithIdentifier("closeCancelModalAttraction", sender: self)
    }
    
    //  MARK: - Action
    @IBAction func saveAttractions(sender: UIButton) {
        if (validAttraction()) {
            
            if (attraction == nil) {
                attraction = Attraction()
            }
            
            attraction!.price = price
            attraction!.name = name
            attraction!.startTime = date
            attraction!.interval = 1
            
            
            attraction!.subCategory = category
            attraction!.city = place.city
            attraction!.itinerary = itinerary
            
            attraction!.category = PlaceType.ATTRACTION
            
            attraction!.save()
            
            performSegueWithIdentifier("closeDoneModalAttraction", sender: self)
        }
        
    }
    
    // MARK: - TextField Delegate
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if (textField == dateTextField) {
            if (place != nil) {
                if (place.beginDate() != nil) {
                    if (place.endDate() != nil) {
                        textField.dateTextField(place.beginDate()!, max: place.endDate()!)
                    }
                    else {
                        textField.dateTextField(place.beginDate()!)
                    }
                }
                else {
                    textField.dateTextField((itinerary?.startTime)!)
                }
            }
        }
        
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if (textField == nameTextField) {
            self.name = textField.text!
        }
        if (textField == dateTextField) {
            if (textField.text! != "") {
                self.date = textField.text?.stringToDate
            }
        }
        if (textField == priceTextField) {
            if ((Double(textField.text!)) != nil) {
                self.price = (textField.text?.priceValue)!
            }
        }
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    // MARK: - Auxiliar Methods
    
    func validAttraction () -> Bool {
        if (name != "") {
            return true
        }
        else {
            let alert = UIAlertController(title: "Campos Vazios", message: "É preciso preencher todas os campos para salvar", preferredStyle: UIAlertControllerStyle.Alert)
            let defaultAction = UIAlertAction(title: "Entendido", style: .Default, handler: nil)
            alert.addAction(defaultAction)
            presentViewController(alert, animated: true, completion: nil)
//            let alert = UIAlertView()
//            alert.title = "Erro"
//            alert.message = "Parece que haver campos vazios"
//            alert.addButtonWithTitle("Entendido")
//            alert.show()
            return false
        }
    }
    
    @IBAction func categoryButton(sender: UIButton) {
        
        sender.setTitleColor(ColorPalette.purple(), forState: UIControlState.Normal)
        
        sender.enabled = false
        
        categoryView.frame.size =  CGSize(width: self.view.frame.size.width, height: self.view.frame.height/3)
        categoryView.frame.origin = CGPoint(x: 0 , y: self.view.frame.size.height)
        
        self.view.addSubview(categoryView)
        
        UIView.animateWithDuration(0.5) { () -> Void in
            self.categoryView.transform = CGAffineTransformMakeTranslation(0, -self.categoryView.frame.size.height*3/4)
            self.view.transform = CGAffineTransformMakeTranslation(0, -self.categoryView.frame.size.height/4)
        }
    }
    
    // MARK : - CategoryDelegate
    
    func selectedCategory(category: Category) {
        attractionTypeIcon.image = category.image!
        selectButton.setTitle(category.name, forState: .Normal)
        self.category = AttractionType(rawValue: category.type)!
    }
    
    func changeCategory(number:NSInteger,view:UIView) {
        
        UIView.animateWithDuration(0.3, animations: { () -> Void in
            view.transform = CGAffineTransformMakeTranslation(0, 0)
            self.view.transform = CGAffineTransformMakeTranslation(0, 0)
            }) { (Bool) -> Void in
                if(Bool == true){
                    view.removeFromSuperview()
                    self.selectButton.enabled = true
                }
        }
    }
    
}
