//
//  CitiesController.swift
//  NeoTrip
//
//  Created by Joao Pedro Fabiano Franco on 12/15/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import UIKit

class SearchOldController: UIViewController {
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var searchTableView: UITableView!
    
    let searchController = UISearchController(searchResultsController: nil)
    
    var array = []
    var filteredArray = []
    var type: SearchType!
    var selected: AnyObject!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Search Controller
        searchController.searchBar.delegate = self
        searchController.searchResultsUpdater = self
        
        searchController.hidesNavigationBarDuringPresentation = true
        searchController.dimsBackgroundDuringPresentation = true
        searchController.searchBar.sizeToFit()
        searchController.searchBar.placeholder = "Buscar"
        
        searchTableView.tableHeaderView = searchController.searchBar
        
        // Gesture
        let touchOutside: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("cancelSearch:"))
        self.backgroundView.addGestureRecognizer(touchOutside)

        // Load array
        if (type == .CITY) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                let dict = NSMutableDictionary()
                dict["query"] = ""
                
                let (_, array) = databaseManager!.search(.CITY, options:dict)
                self.array = array
                
                
                self.searchTableView.reloadData()
            }
        }
    }
    
    /* MARK -: Tap Gesture */
    func cancelSearch(recognizer: UITapGestureRecognizer){
        performSegueWithIdentifier("closeCancelModalSearch", sender: self)
    }
}

extension SearchOldController: UISearchBarDelegate, UISearchResultsUpdating{
    
    func updateSearchResultsForSearchController(searchController: UISearchController) {
        filterContentForSearchText(searchController.searchBar.text!)
    }
    
    func filterContentForSearchText(searchText: String, scope: String = "All") {
        filteredArray = array.filter { element in
            return element.lowercaseString.containsString(searchText.lowercaseString)
        }
        
        searchTableView.reloadData()
    }
    
}

extension SearchOldController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if searchController.active && searchController.searchBar.text != "" {
            return filteredArray.count
        }
        return array.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        var result: UITableViewCell!
        
        if (type == SearchType.CITY){
            let cityCell = tableView.dequeueReusableCellWithIdentifier("cityCell") as! CityCell
            
            if searchController.active && searchController.searchBar.text != "" {
                cityCell.fill(filteredArray[indexPath.row] as! City)
            } else {
                cityCell.fill(array[indexPath.row] as! City)
            }
            
            result = cityCell
        }
        
        return result
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        self.selected = array[indexPath.row]
    }
    
}
