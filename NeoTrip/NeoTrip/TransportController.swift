//
//  TransportController.swift
//  NeoTrip
//
//  Created by Joao Pedro Fabiano Franco on 10/19/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import UIKit

class TransportController: UIViewController, UITextFieldDelegate , CategoryDelegate {
    
    @IBOutlet weak var priceLabel: UILabel!
    @IBOutlet weak var transportstartDate: UITextField!
    @IBOutlet weak var transportPrice: UITextField!
    @IBOutlet weak var transportTypeIcon: UIImageView!
    @IBOutlet weak var selectButton: UIButton!
    
    var itinerary: Itinerary!
    var startNode: Place?
    var endedNode: Place?
    var transport: Transport?
    
    var category: TransportType = TransportType.NONE
    
    var date : NSDate?
    
    var inside : Bool = false
    
    var categoryView : CategoryView!
    
    var tutorial : Tutorial?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        //Button layout
        selectButton.makeCircular(5)
        
        // Delegate
        transportPrice.delegate = self
        transportstartDate.delegate = self
        
        let touchOutside: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("cancelTransport:"))
        self.view.addGestureRecognizer(touchOutside)
        
        categoryView = UIView.loadFromNibNamed("CategoryView") as! CategoryView
        categoryView.delegate = self
        categoryView.makeMeTransport()
        
        // Default
        if (transport != nil) {
            startNode = transport!.startPoint
            endedNode = transport!.endPoint
            
//            transportName.text = transport!.name
            
            if (transport!.startTime != nil){
                transportstartDate.text = transport!.startTime!.dateToString!
            }
            
            if (transport!.price != nil){
                transportPrice.text = transport!.price!.stringValue
            }
            
            for c in categoryView.categories {
                if (c.type == transport!.category.rawValue) {
                    selectButton.setTitle(c.name, forState: .Normal)
                    category = TransportType(rawValue: c.type)!
                    transportTypeIcon.image = c.image!
                }
            }
    
        }
        else {
            if (date != nil){
                transportstartDate.text = date!.dateToString
            }
            
            for c in categoryView.categories {
                if (c.type == 0) {
                    selectButton.setTitle(c.name, forState: .Normal)
                    category = TransportType(rawValue: c.type)!
                    transportTypeIcon.image = c.image!
                }
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        UIApplication.sharedApplication().statusBarStyle = .Default
    }
    
    /* MARK: - Tap Gesture */
    func cancelTransport(recognizer: UITapGestureRecognizer) {
        if (inside) {
            performSegueWithIdentifier("closeCancelModalTransportInside", sender: self)
        }
        else {
            performSegueWithIdentifier("closeCancelModalTransport", sender: self)
        }
    }
    
    /* MARK: - Action */
    @IBAction func saveTransport(sender: UIButton) {
        if (transportstartDate.text != "" && transportPrice.text != ""){
            if (transport == nil) {
                transport = Transport()
            }
            
//            transport!.name = transportName.text!
            transport!.price = NSNumber(double: transportPrice.text!.priceValue!)
            transport!.startPoint = startNode
            transport!.endPoint = endedNode
            transport!.itinerary = itinerary
            
            transport!.category = category
            
            if (date != nil) {
                transport!.startTime = date!
            }
            
            transport!.save()
            
            if (inside) {
                performSegueWithIdentifier("closeDoneModalTransportInside", sender: self)
            }
            else {
                performSegueWithIdentifier("closeDoneModalTransport", sender: self)
            }
        }
    }
    
    @IBAction func selectCategory(sender: UIButton) {
        
        sender.setTitleColor(ColorPalette.purple(), forState: UIControlState.Normal)
        
        sender.enabled = false
        
        categoryView.frame.size =  CGSize(width: self.view.frame.size.width, height: self.view.frame.height/3)
        categoryView.frame.origin = CGPoint(x: 0 , y: self.view.frame.size.height)
        
        self.view.addSubview(categoryView)
        
        UIView.animateWithDuration(0.5) { () -> Void in
            self.categoryView.transform = CGAffineTransformMakeTranslation(0, -self.categoryView.frame.size.height*3/4)
            self.view.transform = CGAffineTransformMakeTranslation(0, -self.categoryView.frame.size.height/4)
        }
    }
    
    // MARK: - Aux
//    func validTransport () -> Bool {
//        if (name != "") {
//            return true
//        }
//        else {
//            let alert = UIAlertView()
//            alert.title = "Erro"
//            alert.message = "Parece que haver campos vazios"
//            alert.addButtonWithTitle("Entendido")
//            alert.show()
//            return false
//        }
//    }
    
    // MARK: - Segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "closeDoneModalTransport" || segue.identifier == "closeCancelModalTransport"){
            let plannerController = segue.destinationViewController as! PlannerController
            plannerController.addedTransport = transport
            
            if (tutorial != nil) {
                if (transport != nil && tutorial!.index == 3) {
                    tutorial!.advanceLevel()
                }
            }
        }
    }
    
    // MARK : - CategoryDelegate
    func selectedCategory(category: Category) {
        transportTypeIcon.image = category.image!
        selectButton.setTitle(category.name, forState: .Normal)
        self.category = TransportType(rawValue: category.type)!
    }
    
    func changeCategory(number:NSInteger,view:UIView){
        
        UIView.animateWithDuration(0.3, animations: { () -> Void in
            view.transform = CGAffineTransformMakeTranslation(0, 0)
            self.view.transform = CGAffineTransformMakeTranslation(0, 0)
            }) { (Bool) -> Void in
                if(Bool == true){
                    view.removeFromSuperview()
                    self.selectButton.enabled = true
                }
        }
    }
    
    // MARK: - TextFieldDelegate
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if (textField.superview!.center.y - textField.center.y < 0){
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                textField.superview!.transform = CGAffineTransformMakeTranslation(0, self.view.center.y - textField.center.y )
            })
        }
        
        if(textField == transportstartDate){
            if (startNode!.endDate() != nil) {
                textField.dateTextField(startNode!.endDate()!)
            }
            else {
                textField.dateTextField((itinerary?.startTime)!)
            }
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            textField.superview!.transform = CGAffineTransformMakeTranslation(0, 0)
        })
        
        if (textField == transportstartDate) {
            if (textField.text! != "") {
                date = textField.text?.stringToDate
            }
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
}

extension TransportController: TutorialDelegate {
    func prepareTutorial(index: Int) {
        
    }
    
    func finishTutorial() {
        
    }
    
    func advanceLevel() {
        
    }
    
    func updateName() {
        
    }
}
