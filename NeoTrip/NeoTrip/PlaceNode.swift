//
//  CityView.swift
//  NeoTrip
//
//  Created by Joao Pedro Fabiano Franco on 10/19/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import UIKit
import MapKit

protocol placeNodeDelegate {
//    func showPlaceController(place: Place)
    func tapOnNode(placeNode: PlaceNode)
    func longPressOnNode(placeNode: PlaceNode)
    func panOnNode(placeNode: PlaceNode, recognizer: UIPanGestureRecognizer)
    func tapOnDelete(placeNode:PlaceNode)
    // root
    func tapOnRoot(placeNode:PlaceNode)
}

class PlaceNode: UIView {

    @IBOutlet weak var selectionView: UIView!
    
    @IBOutlet weak var iconView: UIView!
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var time: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var deleteView: UIView!
    
    var place: Place!
    
    var delegate: placeNodeDelegate!
    
    var animation : CABasicAnimation!
    
    var deleteMode: Bool = false
    var blockLong: Bool = false
    var initialPosition:CGPoint = CGPointMake(0, 0)
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let tap:UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("tapOnNode:"))
        self.addGestureRecognizer(tap)
        
        let longPress:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: Selector("longPressOnNode:"))
        self.addGestureRecognizer(longPress)
        
        let pan: UIPanGestureRecognizer = UIPanGestureRecognizer(target: self, action: Selector("panOnNode:"))
        self.addGestureRecognizer(pan)
        
        let tapDelete : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("tapOnDelete:"))
        deleteView.addGestureRecognizer(tapDelete)
            
        self.backgroundColor = UIColor.clearColor()
        self.selectionView.backgroundColor = UIColor.clearColor()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        iconView.makeCircle()
    }
    
    func tapOnNode(recognizer: UITapGestureRecognizer) {
        if (!deleteMode) {
            delegate.tapOnNode(self)
        }
        else {
            delegate.tapOnDelete(self)
        }
    }
    
    func longPressOnNode(recognizer: UILongPressGestureRecognizer) {
        if (recognizer.state == UIGestureRecognizerState.Began && !self.blockLong) {
            delegate.longPressOnNode(self)
            self.blockLong = true
        }
        if (recognizer.state == UIGestureRecognizerState.Ended ||
            recognizer.state == UIGestureRecognizerState.Cancelled ||
            recognizer.state == UIGestureRecognizerState.Failed) {
                self.blockLong = false
        }
    }
    
    func panOnNode (recognizer: UIPanGestureRecognizer) {
        delegate.panOnNode(self, recognizer: recognizer)
    }
    
    func tapOnDelete (recognizer: UITapGestureRecognizer) {
        delegate.tapOnDelete(self)
    }

    func changeDeleteMode (value: Bool) {
        self.deleteMode = value
        deleteView.hidden = !value
        if (!value) {
            stopShacking()
        }
        else {
            startShaking()
        }
    }
    
    func fillVertex(name: String, root: Bool, nDays: String, priceValue: CGFloat){
        self.name.text = name
        time.text = nDays
        price.text = String.cashString(priceValue)
        
        self.layoutIfNeeded()
    }
    
    func startShaking() {
        animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.1
        animation.repeatCount = Float.infinity
        animation.autoreverses = true
        animation.fromValue = NSValue(CGPoint: CGPointMake(self.center.x - 2, self.center.y))
        animation.toValue = NSValue(CGPoint: CGPointMake(self.center.x + 2, self.center.y))
        self.layer.addAnimation(animation, forKey: "position")
    }
    
    func stopShacking() {
        self.layer.removeAllAnimations()
    }
    
    func setSize(wh: CGFloat){
        var newFrame = self.frame
        newFrame.size.width = wh
        newFrame.size.height = wh
        self.frame = newFrame
        
        self.setNeedsDisplay()
        self.setNeedsLayout()
    }

    func setPosition(parentView: UIView, position: CGPoint){
        self.center = position
        self.initialPosition = position
    }
    
    func changeColor(color: UIColor) {
        self.iconView.backgroundColor = color
    }
}