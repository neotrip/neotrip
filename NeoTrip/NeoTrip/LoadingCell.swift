//
//  LoadingCell.swift
//  NeoTrip
//
//  Created by Lucas Padilha on 2/4/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

class LoadingCell: UITableViewCell {

    @IBOutlet weak var activity: UIActivityIndicatorView!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

    func activityBegin() {
        activity.startAnimating()
    }
}
