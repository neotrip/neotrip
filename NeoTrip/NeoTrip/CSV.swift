import Foundation

public class CSV {
    let chunkSize : Int = 4096
    
    //var fileHandle : NSFileHandle!
    let inputStream:NSInputStream
    let buffer:NSMutableData
    let delimData : NSData = "\n".dataUsingEncoding(encoding)!
    var atEof : Bool = false
    var buf:[UInt8]
    
    let dataSize:Int
    var readData:Int = 0
    init?(data: NSData) {
        dataSize = data.length
        
        buffer = NSMutableData(capacity: chunkSize)!
        inputStream = NSInputStream(data: data)
        buf = [UInt8](count: chunkSize, repeatedValue: 0)
    
        inputStream.open()
    }
    
    deinit {
        self.close()
    }
    
    func percentageRead() -> Double {
        return Double(readData) / Double(dataSize)
    }
    
    /// Return next line, or nil on EOF.
    func nextLine() -> String? {
        if atEof {
            return nil
        }
        
        // Read data chunks from file until a line delimiter is found:
        var range = buffer.rangeOfData(delimData, options: [], range: NSMakeRange(0, buffer.length))
        while range.location == NSNotFound {
            let bytesRead = inputStream.read(&buf, maxLength: buf.capacity)
            if bytesRead == 0 {
                // EOF or read error.
                atEof = true
                if buffer.length > 0 {
                    // Buffer contains last line in file (not terminated by delimiter).
                    let line = NSString(data: buffer, encoding: encoding)
                    
                    buffer.length = 0
                    return line as String?
                }
                // No more lines.
                return nil
            }
            buffer.appendData(NSData(bytes: buf, length: bytesRead))
            range = buffer.rangeOfData(delimData, options: [], range: NSMakeRange(0, buffer.length))
        }
        
        // Convert complete line (excluding the delimiter) to a string:
        let line = NSString(data: buffer.subdataWithRange(NSMakeRange(0, range.location)),
            encoding: encoding)
        // Remove line (and the delimiter) from the buffer:
        buffer.replaceBytesInRange(NSMakeRange(0, range.location + range.length), withBytes: nil, length: 0)
        
        readData += range.location + range.length
        
        return line as String?
    }
    
    /// Close the underlying file. No reading must be done after calling this method.
    func close() -> Void {
        inputStream.close()
    }
    
}
