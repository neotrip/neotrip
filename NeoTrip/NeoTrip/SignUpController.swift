//
//  SignUpController.swift
//  NeoTrip
//
//  Created by Joao Pedro Fabiano Franco on 11/4/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import UIKit

class SignUpController: UIViewController, UITextFieldDelegate{
    var traveler:Traveler?
    
    @IBOutlet weak var firstName: UITextField!
    @IBOutlet weak var lastName: UITextField!
    @IBOutlet weak var username: UITextField!
    @IBOutlet weak var password: UITextField!
    @IBOutlet weak var repassword: UITextField!
    @IBOutlet weak var email: UITextField!
    
    @IBOutlet weak var feedback: UILabel!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.feedback.hidden = true
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if (self.view.center.y - textField.center.y < 0){
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.view.transform = CGAffineTransformMakeTranslation(0, self.view.center.y - textField.center.y )
            })
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.view.transform = CGAffineTransformMakeTranslation(0, 0)
        })
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showApp"){
//            let tabBarController = segue.destinationViewController as! TabBarController
//            tabBarController.traveler = traveler
        }
    }

    @IBAction func signup(sender: AnyObject) {
        self.startSignUp()
        
        if password.text != repassword.text {
            self.stopSignUp()
            feedback.hidden = false
            feedback.text = "senhas diferentes"
        }
        else if (password.text!.lengthOfBytesUsingEncoding(encoding) < 4) {
            self.stopSignUp()
            feedback.hidden = false
            feedback.text = "senha deve ter mais de 4 caracteres"
        }
        else {
            let user:User = User()
            traveler = Traveler()
            
            user.id = username.text
            user.password = password.text
            
            traveler!.id = user.id
            traveler!.firstName = firstName.text
            traveler!.lastName = lastName.text
            traveler!.mail = email.text
        
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                let result:Result = databaseManager!.signup(self.traveler!, withUser: user)
                switch (result) {
                case .YES:
                    
                    dispatch_async(dispatch_get_main_queue()){
                        self.stopSignUp()
                        self.feedback.hidden = true
                        self.performSegueWithIdentifier("showApp", sender: self)
                    }
                    break
                
                case .NO:

                    dispatch_sync(dispatch_get_main_queue()){
                        self.stopSignUp()
                        self.feedback.hidden = false
                        self.feedback.text = "usuário já existe ou inválido"
                    }
                    break
                    
                case .DEPRECATED:
                    
                    dispatch_sync(dispatch_get_main_queue()){
                        self.stopSignUp()
                        self.feedback.hidden = false
                        self.feedback.text = "atualize o app!"
                    }
                    break
                    
                    
                case .UNCONNECTED:
                    
                    dispatch_sync(dispatch_get_main_queue()){
                        self.stopSignUp()
                        self.feedback.hidden = false
                        self.feedback.text = "servidor fora do ar"
                    }
                    break
                    
                default:
                    
                    break
                
                }
            }
        }
    }
    
    @IBAction func backLogin(){
        performSegueWithIdentifier("closeCancelSignUp", sender: self)
    }
    
    
    // MARK: - Aux
    func startSignUp(){
        self.view.startLoadingAnimation(UIColor.whiteColor())
        firstName.hidden = true
        lastName.hidden = true
        username.hidden = true
        password.hidden = true
        repassword.hidden = true
        email.hidden = true
    }
    
    func stopSignUp(){
        self.view.stopLoadingAnimation()
        firstName.hidden = false
        lastName.hidden = false
        username.hidden = false
        email.hidden = false
        password.hidden = false
        repassword.hidden = false
    }
}
