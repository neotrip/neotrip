//
//  RootNode.swift
//  NeoTrip
//
//  Created by Lucas Padilha on 1/21/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

class RootNode: PlaceNode {
    
    override func tapOnNode(recognizer: UITapGestureRecognizer) {
        delegate.tapOnRoot(self)
    }
    
    override func longPressOnNode(recognizer: UILongPressGestureRecognizer) {
        if (recognizer.state == UIGestureRecognizerState.Began && !self.blockLong) {
            delegate.longPressOnNode(self)
            self.blockLong = true
        }
        if (recognizer.state == UIGestureRecognizerState.Ended ||
            recognizer.state == UIGestureRecognizerState.Cancelled ||
            recognizer.state == UIGestureRecognizerState.Failed) {
                self.blockLong = false
        }
    }
    
    override func panOnNode (recognizer: UIPanGestureRecognizer) {
        delegate.panOnNode(self, recognizer: recognizer)
    }
    
    override func tapOnDelete(recognizer: UITapGestureRecognizer) {
        // do nothing
    }
    
    override func changeDeleteMode(value: Bool) {
        // do nothing
    }
    
    override func changeColor(color: UIColor) {
        // do nothing
    }
    
    func fillRoot(name: String){
        self.name.text = name
        
        self.layoutIfNeeded()
    }

}
