//
//  CountryTableViewCell.swift
//  NeoTrip
//
//  Created by Carlos Henrique Cayres on 2/1/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

class CountryTableViewCell: UITableViewCell {
    
    @IBOutlet weak var countryImage: UIImageView!
    @IBOutlet weak var countryName: UILabel!
    @IBOutlet weak var countryContinent: UILabel!
    
    var countryISO:String!
    let check = UIView()
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        check.frame.size = CGSize(width: rect.height*0.3, height: rect.height*0.3)
        check.center = CGPoint(x: rect.width + rect.height/2, y: rect.height/2)
        check.backgroundColor = ColorPalette.green()
        check.makeCircle()
        self.addSubview(check)
    }
    
    func setInfos(){
        if (countryISO != nil) {
            let name = NSLocale.systemLocale().displayNameForKey(NSLocaleCountryCode, value: countryISO)
            countryImage.image = UIImage(named: countryISO.lowercaseString)
            countryName.text = name
            countryContinent.text = countryISO
        }
    }
    
}
