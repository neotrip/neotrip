//
//  Graph.swift
//  NeoTrip
//
//  Created by Joao Pedro Fabiano Franco on 10/20/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import UIKit
import MapKit
import Darwin

class Graph: UIScrollView, UIScrollViewDelegate {
    var placesNode: [PlaceNode]! = []
    var transportsNode: [TransportNode]! = []
    
    var itinerary: Itinerary!
    var planner : PlannerController?
    var plannerInside : PlannerInsideController?
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    // MARK: - Edge
    func removeEdges(){
        for subview in self.subviews{
            if (subview is TransportNode){
                subview.removeFromSuperview()
            }
        }
        
        transportsNode = []
    }
    
    func removeTemporaryEdges(){
        if (self.layer.sublayers != nil){
            for layer in self.layer.sublayers!{
                if layer is Arrow{
                    layer.removeFromSuperlayer()
                }
            }
        }
    }
    
    func setArrow(point1: CGPoint, point2: CGPoint) -> Arrow{
        let arrowPath = Arrow()
        arrowPath.fillColor = ColorPalette.blue().CGColor
        arrowPath.path = Arrow.bezierPathWithArrowFromPoint(point1, endPoint: point2, tailWidth: 2, headWidth: 10, headLength: 16).CGPath
        
        return arrowPath
    }
    
    func addNewEdge(place1: UIView, place2: UIView, transport: Transport?){
        let point1: CGPoint
        let point2: CGPoint
        (point1, point2) = whichQuad(place1, v2: place2)
        
        let new = TransportNode()
        new.transport = transport
        new.startPoint = point1
        new.endPoint = point2
        
        // atribuindo para o no a planner controller como delegate
        if (planner != nil) {
            new.transportNodeDelegate = planner!
        } else if (plannerInside != nil) {
            new.transportNodeDelegate = plannerInside!
        }
        
        new.setView()
        
        self.addSubview(new)
        self.transportsNode.append(new)
        
        
        self.setNeedsLayout()
    }
    
    //MARK: - Vertex
    func addNewVertex(place: Place, size: CGFloat, position: CGPoint, color: UIColor){
        if (place.category == .ROOT) {
            let rootNode = NSBundle.mainBundle().loadNibNamed("RootNode", owner: self, options: nil).first as! RootNode
            
            rootNode.place = place
            rootNode.setSize(size)
            rootNode.setPosition(self, position: position)
            rootNode.iconView.backgroundColor = color
            
            rootNode.fillRoot(place.city!.name!)
            
            if (planner != nil) {
                rootNode.delegate = planner!
            }
            else if (plannerInside != nil) {
                rootNode.delegate = plannerInside!
            }
            
            self.addSubview(rootNode)
            self.placesNode.append(rootNode)
            
            self.setNeedsLayout()
        }
        else {
            let placeNode = NSBundle.mainBundle().loadNibNamed("PlaceNode", owner: self, options: nil).first as! PlaceNode
            
            placeNode.place = place
            placeNode.setSize(size)
            placeNode.setPosition(self, position: position)
            placeNode.iconView.backgroundColor = color
            
            switch place {
                case is Port:
                    placeNode.fillVertex(place.name!, root: false, nDays: place.interval!.stringValue, priceValue: CGFloat(place.getPrice()))
                case is Accomodation:
                    placeNode.fillVertex(place.name!, root: false, nDays: place.interval!.stringValue, priceValue: CGFloat(place.getPrice()))
                case is Attraction:
                    placeNode.fillVertex(place.name!, root: false, nDays: place.interval!.stringValue, priceValue: CGFloat(place.getPrice()))
                default:
                    placeNode.fillVertex(place.city!.name!, root: false, nDays: place.interval!.stringValue, priceValue: CGFloat(place.sumPrices()))
                }
            
            if (planner != nil) {
                placeNode.delegate = planner!
            }
            else if (plannerInside != nil) {
                placeNode.delegate = plannerInside!
            }
            
            self.addSubview(placeNode)
            self.placesNode.append(placeNode)
            
            self.setNeedsLayout()
        }

        
    }
    
    func vertexCenterProporcional(coord: CLLocationCoordinate2D) -> CGPoint{
        var lat: CGFloat = CGFloat(coord.latitude)
        var long: CGFloat = CGFloat(coord.longitude)
        
        lat = ((-lat+90)*self.contentSize.height)/180
        long = ((long+180)*self.contentSize.width)/360
        
        return CGPointMake(long, lat)
    }
    
    // MARK: - Graph
    func generateGraph(){
        removeVertexs()
        
        if itinerary == nil {
            fatalError("itinerary nil. why?")
        }
        
        let cities = itinerary.cities()
        for city in cities! {
            if (city.latitude == nil){
                if let placeOfCity = itinerary.placeThatRepresents(city) {
                    addNewVertex(placeOfCity, size: VERTEX_SIZE, position: newCenter(), color: ColorPalette.purple())
                }
            } else {
                if let placeOfCity = itinerary.placeThatRepresents(city) {
                    addNewVertex(placeOfCity, size: VERTEX_SIZE, position: vertexCenterProporcional(CLLocationCoordinate2D(latitude: city.latitude!.doubleValue, longitude: city.longitude!.doubleValue)), color: ColorPalette.purple())
                }
            }
            
            
            repositionClosedPlaces()
            
            self.setNeedsDisplay()
        }
    }
    
    func repositionTransport(){
        let transports = itinerary.transports!.allObjects as! [Transport]
        
        /* ineficiente */
        for transport in transports{
            var placeNode1: PlaceNode!
            var placeNode2: PlaceNode!
            
            for placeNode in placesNode{
                if (placeNode.place.category != .PORT && placeNode.place.category != .ATTRACTION && placeNode.place.category != .ACCOMODATION){
                    if (placeNode.place.city! == transport.startPoint!.city!){
                        placeNode1 = placeNode
                    } else if (placeNode.place.city! == transport.endPoint!.city!){
                        placeNode2 = placeNode
                    }
                    
                    if (placeNode1 != nil && placeNode2 != nil){
                        break
                    }
                } else {
                    if (placeNode.place == transport.startPoint){
                        placeNode1 = placeNode
                    } else if (placeNode.place == transport.endPoint){
                        placeNode2 = placeNode
                    }
                    
                    if (placeNode1 != nil && placeNode2 != nil){
                        break
                    }
                }
            }
            
            if (placeNode1 != nil && placeNode2 != nil){
                switch (transport.status) {
                case .wait, .delete, .deleting, .deleted:
                    break
                default:
                    addNewEdge(placeNode1, place2: placeNode2, transport: transport)
                    break
                }
            }
        }
    }
    
    func repositionClosedPlaces(){
        if (placesNode.count > 1){
            var queue = [placesNode.first!]
            if let root = itinerary!.root(){
                for pn in placesNode{
                    if (pn.place == root){
                        queue = [pn]
                        break;
                    }
                }
            }
            var visited: [PlaceNode] = []
            
            while (visited.count != placesNode.count){
                var currentPlaceNode: PlaceNode!
                
                if (queue.count == 0){
                    let visitedSet = Set(visited)
                    let placesSet = Set(placesNode)
                    let notVisitedSet = placesSet.subtract(visitedSet)
                    let notVisited = Array(notVisitedSet)
                    
                    queue.append(notVisited.first!)
                }
                
                currentPlaceNode = queue.first!
                queue.removeFirst()
                
                if (!visited.contains(currentPlaceNode)){
                    visited.append(currentPlaceNode)
                }
                
                let minSortedPlacesNode = placesNode.sort({
                    let dist1 = abs(UIView.dist(p1: currentPlaceNode.center, p2: $0.center))
                    let dist2 = abs(UIView.dist(p1: currentPlaceNode.center, p2: $1.center))
                    return dist1 < dist2
                })
                
                for placeNode in minSortedPlacesNode{
                    if (placeNode != currentPlaceNode){
                        let distValue = abs(UIView.dist(p1: currentPlaceNode.center, p2: placeNode.center))
                        
                        if (distValue < COOLDIST - 1){
                            if (!queue.contains(placeNode)){
                                queue.append(placeNode)
                            }
                            
                            let p1 = currentPlaceNode!
                            let p2 = placeNode
                            let p1x = p1.center.x
                            let p2x = p2.center.x
                            let p1y = p1.center.y
                            let p2y = p2.center.y
                            let deltaX = abs(p1x-p2x)
                            let deltaY = abs(p1y-p2y)
                            let dist = sqrt(deltaX*deltaX + deltaY*deltaY)
                            let proportional = (COOLDIST + dist)/dist
                            
                            let newDeltaX = deltaX*proportional
                            let newDeltaY = deltaY*proportional
                            
                            if (p2.center.x >= p1.center.x && p2.center.y < p1.center.y){
                                placeNode.setPosition(self, position: CGPoint(x: p1x + newDeltaX, y: p1y - newDeltaY))
                            } else if (p2.center.x < p1.center.x && p2.center.y < p1.center.y){
                                placeNode.setPosition(self, position: CGPoint(x: p1x - newDeltaX, y: p1y - newDeltaY))
                            } else if (p2.center.x < p1.center.x && p2.center.y >= p1.center.y){
                                placeNode.setPosition(self, position: CGPoint(x: p1x - newDeltaX, y: p1y + newDeltaY))
                            } else if (p2.center.x >= p1.center.x && p2.center.y >= p1.center.y){
                                placeNode.setPosition(self, position: CGPoint(x: p1x + newDeltaX, y: p1y + newDeltaY))
                            }
                        }
                    }
                }
                
                
            }
        }
        
        self.setNeedsDisplay()
    }
    
    func generateGraphInside(city: City){
        removeVertexs()
        
        let accommodations = itinerary.accommodationsIn(city)
        let attractions = itinerary.attractionsIn(city)
        let ports = itinerary.portsIn(city)
        
        let width = self.bounds.size.width
        let a = self.center.x
        let b = self.center.y
        
        let radiusAtt = 0.5*width/2
        let radiusAcc = 0.1*width/2
        let radiusPort = 0.3*width/2
        
        let countAtt = attractions.count
        let countAcc = accommodations.count
        let countPort = ports.count
        
        for attraction in attractions{
            let index = attractions.indexOf(attraction)
            let angle = CDouble(index!*360/countAtt)
            let angleRadius = angle*M_PI/180
            
            let cosino: CGFloat = CGFloat(cos(angleRadius))
            let sino: CGFloat = CGFloat(sin(angleRadius))
            
            let x = radiusAtt*cosino + a
            let y = -radiusAtt*sino + b
            
            addNewVertex(attraction, size: VERTEX_SIZE_AA, position: CGPoint(x: x, y: y), color: ColorPalette.yellow())
        }
        
        for accommodation in accommodations{
            let index = accommodations.indexOf(accommodation)
            let angle = CDouble(index!*360/countAcc)
            let angleRadius = angle*M_PI/180
            
            let cosino: CGFloat = CGFloat(cos(angleRadius))
            let sino: CGFloat = CGFloat(sin(angleRadius))
            
            let x = radiusAcc*cosino + a
            let y = -radiusAcc*sino + b
            
            addNewVertex(accommodation, size: VERTEX_SIZE_AA, position: CGPoint(x: x, y: y), color: ColorPalette.red())
        }
        
        for port in ports{
            let index = ports.indexOf(port)
            let angle = CDouble(index!*360/countPort)
            let angleRadius = angle*M_PI/180
            
            let cosino: CGFloat = CGFloat(cos(angleRadius))
            let sino: CGFloat = CGFloat(sin(angleRadius))
            
            let x = radiusPort*cosino + a
            let y = -radiusPort*sino + b
            
            addNewVertex(port, size: VERTEX_SIZE_AA, position: CGPoint(x: x, y: y), color: ColorPalette.lightBlue())
        }
        
        
        removeEdges()
        repositionTransport()
        
        self.setNeedsDisplay()
    }
    
    
    func removeVertexs(){
        for vertex in self.subviews{
            if (vertex is PlaceNode){
                vertex.removeFromSuperview()
            }
        }
        placesNode = []
    }
    
    // MARK: - Aux
    func newCenter() -> CGPoint{
        var x = CGFloat(arc4random_uniform(UInt32(self.contentSize.width)))
        var y = CGFloat(arc4random_uniform(UInt32(self.contentSize.height)))
        var far = false
        
        while (!far){
            var again = false
            for PlaceNode in self.placesNode{
                if (UIView.dist(p1: PlaceNode.center, p2: CGPointMake(x, y)) < 100){
                    x = CGFloat(arc4random_uniform(UInt32(self.contentSize.width)))
                    y = CGFloat(arc4random_uniform(UInt32(self.contentSize.height)))
                    again = true
                    break;
                }
            }
            
            if (!again){
                far = true
            }
        }
        
        return CGPointMake(x, y)
    }
    
    func minDistPlacesNodes() -> CGFloat{
        var min = INFINITY
        
        if (placesNode.count > 1){
            for i in placesNode{
                for j in placesNode{
                    if (i != j){
                        let distValue = abs(UIView.dist(p1: i.center, p2: j.center))
                        if (distValue < min){
                            min = distValue
                        }
                    }
                }
            }
            
            return min
        } else {
            return 0
        }
    }
    
    func maxDistPlacesNodes() -> CGFloat{
        var max = ZERO
        
        for i in placesNode{
            for j in placesNode{
                if (i != j){
                    let distValue = abs(UIView.dist(p1: i.center, p2: j.center))
                    if (distValue > max){
                        max = distValue
                    }
                }
            }
        }
        
        return max
    }

}