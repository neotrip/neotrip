//
//  CategoryView.swift
//  NeoTrip
//
//  Created by Carlos Henrique Cayres on 1/12/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

protocol CategoryDelegate{
    
    func changeCategory(number:NSInteger,view:UIView)
//    func selectedImage(name:String)
//    func selectedName(name:String)
//    func selectedIndex(index: Int)
    func selectedCategory(category: Category)
    
}

class Category {
    var name: String = ""
    var image: UIImage?
    var type: Int = 0
    
    init(name: String, image: UIImage, type: Int) {
        self.name = name
        self.image = image
        self.type = type
    }
}

class CategoryView: UIView , UIPickerViewDelegate{
    
    @IBOutlet weak var categoryPickerView: UIPickerView!
    @IBOutlet weak var selectedButton: UIButton!
    
    var delegate: CategoryDelegate!
    
    var categories: [Category] = []
    var selectedIndex: Int!
    
    override func awakeFromNib() {
        
        selectedButton.tintColor = ColorPalette.purple()
        categoryPickerView.delegate = self
        selectedIndex = 0

    }
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        
        delegate.selectedCategory(categories[selectedIndex])
    }
    
    @IBAction func confirmed(sender: AnyObject){
        
        delegate.changeCategory(selectedIndex, view: self)
        self.removeFromSuperview()
    }

    func makeMeTransport(){
        categories = []
        
        categories.append(Category(name: "Sem tipo", image: UIImage(named: "neotrip-logo")!, type: TransportType.NONE.rawValue))
        categories.append(Category(name: "Avião", image: UIImage(named: "airplane")!, type: TransportType.AIRPLANE.rawValue))
        categories.append(Category(name: "Navio", image: UIImage(named: "ship")!, type: TransportType.SHIP.rawValue))
        categories.append(Category(name: "Trem", image: UIImage(named: "train")!, type: TransportType.TRAIN.rawValue))
        categories.append(Category(name: "Metrô", image: UIImage(named: "subway")!, type: TransportType.SUBWAY.rawValue))
        categories.append(Category(name: "Ônibus", image: UIImage(named: "bus")!, type: TransportType.BUS.rawValue))
        categories.append(Category(name: "Taxi", image: UIImage(named: "taxi")!, type: TransportType.TAXI.rawValue))
        categories.append(Category(name: "Carona", image: UIImage(named: "ride")!, type: TransportType.RIDE.rawValue))
        categories.append(Category(name: "Carro", image: UIImage(named: "car")!, type: TransportType.CAR.rawValue))
        categories.append(Category(name: "Bicicleta", image: UIImage(named: "bicycle")!, type: TransportType.BICYCLE.rawValue))
        categories.append(Category(name: "A pé", image: UIImage(named: "foot")!, type: TransportType.FOOT.rawValue))
        categories.append(Category(name: "Uber", image: UIImage(named: "uber")!, type: TransportType.UBER.rawValue))
        
        categoryPickerView.reloadAllComponents()
    }
    
    func makeMeAttraction(){
        categories = []
        
        categories.append(Category(name: "Sem tipo", image: UIImage(named: "neotrip-logo")!, type: AttractionType.OTHER.rawValue))
        categories.append(Category(name: "Museu", image: UIImage(named: "museum")!, type: AttractionType.MUSEUM.rawValue))
        categories.append(Category(name: "Show", image: UIImage(named: "show")!, type: AttractionType.SHOW.rawValue))
        categories.append(Category(name: "Compras", image: UIImage(named: "shop")!, type: AttractionType.SHOP.rawValue))
        categories.append(Category(name: "Bar", image: UIImage(named: "bar")!, type: AttractionType.BAR.rawValue))
        categories.append(Category(name: "Balada", image: UIImage(named: "ballad")!, type: AttractionType.BALLAD.rawValue))
        categories.append(Category(name: "Restaurante", image: UIImage(named: "restaurant")!, type: AttractionType.RESTAURANT.rawValue))
        categories.append(Category(name: "Passeio Turístico", image: UIImage(named: "tour")!, type: AttractionType.TOUR.rawValue))
        categories.append(Category(name: "Parque de Diversão", image: UIImage(named: "park")!, type: AttractionType.PARK.rawValue))
        categories.append(Category(name: "Esportes", image: UIImage(named: "sport")!, type: AttractionType.SPORT.rawValue))
        categories.append(Category(name: "Trilha", image: UIImage(named: "trekking")!, type: AttractionType.TREKKING.rawValue))
        categories.append(Category(name: "Cinema", image: UIImage(named: "cinema")!, type: AttractionType.CINEMA.rawValue))
        categories.append(Category(name: "Evento", image: UIImage(named: "event")!, type: AttractionType.EVENT.rawValue))
        categories.append(Category(name: "Skii", image: UIImage(named: "skii")!, type: AttractionType.SKII.rawValue))
        categories.append(Category(name: "Praia", image: UIImage(named: "beach")!, type: AttractionType.BEACH.rawValue))
        categories.append(Category(name: "Teatro", image: UIImage(named: "theater")!, type: AttractionType.THEATER.rawValue))
        categories.append(Category(name: "Festa", image: UIImage(named: "party")!, type: AttractionType.PARTY.rawValue))
        categories.append(Category(name: "Café", image: UIImage(named: "coffee")!, type: AttractionType.COFFEE.rawValue))
        
        categoryPickerView.reloadAllComponents()
    }
    
    func makeMePort(){
    
        
    }
    
    //  MARK: PickerView Delegate
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return pickerView.bounds.height/4
    }
    
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int{
        return 1
    }

    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return categories.count
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        
        let height = pickerView.bounds.height/4

        let myView = UIView(frame: CGRectMake(0, 0, pickerView.bounds.width - 30, height))
        let myImageView = UIImageView(frame: CGRectMake(0, 0, height*4/5, height*4/5))
        let myLabel = UILabel(frame: CGRectMake(height*4/5 + 8, 0, pickerView.bounds.width - height*4/5, height*4/5 ))
        
        myImageView.image = categories[row].image!
        myLabel.text = categories[row].name
        
        myView.addSubview(myLabel)
        myView.addSubview(myImageView)
        
        return myView
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int){
        delegate.selectedCategory(categories[row])
    }
    
}