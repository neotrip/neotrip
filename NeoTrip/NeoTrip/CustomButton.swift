//
//  VIPButton.swift
//  NeoTrip
//
//  Created by Lucas Padilha on 1/8/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

class CustomButton: UIButton {

    override func awakeFromNib() {
        
        self.layer.cornerRadius = 5.0
        self.layer.borderWidth = 1.0
        self.layer.borderColor = ColorPalette.purple().CGColor
    }
    
    func setColorBorder(color: UIColor){
        self.layer.borderColor = color.CGColor
    }

}
