//
//  LoginController.swift
//  NeoTrip
//
//  Created by Joao Pedro Fabiano Franco on 11/4/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import UIKit
import ParseUI
import FBSDKCoreKit
import FBSDKLoginKit

class LoginController: UIViewController, FBSDKLoginButtonDelegate{
    
    @IBOutlet weak var usernameTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    @IBOutlet weak var loginButton: UIButton!
    @IBOutlet weak var registerButton: UIButton!
    @IBOutlet weak var errorLabel: UILabel!
    @IBOutlet weak var activitySpace: UIView!
    
    @IBOutlet weak var facebookLoginButton: FBSDKLoginButton!
    
    @IBOutlet weak var loginView: UIView!
    @IBOutlet weak var logoView: UIView!
    
    var loginTraveler: Traveler!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        /* Database connect */
        if let user = User.user() {
            loginTraveler = Traveler.traveler(user.id!)
            
            databaseManager = ServerManager(user: user)
            
            self.logoView.center.y = self.logoView.center.y - self.loginView.bounds.height
            
            self.loginView.alpha = 0.8
            self.passwordTextField.alpha = 1
            self.usernameTextField.alpha = 1
            self.facebookLoginButton.alpha = 1
            self.loginButton.alpha = 1
            self.registerButton.alpha = 1
            
            usernameTextField.text = ""
            passwordTextField.text = ""
            
            facebookLoginButton.delegate = self
            
            dispatch_async(dispatch_get_main_queue()){
                self.performSegueWithIdentifier("showApp", sender: self)
            }
            
        } else {
            databaseManager = ServerManager()
            facebookLoginButton.alpha = 0
            
            /* Animation */
            UIView.animateWithDuration(1, delay: 1, options: UIViewAnimationOptions.CurveEaseOut, animations: { () -> Void in
                self.logoView.center.y = self.logoView.center.y - self.loginView.bounds.height
                }, completion: nil)
            
            UIView.animateWithDuration(1, delay: 1.3, options: UIViewAnimationOptions.CurveLinear, animations: { () -> Void in
                self.loginView.alpha = 0.8
                self.passwordTextField.alpha = 1
                self.usernameTextField.alpha = 1
                self.facebookLoginButton.alpha = 1
                }, completion: nil)
            
            UIView.animateWithDuration(0.7, delay: 1.7, options: UIViewAnimationOptions.CurveLinear, animations: { () -> Void in
                self.loginButton.alpha = 1
                self.registerButton.alpha = 1
                }, completion: nil)
            
            /* Modal */
            usernameTextField.text = ""
            passwordTextField.text = ""
            
            facebookLoginButton.delegate = self
            facebookLoginButton.hidden = true
        }
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        loginView.makeCurve(10)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        facebookLoginButton.hidden = true
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        UIApplication.sharedApplication().statusBarStyle = .Default
    }
    
    /* MARK: - TextField Delegate */
    func textFieldShouldReturn(textField: UITextField!) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showApp"){
//            let tabBarController = segue.destinationViewController as! TabBarController
//            tabBarController.traveler = loginTraveler
        }
    }
    
    // MARK: - Unwind Segue
    @IBAction func cancelSignUpUnwindSegue (unwindSegue: UIStoryboardSegue) {
        
    }
    
    // MARK: - Actions
    @IBAction func login(sender: UIButton) {
        let loginUser = User()
        loginUser.id = usernameTextField.text
        loginUser.password = passwordTextField.text
        
        self.activitySpace.startLoadingAnimation(UIColor.whiteColor())
        
        self.loginView.hidden = true
        self.facebookLoginButton.hidden = true
        self.loginButton.hidden = true
        self.registerButton.hidden = true
        self.errorLabel.hidden = true
        
        print("Start Login...")
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            let (result, traveler) = databaseManager!.login(loginUser, retry: false)
            switch (result) {
            case .YES:
                
                print("Login Done")
                
                self.loginTraveler = traveler
                dispatch_async(dispatch_get_main_queue()){
                    self.performSegueWithIdentifier("showApp", sender: self)
                }
                break
                
            case .NO:
                
                dispatch_sync(dispatch_get_main_queue()){
                    self.errorLabel.hidden = false
                    self.errorLabel.text = "Senha ou usuário incorretos"
                }
                break
                
            case .DEPRECATED:
                
                dispatch_sync(dispatch_get_main_queue()){
                    self.errorLabel.hidden = false
                    self.errorLabel.text = "Atualize o app!"
                }
                break
                
            case .UNCONNECTED:
                
                dispatch_sync(dispatch_get_main_queue()){
                    self.errorLabel.hidden = false
                    self.errorLabel.text = "Falha na conexão com o servidor"
                }
                break
                
                
            default:
                break
            
            }
            
            dispatch_sync(dispatch_get_main_queue()){
                self.activitySpace.stopLoadingAnimation()
                self.loginButton.hidden = false
                self.registerButton.hidden = false
                self.facebookLoginButton.hidden = false
                self.loginView.hidden = false
            }
        }
    }
    
    
    // MARK: - Modal
    func loadCities(){
    }
    
    // MARK: - Facebook Delegate
    func loginButtonDidLogOut(loginButton: FBSDKLoginButton!){
        let loginManager: FBSDKLoginManager = FBSDKLoginManager()
        loginManager.logOut()
    }
    
    func loginButton(loginButton: FBSDKLoginButton!, didCompleteWithResult result: FBSDKLoginManagerLoginResult!, error: NSError!) {
        
        requestFacebookData { (dic) -> Void in
            let id = dic.valueForKey("id") as! String
            let name = dic.valueForKey("name") as! String
            let email = dic.valueForKey("email") as! String
            let gender = dic.valueForKey("gender") as! String
            
            print("Facebook User:")
            print(" -Facebook ID:\(id)")
            print(" -Nome:\(name)")
            print(" -Email: \(email)")
            print(" -Genero \(gender)")
        }
    }
    
    func requestFacebookData(completion: (NSDictionary) -> Void){
        let graphRequest : FBSDKGraphRequest = FBSDKGraphRequest(graphPath: "me", parameters: ["fields":"id,gender,birthday,email,name,picture.width(480).height(480)"])
        
        graphRequest.startWithCompletionHandler({ (connection, result, error) -> Void in
            if ((error) != nil){
                print("Error: \(error.description)")
            } else {
                let dic = result as! NSDictionary
                completion(dic)
            }
        })
    }
    
}
