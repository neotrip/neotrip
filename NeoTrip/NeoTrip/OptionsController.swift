//
//  OptionsController.swift
//  NeoTrip
//
//  Created by Lucas Padilha on 1/6/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

class OptionsController: UIViewController {
    
    @IBOutlet weak var attractionView: UIView!
    @IBOutlet weak var accommodationView: UIView!
    @IBOutlet weak var portView: UIView!
    
    var itinerary : Itinerary!
    var place : Place!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Gestures
        let tapAccommodation : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("tapAccommodation:"))
        accommodationView.addGestureRecognizer(tapAccommodation)
        
        let tapPort : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("tapPort:"))
        portView.addGestureRecognizer(tapPort)
        
        let tapAttraction : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("tapAttraction:"))
        attractionView.addGestureRecognizer(tapAttraction)
        
        let touchOutside : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("cancel:"))
        self.view.addGestureRecognizer(touchOutside)
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidAppear(animated)
        
        UIApplication.sharedApplication().statusBarStyle = .Default
    }
    
    // MARK: - Gesture Recognizers
    
    func tapAccommodation (recognizer: UITapGestureRecognizer) {
        if (Int(place!.interval!) - place!.usedDays() > 0){
            performSegueWithIdentifier("showModalAccommodationOption", sender: self)
        } else {
            cantAddAccommodationAlert()
        }
    }
    
    func tapPort (recognizer: UITapGestureRecognizer) {
        performSegueWithIdentifier("showModalPortOption", sender: self)
    }
    
    func tapAttraction (recognizer: UITapGestureRecognizer) {
        performSegueWithIdentifier("showModalAttractionOption", sender: self)
    }
    
    func cancel (recognizer: UITapGestureRecognizer) {
        performSegueWithIdentifier("closeCancelModalOptions", sender: self)
    }
    
    // MARK: - Segue

    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showModalAttractionOption"){
            let controller = segue.destinationViewController as! AttractionController
            controller.itinerary = itinerary
            controller.place = place
        }
        else if (segue.identifier == "showModalAccommodationOption"){
            let controller = segue.destinationViewController as! AccommodationController
            controller.itinerary = itinerary
            controller.place = place
        }
        else if (segue.identifier == "showModalPortOption"){
            let controller = segue.destinationViewController as! PortController
            controller.itinerary = itinerary
            controller.place = place
        }
        
        var viewControllers = navigationController?.viewControllers
        let index = viewControllers?.indexOf(self)
        viewControllers?.removeAtIndex(index!)
        navigationController?.viewControllers = viewControllers!
    }
    
    // MARK: - Alert
    func cantAddAccommodationAlert() {
        let alert = UIAlertController(title: "Dias insuficientes", message: "Você não tem mais dias disponíveis para adicionar uma acomodação", preferredStyle: UIAlertControllerStyle.Alert)
        let defaultAction = UIAlertAction(title: "Entendido", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        presentViewController(alert, animated: true, completion: nil)
    }

}
