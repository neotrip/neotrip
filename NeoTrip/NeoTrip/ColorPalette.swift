//
//  ColorPalette.swift
//  NeoTrip
//
//  Created by Carlos Henrique Martins Cayres on 27/10/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import UIKit

class ColorPalette: NSObject {
    
    class func yellow() -> UIColor {
        
        return UIColor(red:0.95, green:0.68, blue:0.15, alpha:1.0)
    }

    class func blue() -> UIColor {
        
        return UIColor(red:0.31, green:0.43, blue:0.82, alpha:1.0)
    }
    
    class func red() -> UIColor {
        
        return UIColor(red:1.00, green:0.35, blue:0.35, alpha:1.0)
    }
    
    class func green() -> UIColor {
            
        return UIColor(red:0.67, green:0.82, blue:0.59, alpha:1.0)
    }

    class func purple() -> UIColor {
                
        return UIColor(red:0.29, green:0.23, blue:0.37, alpha:1.0)
    }
    
    class func gray() -> UIColor {
                    
        return UIColor(red:0.90, green:0.90, blue:0.90, alpha:1.0)
    }
    
    class func darkGray() -> UIColor {
                        
        return UIColor(red:0.72, green:0.68, blue:0.71, alpha:1.0)
    }
    
    class func black() -> UIColor {
        
        return UIColor.blackColor()
    }
    
    class func white() -> UIColor {
        
        return UIColor.whiteColor()
    }
    
    class func lightBlue() -> UIColor {
        return UIColor(red: 0.59, green: 0.74, blue: 0.82, alpha: 1.0)
    }
    
}



