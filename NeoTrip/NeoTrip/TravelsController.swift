//
//  TravelsController.swift
//  NeoTrip
//
//  Created by Joao Pedro Fabiano Franco on 11/18/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import UIKit

class TravelsController: UITableViewController {
    
    var currentTraveler: Traveler!
    var travelsOwned: [Travel]!
    var travelsTaking: [Travel]!
    
    var selectedTravel: Travel!
    
//    var tutorial: Bool = true
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        currentTraveler = databaseManager!.traveler
        
        travelsOwned = Travel.search(currentTraveler, option: TRAVELSOWNED)
        travelsTaking = Travel.search(currentTraveler, option: TRAVELSTAKING)
        
        let refreshControl = UIRefreshControl()
        refreshControl.addTarget(self, action: "refreshing:", forControlEvents: .ValueChanged)
        self.tableView.addSubview(refreshControl)
        
        // Notification
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: "statusChanged:", name: ConnectionChanged, object: nil)
        
        Travel.push(currentTraveler)

        refreshing(refreshControl)
        //databaseManager?.delegate = self
    }
    
    override func viewWillAppear(animated: Bool) {
        tableView.reloadData()
    }
    
    /* MARK: - Aux */
    func refreshing(rControl: UIRefreshControl){
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            databaseManager!.getTravels(forTraveler: self.currentTraveler)
            dispatch_sync(dispatch_get_main_queue()) {
                self.refreshTravels()
                rControl.endRefreshing()
            }
        }
    }
    
    func refreshTravels(){
        travelsOwned = Travel.search(currentTraveler, option: TRAVELSOWNED)
        travelsTaking = Travel.search(currentTraveler, option: TRAVELSTAKING)
        tableView.beginUpdates()
        tableView.deleteSections(NSIndexSet(index: 0), withRowAnimation: .None)
        tableView.insertSections(NSIndexSet(index: 0), withRowAnimation: .None)
        tableView.deleteSections(NSIndexSet(index: 1), withRowAnimation: .None)
        tableView.insertSections(NSIndexSet(index: 1), withRowAnimation: .None)
        tableView.endUpdates()
    }
    
    /* MARK: - Unwind Segue */
    @IBAction func donePlannerUnwindSegue(unwindSegue: UIStoryboardSegue){
        
    }
    
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 2
    }
    
    override func tableView(tableView: UITableView, titleForHeaderInSection section: Int) -> String? {
        if (section == 0) {
            return "Minhas Viagens"
        }
        else {
            return "Outras Viagens"
        }
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        if (section == 0) {
            return travelsOwned.count + 1
        }
        else {
            return travelsTaking.count
        }
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let result: UITableViewCell!
        
        if (indexPath.section == 0) {
            if (indexPath.row < travelsOwned.count){
                let travelsCell = tableView.dequeueReusableCellWithIdentifier("travelsCell", forIndexPath: indexPath) as! TravelCell
                travelsCell.fillCell(travelsOwned[indexPath.row])
                result = travelsCell
            } else {
                let newTravel = tableView.dequeueReusableCellWithIdentifier("newCell", forIndexPath: indexPath) as! NewTravel
                result = newTravel
            }
            
            return result
        }
        else {
            let travelsCell = tableView.dequeueReusableCellWithIdentifier("travelsCell", forIndexPath: indexPath) as! TravelCell
            travelsCell.fillCell(travelsTaking[indexPath.row])
            result = travelsCell
            
            return result
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.section == 0) {
            if (indexPath.row < travelsOwned.count){
                selectedTravel = travelsOwned[indexPath.row]
                performSegueWithIdentifier("showPlanner", sender: self)
            } else {
                // ADICIONANDO NOVA TRIP
                let newTravel = Travel()
                
                let defaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
                if let bool = defaults.objectForKey("tutorial") as? Bool {
                    // if exists, can be true or false
                    if (bool) {
                        newTravel.name = "Tutorial 0: Primeira Viagem"
                        defaults.setBool(false, forKey: "tutorial")
                    }
                    else {
                        newTravel.name = "Viagem \(currentTraveler.travelsOwned!.count)"
                    }
                } else {
                    // if not, nil is returned. set true if you are not gonna show again. maybe we can set false if he wants to see tutorial again, don't know
                    defaults.setBool(false, forKey: "tutorial")
                    defaults.synchronize()
                    newTravel.name = "Tutorial 0: Primeira Viagem"
                }
                
                newTravel.travelerOwner = currentTraveler
                
                if (currentTraveler.city != nil) {
                    let place = Place()
                    place.itinerary = newTravel.itinerary!
                    place.city = currentTraveler.city
                    place.interval = 0
                    place.category = PlaceType.ROOT
                    place.save()
                }
                
                newTravel.save()
                
                refreshTravels()
            }
        }
        else {
            selectedTravel = travelsTaking[indexPath.row]
            performSegueWithIdentifier("showPlanner", sender: self)
        }
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showPlanner"){
            let plannerController = segue.destinationViewController as! PlannerController
            plannerController.travel = selectedTravel
            
            plannerController.traveler = currentTraveler
            
            if (selectedTravel.name!.componentsSeparatedByString(":")[0].componentsSeparatedByString(" ")[0] == "Tutorial") {
                plannerController.tutorialBool = true
            }
            else {
                plannerController.tutorialBool = false
            }
        }
    }
    
    override func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 70.0
    }
    
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        if (indexPath.section == 0) {
            if (editingStyle == UITableViewCellEditingStyle.Delete) {
                let cell = tableView.cellForRowAtIndexPath(indexPath)
                if (!(cell is NewTravel)) {
                    travelsOwned[indexPath.row].delete()
                    refreshTravels()
                }
            }
        } else {
            if (editingStyle == UITableViewCellEditingStyle.Delete) {
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                    databaseManager!.untakePartTravel(self.travelsTaking[ indexPath.row ])
                    dispatch_sync(dispatch_get_main_queue()) {
                        self.refreshTravels()
                    }
                }
            }
        }
    }
}

// MARK: - Connection Delegate
extension TravelsController {
    func statusChanged(notification:NSNotification) {
        /*
        let status = Connection(rawValue: notification.object as! Int)
        if (status == Connection.LOGGED) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                databaseManager!.getTravels(forTraveler: self.currentTraveler)
                dispatch_sync(dispatch_get_main_queue()) {
                    self.refreshTravels()
                }
            }
        } else {
            
        }*/
    }
}
