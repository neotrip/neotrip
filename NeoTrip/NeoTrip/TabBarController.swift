//
//  TabBarController.swift
//  NeoTrip
//
//  Created by Joao Pedro Fabiano Franco on 10/19/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import UIKit

class TabBarController: UITabBarController {
    
    var search: UITabBarItem!
    var travels: UITabBarItem!
    var perfil: UITabBarItem!
    
    //var traveler: Traveler!
    
    var connectionFeedback : UIView?

    override func viewDidLoad() {
        super.viewDidLoad()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "statusChanged:", name: ConnectionChanged, object: nil)
        
        if (databaseManager!.status ==  Connection.CONNECTED || databaseManager?.status == Connection.LOGGED) {
            if (connectionFeedback != nil) {
                connectionFeedback!.removeFromSuperview()
            }
        }
        else {
            connectionFeedback = UIView.loadFromNibNamed("ConnectionFeedback")
            connectionFeedback?.frame = self.view.frame
            connectionFeedback?.center = self.view.center
            connectionFeedback?.layer.zPosition = 12
            self.view.addSubview(connectionFeedback!)
//            connectionFeedback?.sendSubviewToBack(self.view)
            self.view.sendSubviewToBack(connectionFeedback!)
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        loadTabBar()
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    func loadTabBar(){
        tabBar.barTintColor = ColorPalette.purple()
        
        var itens = self.tabBar.items as [UITabBarItem]!
        travels = itens[0]
        search = itens[1]
        perfil = itens[2]
        
        search.image = UIImage(named: "search-darkgray")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        search.setTitleTextAttributes([NSForegroundColorAttributeName: ColorPalette.darkGray()], forState: UIControlState.Normal)
        search.selectedImage = UIImage(named: "search-white")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        search.setTitleTextAttributes([NSForegroundColorAttributeName: ColorPalette.white()], forState: UIControlState.Selected)
        search.title = "Procurar"
        
        travels.image = UIImage(named: "graph-darkgray")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        travels.setTitleTextAttributes([NSForegroundColorAttributeName: ColorPalette.darkGray()], forState: UIControlState.Normal)
        travels.selectedImage = UIImage(named: "graph-white")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        travels.setTitleTextAttributes([NSForegroundColorAttributeName: ColorPalette.white()], forState: UIControlState.Selected)
        travels.title = "Trips"
        
        perfil.image = UIImage(named: "profile-darkgray")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        perfil.setTitleTextAttributes([NSForegroundColorAttributeName: ColorPalette.darkGray()], forState: UIControlState.Normal)
        perfil.selectedImage = UIImage(named: "profile-white")?.imageWithRenderingMode(UIImageRenderingMode.AlwaysOriginal)
        perfil.setTitleTextAttributes([NSForegroundColorAttributeName: ColorPalette.white()], forState: UIControlState.Selected)
        perfil.title = "Perfil"
    }
    
}

// MARK: - Connection Delegate
extension TabBarController {
    func statusChanged(notification:NSNotification) {
        let status = Connection(rawValue: notification.object as! Int)
        if (status ==  Connection.CONNECTED || status == Connection.LOGGED) {
            if (connectionFeedback != nil) {
            connectionFeedback!.removeFromSuperview()
            connectionFeedback = nil
            }
        } else {
            if (connectionFeedback == nil) {
            connectionFeedback = UIView.loadFromNibNamed("ConnectionFeedback")
            connectionFeedback?.frame = self.view.frame
            connectionFeedback?.center = self.view.center
            connectionFeedback?.layer.zPosition = 12
            self.view.addSubview(connectionFeedback!)
            self.view.sendSubviewToBack(connectionFeedback!)
            }
        }
    }
}
