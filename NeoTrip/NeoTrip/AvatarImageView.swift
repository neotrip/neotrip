//
//  AvatarImageView.swift
//  NeoTrip
//
//  Created by Lucas Padilha on 1/8/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

class AvatarImageView: UIImageView {

    override func awakeFromNib() {
        self.layer.cornerRadius = 10.0
        self.layer.masksToBounds = true
        self.layer.borderColor = UIColor.whiteColor().CGColor
        self.layer.borderWidth = 3.0
    }
    
}
