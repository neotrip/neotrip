//
//  PlannerController.swift
//  NeoTrip
//
//  Created by Joao Pedro Fabiano Franco on 10/19/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import UIKit
import MapKit

struct TransportEdge {
    var startNode: Place!
    var endedNode: Place!
}

class PlannerController: UIViewController {
    
    @IBOutlet weak var graph: Graph!
    
    @IBOutlet weak var menu: UIView!
    
    @IBOutlet weak var travelIcon: UIView!
    @IBOutlet weak var travelName: UILabel!
    @IBOutlet weak var travelEdit: UIButton!
    @IBOutlet weak var travelTime: UILabel!
    @IBOutlet weak var travelPrice: UILabel!
    @IBOutlet weak var travelDate: UILabel!
    
    var traveler: Traveler!
    var itinerary: Itinerary?
    var travel: Travel!
    var newPosition: CGPoint!
    
    var printscreen: UIImage!
    
    var transportEdge = TransportEdge()
    
    var began: PlaceNode!
    var ended: PlaceNode!
    
    var currentEdittingPlace: Place?
    var currentEdittingTransport: Transport?
    
    var deleteMode : Bool = false
    
    var sumScale: CGFloat = 0
    var startPosition: CGPoint!
    
    var addedPlace: Place?
    var addedTransport: Transport?
    
    // Tutorial
    var tutorial : Tutorial?
    var tutorialBool : Bool = false
    @IBOutlet weak var tutorialBlurView: UIView!
    var tutorialZ : CGFloat = 0
    var subview: UIView?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.graph.contentSize = CGSize(width: 2*self.graph.frame.size.height, height: 3*self.graph.frame.size.height)
        self.graph.contentInset = UIEdgeInsetsZero
        
        // Gestures
        let touchGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("addNewTouchCity:"))
        self.graph.addGestureRecognizer(touchGesture)
        
        let openEdit: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("openEditTravel:"))
        self.menu.addGestureRecognizer(openEdit)
        
        self.graph.planner = self
        
        itinerary = travel.itinerary
        itinerary!.push()
        graph.itinerary = itinerary!
        
        currentEdittingPlace = nil
        
        self.graph.contentOffset = CGPoint(x: self.graph.frame.size.width/2, y: self.graph.frame.size.height/2)
        
        
        // Notification
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: "statusChanged:", name: ConnectionChanged, object: nil)
        
        // Tutorial
        if (tutorialBool) {
            tutorial = Tutorial()
            tutorial!.index = Int(travel.name!.componentsSeparatedByString(":")[0].componentsSeparatedByString(" ")[1])!
            tutorial!.travel = travel
            initTutorial()
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        bool = true
        NSNotificationCenter.defaultCenter().removeObserver(self)
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            databaseManager?.unregister()
        }
        
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        fillPlanner()
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "refresh", name: TravelRefreshed, object: nil)
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            if (databaseManager?.register(self.travel) == Result.NO) {
                print("you have no permission to access this travel!")
            }
            databaseManager?.getTravelInfo(forTravel: self.travel)
        }
        if (tutorial != nil) {
            updateName()
        }
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        
        if (tutorial != nil) {
            if (tutorial!.index == 3) {
                tutorial!.initTutorial()
            }
        }
    }
    
    var bool:Bool = true
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        if (bool) {
            bool = false
            
            graph.generateGraph()
            fixPositions()
            if let root = itinerary?.root(){
                centerPlace(root)
            } else if (graph.placesNode.count > 0) {
                centerPlace(graph.placesNode.first!.place)
            }
            
            graph.removeEdges()
            graph.repositionTransport()
        }
        
        travelIcon.makeCircle()
    }
    
    // MARK: - TextField Delegate
    func textFieldShouldReturn(textField: UITextField!) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    // MARK: - Segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        changeDeleteMode(false)
        if (segue.identifier == "showTravel"){
            let travelController = segue.destinationViewController as! TravelController
            travelController.travel = travel
            
            travelController.currentTraveler = traveler
            
            travelController.tutorial = tutorial
            if (tutorial != nil) {
                tutorial!.delegate = travelController
            }
        }
        
        if (segue.identifier == "showModalCity"){
            let cityController = segue.destinationViewController as! CityController
            cityController.itinerary = self.itinerary
            
            if (currentEdittingPlace != nil) {
                cityController.place = currentEdittingPlace
            }
            
            cityController.tutorial = tutorial
            if (tutorial != nil) {
                tutorial!.delegate = cityController
            }
        }
        
        if(segue.identifier == "showModalTransport"){
            let transportController = segue.destinationViewController as! TransportController
            if (self.transportEdge.startNode != nil || self.transportEdge.endedNode != nil){
                if (self.transportEdge.startNode.beginDate() != nil) {
                    transportController.date = self.transportEdge.startNode.beginDate()!.dateByAddingTimeInterval(NSTimeInterval(Double(transportEdge.startNode.interval!) * 86400.00))
                }
            }
            transportController.tutorial = tutorial
            
            transportController.transport = self.currentEdittingTransport
            transportController.startNode = self.transportEdge.startNode
            transportController.endedNode = self.transportEdge.endedNode
            transportController.itinerary = self.itinerary
        }
    }
    
    // MARK: - Actions
    @IBAction func backTravels(sender: UIButton) {
        performSegueWithIdentifier("closeDonePlanner", sender: self)
    }
    
    @IBAction func editTravel(sender: UIButton) {
        if (tutorial != nil) {
            if (tutorial!.index == 0) {
                tutorial!.advanceLevel()
                tutorial!.initTutorial()
            }
        }
        performSegueWithIdentifier("showTravel", sender: self)
    }
    
    func openEditTravel(recognizer: UITapGestureRecognizer){
        if (tutorial != nil) {
            if (tutorial!.index == 0) {
                tutorial!.advanceLevel()
                tutorial!.initTutorial()
            }
        }
        performSegueWithIdentifier("showTravel", sender: self)
    }
    
    // MARK: - Tap Gesture
    func addNewTouchCity(recognizer: UITapGestureRecognizer){
        if (!deleteMode){
            performSegueWithIdentifier("showModalCity", sender: self)
        } else {
            changeDeleteMode(!deleteMode)
        }
    }
    
    func editTouchTransport(){
        performSegueWithIdentifier("showModalTransport", sender: self)
    }
    
    // MARK: - Auxiliar Methods
    func editCity(){
        performSegueWithIdentifier("showCity", sender: self)
    }
    
    func addTransport(){
        performSegueWithIdentifier("showModalTransport", sender: self)
    }
    
    func deleteCity(place: Place) {
        //        place.itinerary = nil
        
        /*
        // DELETANDO PROVISORIAMENTE COISAS INTERNAS DA CIDADE - CULPA DO VIP
        let attractions = itinerary!.attractionsIn(place.city!)
        let accommodations = itinerary!.accommodationsIn(place.city!)
        let ports = itinerary!.portsIn(place.city!)
        
        for a in attractions {
            a.delete()
        }
        for b in accommodations {
            b.delete()
        }
        for p in ports {
            p.delete()
        }*/
        
        // DELETANDO PROVISORIAMENTE TRANSPORTES - CULPA DO VIP
//        for t in itinerary!.transports!.allObjects as! [Transport] {
//            if (t.startPoint == place || t.endPoint == place) {
//                t.delete()
//            }
//        }
        
        // DELETANDO CIDADE
        place.delete()
        
        changeDeleteMode(!self.deleteMode)
        
        refresh()
        
        if (tutorial != nil) {
            if (tutorial!.index == 5) {
                tutorial!.advanceLevel()
                tutorial!.initTutorial()
            }
        }
    }
    
    func deleteTransport(transport: Transport) {
        //        transport.itinerary = nil
        transport.delete()
        
        changeDeleteMode(!self.deleteMode)
        
        refresh()
    }
    
    func centerPlace(place: Place){
        for placeNode in graph.placesNode{
            if placeNode.place == place{
                //                UIView.animateWithDuration(0.5, animations: { () -> Void in
                //                    self.graph.contentOffset = CGPoint(x: placeNode.center.x - (self.graph.frame.size.width/2), y: placeNode.center.y - (self.graph.frame.size.height/2))
                //                    }, completion: nil)
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.graph.contentOffset = CGPoint(x: placeNode.center.x - (self.graph.frame.size.width/2), y: placeNode.center.y - (self.graph.frame.size.height/2))
                    }, completion: { (result) -> Void in
                        if (self.tutorial != nil) {
                            if (self.tutorial!.index == 2 && self.itinerary!.root() != nil) {
                                self.tutorial!.delegate = self
                                self.tutorial!.initTutorial()
                            }
                        }
                        if (self.tutorial != nil) {
                            if (self.tutorial!.index == 3) {
                                self.tutorial!.initTutorial()
                            }
                        }
                })
            }
        }
    }
    
    func centerTransport(transport: Transport){
        for transportNode in graph.transportsNode{
            if transportNode.transport == transport{
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.graph.contentOffset = CGPoint(x: transportNode.center.x - (self.graph.frame.size.width/2), y: transportNode.center.y - (self.graph.frame.size.height/2))
                    }, completion: { (result) -> Void in
                        // TO DO ADJUST LVL 4
                        if (self.tutorial != nil) {
                            if (self.tutorial!.index == 4) {
                                self.tutorial!.initTutorial()
                            }
                        }
                })
            }
        }
    }
    
    func fixPositions(){
        for placeNode in graph.placesNode{
            if (placeNode.frame.origin.y + placeNode.bounds.size.height > self.graph.contentSize.height){
                self.graph.contentSize.height += 2*placeNode.frame.size.height
            }
            
            if (placeNode.frame.origin.x + placeNode.bounds.size.width > self.graph.contentSize.width){
                self.graph.contentSize.width += 2*placeNode.frame.size.width
            }
        }
        
        var maxInsideY: CGFloat = 0
        for placeNode in graph.placesNode{
            let insideY = 0 - placeNode.frame.origin.y
            if (insideY > maxInsideY){
                maxInsideY = insideY
            }
        }
        
        var maxInsideX: CGFloat = 0
        for placeNode in graph.placesNode{
            let insideX = 0 - placeNode.frame.origin.x
            if (insideX > maxInsideX){
                maxInsideX = insideX
            }
        }
        
        self.graph.contentSize.height += maxInsideY
        self.graph.contentSize.width += maxInsideX
        
        for placeNode in graph.placesNode{
            placeNode.setPosition(self.graph, position: CGPoint(x: placeNode.frame.origin.x + maxInsideX + placeNode.bounds.size.width/2, y: placeNode.frame.origin.y + maxInsideY + placeNode.bounds.size.height/2))
        }
    }
    
    func refresh() {
        self.graph.contentSize = CGSize(width: 2*self.graph.frame.size.height, height: 3*self.graph.frame.size.height)
        self.graph.contentInset = UIEdgeInsetsZero
        
        dispatch_async(dispatch_get_main_queue()) {
            self.fillPlanner()
            self.graph.generateGraph()
            self.fixPositions()
            
            if let root = self.itinerary?.root(){
                self.centerPlace(root)
            } else if (self.graph.placesNode.count > 0) {
                self.centerPlace(self.graph.placesNode.first!.place)
            }
            
            if (self.addedPlace != nil){
                self.centerPlace(self.addedPlace!)
                self.addedPlace = nil
            }
            
            if (self.addedTransport != nil){
                self.centerTransport(self.addedTransport!)
                self.addedTransport = nil
            }
            
            self.graph.removeEdges()
            self.graph.repositionTransport()
            
            //            if (self.deleteMode) {
            //                self.changeDeleteMode(!self.deleteMode)
            //                self.changeDeleteMode(!self.deleteMode)
            //            }
        }
    }
    
    func fillPlanner(){
        var startDate = "n/d"
        var endDate = "n/d"
        
        travelName.text = travel.fillName()
        travelTime.text = travel.itinerary!.getInterval().stringValue
        travelPrice.text = String.cashString(travel.itinerary!.getPrice() as CGFloat)
        
        if (travel.itinerary!.startTime != nil){
            startDate = travel.itinerary!.startTime!.dateToString!
            endDate = travel.itinerary!.startTime!.dateByAddingTimeInterval(NSTimeInterval(Double(travel.itinerary!.interval!) * 86400.00)).dateToString!
        }
        
        travelDate.text = "\(startDate) - \(endDate)"
    }
    
    func changeDeleteMode(value: Bool) {
        self.deleteMode = value
        for node in graph.placesNode {
            node.changeDeleteMode(self.deleteMode)
        }
        
        for node in graph.transportsNode {
            node.changeDeleteMode(self.deleteMode)
        }
    }
    
    // MARK: - Alerts
    func showFailAlertView(){
        let alertFailTransport = UIAlertController(title: "Erro", message: "Nao foi possivel ligar as duas cidades", preferredStyle: UIAlertControllerStyle.Alert)
        
        alertFailTransport.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alertFailTransport, animated: true, completion: nil)
    }
    
    func showAlert(){
        let alertRect = CGRect(x: 100, y: 100, width: 200, height: 200)
        let alertView = UIView(frame: alertRect)
        alertView.backgroundColor = ColorPalette.blue()
        
        let alertCloseCities = UIAlertController(title: "Cidade Inexistente", message: "Adicione um transporte de uma cidade à outra.", preferredStyle: UIAlertControllerStyle.Alert)
        
        alertCloseCities.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: nil))
        self.presentViewController(alertCloseCities, animated: true, completion: nil)
        
    }
    
    // MARK: - Unwind Segue
    @IBAction func doneCityUnwindSegue(unwindSegue: UIStoryboardSegue){
        if (tutorial != nil) {
            tutorial!.delegate = self
        }
        refresh()
        currentEdittingPlace = nil
        if (tutorial != nil) {
            if (tutorial!.index == 5) {
                tutorial!.initTutorial()
            }
        }
    }
    
    @IBAction func cancelCityUnwindSegue(unwindSegue: UIStoryboardSegue){
        if (tutorial != nil) {
            tutorial!.delegate = self
        }
        refresh()
    }
    
    @IBAction func cancelTransportUnwindSegue(unwindSegue: UIStoryboardSegue){
        if (tutorial != nil) {
            tutorial!.delegate = self
        }
        refresh()
    }
    
    @IBAction func doneTransportUnwindSegue(unwindSegue: UIStoryboardSegue){
        if (tutorial != nil) {
            tutorial!.delegate = self
        }
        refresh()
        currentEdittingTransport = nil
        // provisoriamente
        if (tutorial != nil) {
            if (tutorial!.index == 4) {
                tutorial!.initTutorial()
            }
        }
    }
    
    @IBAction func doneTravelUnwindSegue(unwindSegue: UIStoryboardSegue) {
        if (tutorial != nil) {
            tutorial!.delegate = self
        }
        if (tutorial != nil) {
            if (tutorial!.index == 1 && itinerary!.root() != nil) {
                tutorial!.advanceLevel()
            }
        }
        refresh()
    }
    
}

extension PlannerController: placeNodeDelegate {
    
    func tapOnNode(placeNode: PlaceNode) {
        currentEdittingPlace = placeNode.place
        performSegueWithIdentifier("showModalCity", sender: self)
    }
    
    func longPressOnNode(placeNode: PlaceNode) {
        changeDeleteMode(!self.deleteMode)
    }
    
    func panOnNode(placeNode: PlaceNode, recognizer: UIPanGestureRecognizer) {
        let position = recognizer.locationInView(self.graph)
        var abble : Bool = true
        began = placeNode
        
        if (!deleteMode) {
            if (recognizer.state == .Began) {
                transportEdge.startNode = began.place
            }
            else if (recognizer.state == .Changed) {
                if (began != nil){
                    self.graph.removeTemporaryEdges()
                    self.graph.layer.addSublayer(self.graph.setArrow(began.center, point2: position))
                }
                
                let boundsLeft = self.graph.bounds.origin.x
                let boundsRight = self.graph.bounds.origin.x + self.graph.bounds.size.width
                let boundsTop = self.graph.bounds.origin.y
                let boundsBottom = self.graph.bounds.origin.y + self.graph.bounds.size.height
                let positionX = position.x
                let positionY = position.y
                let margin: CGFloat = 15.0
                let scroll: CGFloat = 100.0
                
                if (positionX < boundsLeft + margin){
                    self.graph.scrollRectToVisible(CGRect(x: self.graph.bounds.origin.x - scroll, y: self.graph.bounds.origin.y, width: self.graph.bounds.size.width, height: self.graph.bounds.size.height), animated: true)
                }
                
                if (positionX > boundsRight - margin){
                    self.graph.scrollRectToVisible(CGRect(x: self.graph.bounds.origin.x + scroll, y: self.graph.bounds.origin.y, width: self.graph.bounds.size.width, height: self.graph.bounds.size.height), animated: true)
                }
                
                if (positionY < boundsTop + margin){
                    self.graph.scrollRectToVisible(CGRect(x: self.graph.bounds.origin.x, y: self.graph.bounds.origin.y - scroll, width: self.graph.bounds.size.width, height: self.graph.bounds.size.height), animated: true)
                }
                
                if (positionY > boundsBottom - margin){
                    self.graph.scrollRectToVisible(CGRect(x: self.graph.bounds.origin.x, y: self.graph.bounds.origin.y + scroll, width: self.graph.bounds.size.width, height: self.graph.bounds.size.height), animated: true)
                }
                
                began.selectionView.layer.borderColor = ColorPalette.blue().CGColor
                began.selectionView.layer.borderWidth = 3
                
                for node in graph.placesNode {
                    if (CGRectContainsPoint(node.frame, position)) {
                        for t in itinerary!.transports!.allObjects as! [Transport] {
                            if (t.endPoint == node.place) {
                                abble = false
                            }
                        }
                        if (abble) {
                            node.selectionView.layer.borderColor = ColorPalette.blue().CGColor
                            node.selectionView.layer.borderWidth = 3
                        }
                        else {
                            node.selectionView.layer.borderColor = ColorPalette.red().CGColor
                            node.selectionView.layer.borderWidth = 3
                        }
                    }
                    else {
                        if (node != began) {
                            node.selectionView.layer.borderColor = UIColor.clearColor().CGColor
                            node.selectionView.layer.borderWidth = 0
                        }
                    }
                }
            }
            else if (recognizer.state == .Ended) {
                abble = true
                began.selectionView.layer.borderColor = UIColor.clearColor().CGColor
                began.selectionView.layer.borderWidth = 0
                began = nil
                self.graph.removeTemporaryEdges()
                for node in graph.placesNode {
                    if (CGRectContainsPoint(node.frame, position)) {
                        for t in itinerary!.transports!.allObjects as! [Transport] {
                            if (t.endPoint == node.place) {
                                abble = false
                            }
                        }
                        ended = node
                        ended.selectionView.layer.borderColor = UIColor.clearColor().CGColor
                        ended.selectionView.layer.borderWidth = 0
                        if (abble) {
                            transportEdge.endedNode = node.place
                        }
                        else {
                            transportEdge.startNode = nil
                            transportEdge.endedNode = nil
                        }
                        break;
                        
                    }
                }
                
                if (((transportEdge.startNode) != nil) &&
                    ((transportEdge.endedNode) != nil) &&
                    (transportEdge.startNode != transportEdge.endedNode) &&
                    abble) {
                        
                        addTransport()
                        
                        transportEdge.startNode = nil
                        transportEdge.endedNode = nil
                }
                else if (!abble) {
                    // NAO PODE VOLTAR PRA UMA CIDADE QUE VC JA SAIU
                    showFailAlertView()
                }
                else{
                    showFailAlertView()
                }
            }
            else {
                began.selectionView.layer.borderColor = UIColor.clearColor().CGColor
                began.selectionView.layer.borderWidth = 0
                began = nil
                self.graph.removeTemporaryEdges()
            }
        }
        else {
            if (recognizer.state == .Began) {
                
            }
            else if (recognizer.state == .Changed) {
                let translation = recognizer.translationInView(self.view)
                placeNode.center = CGPointMake(placeNode.initialPosition.x + translation.x, placeNode.initialPosition.y + translation.y)
            }
            else if (recognizer.state == .Ended) {
                placeNode.center = placeNode.initialPosition
            }
        }
    }
    
    func tapOnDelete(placeNode: PlaceNode) {
        
        let alertDeleteCity = UIAlertController(title: "Deletar \(placeNode.place.city!.name!)", message: "Deseja realmente deletar \(placeNode.place.city!.name!)?", preferredStyle: UIAlertControllerStyle.Alert)
        
        alertDeleteCity.addAction(UIAlertAction(title: "Sim", style: UIAlertActionStyle.Cancel, handler: { (alert) -> Void in
            self.deleteCity(placeNode.place)
        }))
        alertDeleteCity.addAction(UIAlertAction(title: "Não", style: UIAlertActionStyle.Default, handler: nil))
        self.presentViewController(alertDeleteCity, animated: true, completion: nil)
        
    }
    
    func tapOnRoot(placeNode: PlaceNode) {
        performSegueWithIdentifier("showTravel", sender: self)
    }
}

extension PlannerController: TransportNodeDelegate{
    
    func showAddNewCity() {
        performSegueWithIdentifier("showModalCity", sender: self)
    }
    
    func showEditTransport(transport: Transport) {
        currentEdittingTransport = transport
        performSegueWithIdentifier("showModalTransport", sender: self)
    }
    
    func touchDelete(transportNode: TransportNode) {
        deleteTransport(transportNode.transport!)
    }
    
    func longPress(transportNode: TransportNode) {
        changeDeleteMode(!self.deleteMode)
    }
    
}

// MARK: - Tutorial
extension PlannerController: TutorialDelegate {
    
    func tutorialTap(recognizer: UITapGestureRecognizer) {
        tutorialBlurView.hidden = true
    }
    
    func initTutorial () {
        if (tutorial != nil) {
            tutorial!.delegate = self
            tutorial!.initTutorial()
            tutorialBlurView.hidden = false
            let tutorialTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("tutorialTap:"))
            tutorialBlurView.addGestureRecognizer(tutorialTap)
        }
    }
    
    // DELEGATE
    func prepareTutorial(index: Int) {
        switch index {
        case 0:
            tutorialBlurView.hidden = false
            subview = UIView.loadFromNibNamed("Tutorial 0")
            subview!.frame = tutorialBlurView.frame
            subview!.center = tutorialBlurView.center
            tutorialBlurView.addSubview(subview!)
            tutorialZ = menu.layer.zPosition
            menu.layer.zPosition = 10
            break
        case 2:
            tutorialBlurView.hidden = false
            menu.layer.zPosition = tutorialZ
            if (subview != nil) {
                subview!.removeFromSuperview()
            }
            subview = UIView.loadFromNibNamed("Tutorial 2")
            subview!.frame = tutorialBlurView.frame
            subview!.center = tutorialBlurView.center
            tutorialBlurView.addSubview(subview!)
            tutorialBlurView.layoutIfNeeded()
            break
        case 3:
            tutorialBlurView.hidden = false
            if (subview != nil) {
                subview!.removeFromSuperview()
            }
            subview = UIView.loadFromNibNamed("Tutorial 3")
            subview!.frame = tutorialBlurView.frame
            subview!.center = tutorialBlurView.center
            tutorialBlurView.addSubview(subview!)
            tutorialBlurView.layoutIfNeeded()
            break
        case 4:
            tutorialBlurView.hidden = false
            if (subview != nil) {
                subview!.removeFromSuperview()
            }
            subview = UIView.loadFromNibNamed("Tutorial 4")
            subview!.frame = tutorialBlurView.frame
            subview!.center = tutorialBlurView.center
            tutorialBlurView.addSubview(subview!)
            tutorialBlurView.layoutIfNeeded()
            break
        case 5:
            tutorialBlurView.hidden = false
            if (subview != nil) {
                subview!.removeFromSuperview()
            }
            subview = UIView.loadFromNibNamed("Tutorial 5")
            subview!.frame = tutorialBlurView.frame
            subview!.center = tutorialBlurView.center
            tutorialBlurView.addSubview(subview!)
            tutorialBlurView.layoutIfNeeded()
            break
        case 6:
            tutorialBlurView.hidden = false
            if (subview != nil) {
                subview!.removeFromSuperview()
            }
            subview = UIView.loadFromNibNamed("Tutorial Final")
            subview!.frame = tutorialBlurView.frame
            subview!.center = tutorialBlurView.center
            tutorialBlurView.addSubview(subview!)
            tutorialBlurView.layoutIfNeeded()
            tutorial!.advanceLevel()
            break
        default:
            tutorialBlurView.hidden = true
            break
        }
        
    }
    
    func advanceLevel() {
        
    }
    
    func finishTutorial() {
        tutorialBool = false
        tutorial = nil
        let defaults:NSUserDefaults = NSUserDefaults.standardUserDefaults()
        if let _ = defaults.objectForKey("tutorial") as? Bool {
            // if exists, can be true or false
            defaults.setBool(false, forKey: "tutorial")
            defaults.synchronize()
        } else {
            // if not, nil is returned. set true if you are not gonna show again. maybe we can set false if he wants to see tutorial again, don't know
            defaults.setBool(false, forKey: "tutorial")
            defaults.synchronize()
        }
    }
    
    func updateName() {
        travelName.text = travel.name
    }
}

// MARK: - Connection Delegate
extension PlannerController {
    func statusChanged(notification:NSNotification) {
        /*
        let status = Connection(rawValue: notification.object as! Int)
        if (status == Connection.LOGGED) {
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                if (databaseManager?.register(self.travel) == Result.NO) {
                    print("you have no permission to access this travel!")
                }
                databaseManager?.getTravelInfo(forTravel: self.travel)
                self.fillPlanner()
                self.refresh()
            }
        } else {
            
        }*/
    }
}
