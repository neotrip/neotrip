//
//  CityController.swift
//  NeoTrip
//
//  Created by Lucas Padilha on 11/25/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import UIKit

protocol Search {
    func searchDelegate(anyObject: [AnyObject])
}

class CityController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var backgroundView: UIView!
    @IBOutlet weak var daysLabel: UILabel!
    @IBOutlet weak var daysSlider: UISlider!
    @IBOutlet weak var cityTextField: AutoCompleteTextField!

    @IBOutlet weak var cityView: UIView!

    var place : Place?
    var city : City!
    var itinerary : Itinerary!
  
    var price : NSNumber = 0.0
    var days : NSNumber = 0.0
    
    var cities: [City] = []
    
    // Tutorial
    var tutorial: Tutorial?
    @IBOutlet weak var tutorialFeedback: UILabel!

    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.

        // Gestures
        let touchCityView : UITapGestureRecognizer = UITapGestureRecognizer (target: self, action: Selector("callPlannerInside:"))
        self.cityView.addGestureRecognizer(touchCityView)
        
        let touchOutside: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("closeCity:"))
        self.backgroundView.addGestureRecognizer(touchOutside)
        
        // Default
        if (place == nil) {
            days = 1
//            price = 0
        }
        else {
            city = place!.city!

            cityTextField.text = city!.name
            days = place!.interval!
            daysLabel.text = String(days)
            daysSlider.value = Float(days)

//            // calcular quantos dias vai ficar nessa cidade
//            // calcular quanto vai custar essa cidade
//            // precisamos pensar como vamos fazer isso
        }
        
        tutorialFeedback.hidden = true
        
        if (tutorial != nil) {
            tutorial!.initTutorial()
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        setupAutoComplete()
        handleTextFieldInterfaces()
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        cityView.makeCircle()
        //cityTokenView.makeCircular(10)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        
        UIApplication.sharedApplication().statusBarStyle = .Default
    }

    // MARK: - Slider
    @IBAction func daysChanged(sender: UISlider) {
        daysLabel.text = String(Int(sender.value))
        days = NSNumber(integer: Int(sender.value))
    }
    
    // MARK: - Actions
    @IBAction func saveCity (sender: UIButton) {
        if (city != nil) {
            if (place == nil) {
                let cities = itinerary.cities()!
                if (!cities.contains(city)){
                    place = Place()
                    
                    place!.city = city
                    place!.interval = NSNumber(integer: Int(daysSlider.value))
                    place!.itinerary = itinerary
                    
                    place!.save()
                    
                    if (tutorial != nil) {
                        if (tutorial!.index == 2) {
                            tutorial!.advanceLevel()
                        }
                    }
                    
                    performSegueWithIdentifier("closeDoneModalCity", sender: self)
                }  else {
                    cantAddCityAlert()
                }
            } else {
                place!.city = city
                place!.interval = NSNumber(integer: Int(daysSlider.value))
                place!.itinerary = itinerary
                
                place!.save()
                
                if (tutorial != nil) {
                    if (tutorial!.index == 2) {
                        tutorial!.advanceLevel()
                    }
                }
                
                performSegueWithIdentifier("closeDoneModalCity", sender: self)
            }
        }
        else {
            // Avisa que nao criou
        }
    }

    // MARK: - Tap Gesture
    func closeCity(recognizer: UITapGestureRecognizer) {
        if (city != nil) {
            if (place == nil) {
                let cities = itinerary.cities()!
                if (!cities.contains(city)){
                    place = Place()
                    
                    place!.city = city
                    place!.interval = NSNumber(integer: Int(daysSlider.value))
                    place!.itinerary = itinerary
                    
                    place!.save()
                    
                    if (tutorial != nil) {
                        if (tutorial!.index == 2) {
                            tutorial!.advanceLevel()
                        }
                    }
                    
                    performSegueWithIdentifier("closeDoneModalCity", sender: self)
                }  else {
                    cantAddCityAlert()
                }
            } else {
                place!.city = city
                place!.interval = NSNumber(integer: Int(daysSlider.value))
                place!.itinerary = itinerary
                
                place!.save()
                
                if (tutorial != nil) {
                    if (tutorial!.index == 2) {
                        tutorial!.advanceLevel()
                    }
                }
                
                performSegueWithIdentifier("closeDoneModalCity", sender: self)
            }
        }
        else {
            performSegueWithIdentifier("closeCancelModalCity", sender: self)
        }
    }
    
    func callPlannerInside(recognizer: UITapGestureRecognizer) {
        if (city != nil){
            if (place == nil){
                place = Place()
            }
            
            place!.city = city
            place!.interval = NSNumber(integer: Int(daysSlider.value))
            place!.itinerary = itinerary
            
            place!.save()
            
            performSegueWithIdentifier("showPlannerInside", sender: self)
        }
    }
    
    // MARK: - Unwind Segue
    @IBAction func donePlannerInsideUnwindSegue(unwindSegue: UIStoryboardSegue){
        if (tutorial != nil) {
            tutorial!.delegate = self
            tutorial!.initTutorial()
        }
    }

    // MARK: - Segue
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?){
        if (segue.identifier == "showPlannerInside"){
            let plannerInsideController = segue.destinationViewController as! PlannerInsideController
            plannerInsideController.itinerary = itinerary
            plannerInsideController.place = place
            
            // CUIDADO DATAS
            if (place != nil) {
                let beginDate = place!.beginDate()
                
                if (beginDate != nil) {
                    let endDate = beginDate!.dateByAddingTimeInterval(NSTimeInterval(Double(place!.interval!) * 86400.00))
                    plannerInsideController.beginDate = beginDate
                    plannerInsideController.endDate = endDate
                }
                else {
                    plannerInsideController.beginDate = beginDate
                    plannerInsideController.endDate = nil
                }
            }
            
            plannerInsideController.tutorial = tutorial
            if (tutorial != nil) {
                tutorial!.delegate = plannerInsideController
            }
        }
        
        if (segue.identifier == "closeDoneModalCity"){
            let plannerController = segue.destinationViewController as! PlannerController
            if (place != nil){
                plannerController.addedPlace = place
            }
        }
    }

    // MARK: - TextField Delegate
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if (textField is AutoCompleteTextField){
            let auTextField = textField as! AutoCompleteTextField
            if (self.view.center.y - (auTextField.center.y + auTextField.autoCompleteTableHeight!) < 0){
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.view.transform = CGAffineTransformMakeTranslation(0, self.view.center.y - (auTextField.center.y + auTextField.autoCompleteTableHeight!))
                })
            }
        } else {
            if (self.view.center.y - textField.center.y < 0){
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.view.transform = CGAffineTransformMakeTranslation(0, self.view.center.y - textField.center.y )
                })
            }
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.view.transform = CGAffineTransformMakeTranslation(0, 0)
        })
    }
    
    // MARK: - Autocomplete
    func setupAutoComplete(){
        cityTextField.autoCompleteTextColor = ColorPalette.purple()
        cityTextField.autoCompleteTextFont = UIFont(name: "HelveticaNeue-Light", size: 12.0)
        cityTextField.autoCompleteCellHeight = cityTextField.frame.size.height
        cityTextField.maximumAutoCompleteCount = 20
        cityTextField.hidesWhenSelected = true
        cityTextField.hidesWhenEmpty = true
        cityTextField.enableAttributedText = true
        var attributes = [String:AnyObject]()
        attributes[NSForegroundColorAttributeName] = UIColor.blackColor()
        attributes[NSFontAttributeName] = UIFont(name: "HelveticaNeue-Bold", size: 12.0)
        cityTextField.autoCompleteAttributes = attributes
    }
    
    func handleTextFieldInterfaces(){
        cityTextField.onTextChange = {text in
            if !text.isEmpty{
                
                self.cities = City.search(text, queries: nil)
                var citiesString: [String] = []
                
                for city in self.cities {
                    if (city.region != nil){
                        citiesString.append("\(city.name!) - \(city.region!), \(city.country!.name!)")
                    } else {
                        citiesString.append("\(city.name!), \(city.country!.name!)")
                    }
                }
                
                self.cityTextField.autoCompleteStrings = citiesString
            } else {
                self.city = nil
            }
        }
        
        cityTextField.onSelect = {text, indexpath in
            self.city = self.cities[indexpath.row]
        }
    }
    
    // MARK: - Alerts
    func cantAddCityAlert() {
        let alert = UIAlertController(title: "Já existe essa cidade", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        let defaultAction = UIAlertAction(title: "Entendido", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        presentViewController(alert, animated: true, completion: nil)
    }
}

extension CityController : Search {
    func searchDelegate(anyObject: [AnyObject]) {
        self.cities = anyObject as! [City]
        
        var citiesString: [String] = []
        
        for city in cities {
            citiesString.append("\(city.name!), \(city.country!.name!)")
        }
        
        dispatch_sync(dispatch_get_main_queue()) {
            self.cityTextField.autoCompleteStrings = citiesString
        }
        
    }
}

// MARK: - Tutorial
extension CityController: TutorialDelegate {
    func prepareTutorial(index: Int) {
        switch index {
        case 2:
            tutorialFeedback.hidden = false
            tutorialFeedback.text = "Para criar uma cidade, procure a cidade nesse campo e diga o número de dias que pretende ficar"
            break
        case 4:
            tutorialFeedback.hidden = false
            tutorialFeedback.text = "Toque dentro bola roxa para entrar dentro da cidade"
            break
        case 5:
            tutorialFeedback.hidden = false
            tutorialFeedback.text = "Toque no pronto para salvar e voltar para o planejador"
        default:
            tutorialFeedback.hidden = true
            break
        }
    }
    
    func finishTutorial() {
        tutorial = nil
    }
    
    func updateName() {
        
    }
    
    func advanceLevel() {
        
    }
}
