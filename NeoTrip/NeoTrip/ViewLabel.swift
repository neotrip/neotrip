//
//  ViewLabel.swift
//  NeoTrip
//
//  Created by Joao Pedro Fabiano Franco on 10/28/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import UIKit

class ViewLabel: UIView {
    
    var label: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupLabel()
    }
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        
        self.backgroundColor = ColorPalette.gray()
        self.makeCircle()
    }
    
    func setupLabel(){
        label = UILabel()
        label.textAlignment = .Center
        label.textColor = ColorPalette.purple()
        label.translatesAutoresizingMaskIntoConstraints = false
        
        self.addSubview(label)
        
        let labelTop = NSLayoutConstraint(item: label, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0)
        let labelBottom = NSLayoutConstraint(item: label, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 0)
        let labelLeft = NSLayoutConstraint(item: label, attribute: NSLayoutAttribute.Left, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Left, multiplier: 1, constant: 0)
        let labelRight = NSLayoutConstraint(item: label, attribute: NSLayoutAttribute.Right, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Right, multiplier: 1, constant: 0)
        
        self.addConstraint(labelTop)
        self.addConstraint(labelBottom)
        self.addConstraint(labelLeft)
        self.addConstraint(labelRight)
    }
    
    func getText() -> String{
        return label.text!
    }
    
    func setText(text: String){
        label.text = text
    }

}
