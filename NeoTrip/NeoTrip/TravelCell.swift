//
//  TravelCell.swift
//  NeoTrip
//
//  Created by Lucas Padilha on 1/26/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

class TravelCell: UITableViewCell {

    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var interval: UILabel!
    @IBOutlet weak var price: UILabel!
    @IBOutlet weak var owner: UILabel?

    @IBOutlet weak var joinButton : UIButton!
    
    var travel : Travel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }
    
    func fillCell(travel: Travel) {
        self.name.text = travel.fillName()
        
        if (travel.itinerary?.interval != nil) {
            self.interval.text = String(travel.itinerary!.interval!)
        }
        else {
            self.interval.text = "0"
        }
        
//        if (travel.itinerary?.price != nil) {
//            self.price.text = String(travel.itinerary!.price!)
//        }
//        else {
//            self.price.text = "0.0"
//        }
        
        if (travel.travelerOwner != nil) {
            self.owner?.text = "criada por: @" + String(travel.travelerOwner!.id!)
            self.owner?.hidden = false
        }
        else {
            self.owner?.hidden = true
        }
        
        self.travel = travel
    }
    
    // MARK: - Button Actions
    @IBAction func tapJoinButton(sender: UIButton) {
        
    }
    
    func buttonVisibility(hidden: Bool) {
        joinButton.hidden = hidden
    }
}
