//
//  CircularView.swift
//  NeoTrip
//
//  Created by Lucas Padilha on 12/19/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import UIKit

class CircularView: UIView {
    
    // Only override drawRect: if you perform custom drawing.
    // An empty implementation adversely affects performance during animation.
    override func drawRect(rect: CGRect) {
        // Drawing code
        super.drawRect(rect)
        self.makeCircle()
    }
}
