//
//  PortController.swift
//  NeoTrip
//
//  Created by Lucas Padilha on 1/7/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

class PortController: UIViewController, UITextFieldDelegate {

    @IBOutlet weak var nameTextField: UITextField!
    
    var place : Place!
    var itinerary: Itinerary!
    var port : Port?
    
    var name : String = ""
    var type : NSNumber = -1
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Gestures
        let touchOutside : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("cancelPort:"))
        self.view.addGestureRecognizer(touchOutside)
        
        // Delegates
        nameTextField.delegate = self
        
        // Default
        if (port != nil) {
            if (port!.name != nil) {
                name = port!.name!
                nameTextField.text = name
            }
            
            //type
        }
        else {
            
        }
    }

    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        UIApplication.sharedApplication().statusBarStyle = .Default
    }
    
    // MARK: - Button Action
    @IBAction func savePort(sender: UIButton) {
        if (validPort()) {
            if (port == nil) {
                port = Port()
            }
            
            port!.name = name
            port!.interval = 1
            
            // kobuchi precisa implementar
//            port!.type = type
            
            port!.city = place.city
            port!.itinerary = itinerary
            
            port!.category = PlaceType.PORT
            
            port!.save()
        }
        performSegueWithIdentifier("closeDoneModalPort", sender: self)
    }
    
    // Mark: - Tap Gesture
    func cancelPort(recognizer: UITapGestureRecognizer) {
        performSegueWithIdentifier("closeCancelModalPort", sender: self)
    }
    
    // MARK: - TextField Delegate
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        if (textField == nameTextField) {
            self.name = textField.text!
        }
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        if (textField == nameTextField) {
            self.name = textField.text!
        }
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    // MARK: - Auxiliar Methods
    func validPort () -> Bool {
        if (name != "") {
            return true
        }
        else {
            return false
        }
    }

}
