//
//  TravelerTableViewCell.swift
//  NeoTrip
//
//  Created by Lucas Padilha on 1/22/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

class TravelerCell: UITableViewCell {
    
    @IBOutlet weak var name: UILabel!
    @IBOutlet weak var username: UILabel!
    @IBOutlet weak var avatarImage: UIImageView!
    @IBOutlet weak var followButton: CustomButton!
    
    let waitingIndicator = UIActivityIndicatorView()
    
    var following : Bool = false
    var traveler : Traveler!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Configure the view for the selected state
        
    }
    
    @IBAction func tapFollowButton(sender: UIButton) {
        if (following) {
            followButton.enabled = false
            waitingIndicator.color = UIColor.whiteColor()
            followButton.startWaiting(waitingIndicator)
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                let result : Result = databaseManager!.unfollowTraveler(self.traveler)
                dispatch_sync(dispatch_get_main_queue()) {
                    if (result == Result.YES) {
                        self.following = false
                        self.followButton.setColorBorder(ColorPalette.purple())
                        self.followButton.backgroundColor = UIColor.clearColor()
                        self.followButton.setTitleColor(ColorPalette.purple(), forState: UIControlState.Normal)
                        self.followButton.setTitle("Seguir", forState: UIControlState.Normal)
                        
                        self.followButton.enabled = true
                        self.followButton.stopWaiting(self.waitingIndicator)
                    }
                }
            }
        }
        else {
            followButton.enabled = false
            waitingIndicator.color = ColorPalette.purple()
            followButton.startWaiting(waitingIndicator)
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                let result : Result = databaseManager!.followTraveler(self.traveler)
                dispatch_sync(dispatch_get_main_queue()) {
                    if (result == Result.YES) {
                        self.following = true
                        self.followButton.setColorBorder(UIColor.whiteColor())
                        self.followButton.backgroundColor = ColorPalette.purple()
                        self.followButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                        self.followButton.setTitle("Seguindo", forState: UIControlState.Normal)
                        
                        self.followButton.enabled = true
                        self.followButton.stopWaiting(self.waitingIndicator)
                    }
                }
            }
        }
    }
    
    func fillCell(loggedTraveler: Traveler, traveler: Traveler){
        var name : String!
        if (traveler.lastName != nil) {
            name = "\(traveler.firstName!) \(traveler.lastName!)"
        }
        else {
            name = "\(traveler.firstName!)"
        }
        let username = "@\(traveler.id!)"
        var image = UIImage()
        
        if((traveler.avatar.image) != nil){
            image = UIImage(data: traveler.avatar.image!)!
        }else{
            image = UIImage(named: "avatar-backpacker")!
        }
        
        if (loggedTraveler != traveler) {
            let following = loggedTraveler.following(traveler)
            
            if (following) {
                followButton.setColorBorder(UIColor.whiteColor())
                followButton.backgroundColor = ColorPalette.purple()
                followButton.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
                followButton.setTitle("Seguindo", forState: UIControlState.Normal)
            }
            else {
                followButton.setColorBorder(ColorPalette.purple())
                followButton.backgroundColor = UIColor.clearColor()
                followButton.setTitleColor(ColorPalette.purple(), forState: UIControlState.Normal)
                followButton.setTitle("Seguir", forState: UIControlState.Normal)
            }
            self.following = following
            buttonVisibility(false)
        }
        else {
            buttonVisibility(true)
        }
        self.name.text = name
        self.username.text = username
        self.avatarImage.image = image
        self.traveler = traveler
    }
    
    func buttonVisibility(hidden: Bool) {
        followButton.hidden = hidden
    }
}
