//
//  TravelController.swift
//  NeoTrip
//
//  Created by Lucas Padilha on 1/10/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

class TravelController: UIViewController, UIScrollViewDelegate, UITextFieldDelegate, UITableViewDelegate, UITableViewDataSource {
    
    // ScrollView
    @IBOutlet var scrollView:UIScrollView!
    
    // TableView
    @IBOutlet weak var travelersTableView: UITableView!
    
    @IBOutlet var avatarImage:UIView!
    @IBOutlet weak var travelTime: UILabel!
    @IBOutlet var header:UIView!
    @IBOutlet var headerLabel:UILabel!
    @IBOutlet var headerImageView:UIImageView!
    @IBOutlet var headerBlurImageView:UIImageView!
    var blurredHeaderImageView:UIImageView?
    
    @IBOutlet weak var backButton: UIButton!
    
    // TextFields
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var beginDateTextField: UITextField!
    @IBOutlet weak var endDateTextField: UITextField!
    @IBOutlet weak var sourceCityTextField: AutoCompleteTextField!
    @IBOutlet weak var addTravelerTextField: AutoCompleteTextField!
    
    let offset_HeaderStop:CGFloat = 40.0 // At this offset the Header stops its transformations
    let offset_B_LabelHeader:CGFloat = 95.0 // At this offset the Black label reaches the Header
    let distance_W_LabelHeader:CGFloat = 35.0 // The distance between the bottom of the Header and the top of the White Label
    
    var cities: [City] = []
    var sourceCity: City?
    
    var searchTravelers: [Traveler] = []
    
    lazy var queue:Queue = {
        var queue = Queue()
        queue.name = "search"
        queue.maxConcurrentOperationCount = 1
        return queue
    }()
    
    class SearchTravel: NSOperation {
        
        let getType:SearchType!
        let query:String!
        let options:NSMutableDictionary?
        
        let delegate:Search
        
        init(request:NSDictionary, search:Search) {
            getType = SearchType(rawValue: request["type"] as! Int)!
            query = request["query"] as! String
            options = request["options"] as? NSMutableDictionary
            delegate = search
        }
        
        override func main() {
            
            if (self.cancelled) {
                return
            }
            
            options!["query"] = query
            let (_, array) = databaseManager!.search(getType, options: options!)
            delegate.searchDelegate(array as [AnyObject])
        }
    }
    
    // Outside Information
    var travel : Travel!
    var currentTraveler : Traveler!
    var travelers : [Traveler]!
    var root : Place?
    
    // Tutorial
    var tutorial : Tutorial?
    @IBOutlet weak var tutorialBlurView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        travelers = travel.travelers?.allObjects as! [Traveler]
        
        // Delegates
        scrollView.delegate = self
        nameTextField.delegate = self
        beginDateTextField.delegate = self
        endDateTextField.delegate = self
        sourceCityTextField.delegate = self
        travelersTableView.delegate = self
        travelersTableView.dataSource = self
        
        // Other Set Up
        avatarImage.layer.borderColor = UIColor.whiteColor().CGColor
        avatarImage.layer.borderWidth = 3.0
        
        // Getting Travel Information
        travelInformation()
        
        setupAutoComplete()
        handleTextFieldInterfaces()
        
        // Notification
//        NSNotificationCenter.defaultCenter().addObserver(self, selector: "statusChanged:", name: ConnectionChanged, object: nil)
        
        // Tutorial
        if (tutorial != nil) {
            initTutorial()
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        
        // Header - Image
        
        //        headerImageView = UIImageView(frame: header.bounds)
        //        headerImageView?.image = UIImage(named: "background-travel")
        //        headerImageView?.contentMode = UIViewContentMode.ScaleAspectFill
        //        header.insertSubview(headerImageView, belowSubview: headerLabel)
        
        // Header - Blurred Image
        
        headerBlurImageView = UIImageView(frame: header.bounds)
        headerBlurImageView?.image = UIImage(named: "background-travel")?.blurredImageWithRadius(10, iterations: 20, tintColor: UIColor.clearColor())
        headerBlurImageView?.contentMode = UIViewContentMode.ScaleAspectFill
        headerBlurImageView?.alpha = 0.0
        header.insertSubview(headerBlurImageView, belowSubview: headerLabel)
        
        header.clipsToBounds = true
        
        headerLabel.shadowColor = UIColor.blackColor()
        
        travelersTableView.reloadData()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
    // MARK: - ScrollView Delegate Methods
    func scrollViewDidScroll(scrollView: UIScrollView) {
        
        let offset = scrollView.contentOffset.y
        var avatarTransform = CATransform3DIdentity
        var headerTransform = CATransform3DIdentity
        
        // PULL DOWN -----------------
        
        if offset < 0 {
            
            let headerScaleFactor:CGFloat = -(offset) / header.bounds.height
            let headerSizevariation = ((header.bounds.height * (1.0 + headerScaleFactor)) - header.bounds.height)/2.0
            headerTransform = CATransform3DTranslate(headerTransform, 0, headerSizevariation, 0)
            headerTransform = CATransform3DScale(headerTransform, 1.0 + headerScaleFactor, 1.0 + headerScaleFactor, 0)
            
            header.layer.transform = headerTransform
        }
            
            // SCROLL UP/DOWN ------------
            
        else {
            
            // Header -----------
            
            headerTransform = CATransform3DTranslate(headerTransform, 0, max(-offset_HeaderStop, -offset), 0)
            
            //  ------------ Label
            
            let labelTransform = CATransform3DMakeTranslation(0, max(-distance_W_LabelHeader, offset_B_LabelHeader - offset), 0)
            headerLabel.layer.transform = labelTransform
            
            //  ------------ Blur
            
            headerBlurImageView?.alpha = min (1.0, (offset - offset_B_LabelHeader)/distance_W_LabelHeader)
            
            // Avatar -----------
            
            let avatarScaleFactor = (min(offset_HeaderStop, offset)) / avatarImage.bounds.height / 1.4 // Slow down the animation
            let avatarSizeVariation = ((avatarImage.bounds.height * (1.0 + avatarScaleFactor)) - avatarImage.bounds.height) / 2.0
            avatarTransform = CATransform3DTranslate(avatarTransform, 0, avatarSizeVariation, 0)
            avatarTransform = CATransform3DScale(avatarTransform, 1.0 - avatarScaleFactor, 1.0 - avatarScaleFactor, 0)
            
            if offset <= offset_HeaderStop {
                
                if avatarImage.layer.zPosition < header.layer.zPosition{
                    header.layer.zPosition = 0
                    backButton.layer.zPosition = 3
                }
                
            }else {
                if avatarImage.layer.zPosition >= header.layer.zPosition{
                    header.layer.zPosition = 2
                    backButton.layer.zPosition = 3
                }
            }
        }
        
        // Apply Transformations
        
        header.layer.transform = headerTransform
        avatarImage.layer.transform = avatarTransform
    }
    
    // MARK: - Button Action
    @IBAction func saveAndBack(sender: UIButton) {
        travel.save()
        performSegueWithIdentifier("closeTravel", sender: self)
    }
    
    // MARK: - TextField Delegate Methods
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        return true
    }
    
    func textFieldDidBeginEditing(textField: UITextField) {
        if (textField is AutoCompleteTextField){
            let auTextField = textField as! AutoCompleteTextField
            if (self.view.center.y - (auTextField.center.y + auTextField.autoCompleteTableHeight!) < 0){
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.view.transform = CGAffineTransformMakeTranslation(0, self.view.center.y - (auTextField.center.y + auTextField.autoCompleteTableHeight!))
                })
            }
        } else {
            if (self.view.center.y - textField.center.y < 0){
                UIView.animateWithDuration(0.5, animations: { () -> Void in
                    self.view.transform = CGAffineTransformMakeTranslation(0, self.view.center.y - textField.center.y )
                })
            }
        }
        
        // MARK: - Textfield
        if (textField == beginDateTextField) {
            if (travel.itinerary!.startTime != nil) {
                textField.dateTextField(NSDate(), date: travel.itinerary!.startTime!)
            }
            else {
                textField.dateTextField(NSDate())
            }
        }
        else if (textField == endDateTextField) {
            if (travel.itinerary!.startTime != nil) {
                if (travel.itinerary!.interval != nil && travel.itinerary!.interval != 0) {
                    textField.dateTextField(travel.itinerary!.startTime!.dateByAddingTimeInterval(Double(travel.itinerary!.interval!) * 86400.00))
                }
                else {
                    textField.dateTextField(travel.itinerary!.startTime!)
                }
            }
            else {
                // caso nao tenha nenhuma informacao
                textField.dateTextField(NSDate())
            }
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        //        textField.resignFirstResponder()
        self.view.endEditing(true)
        return true
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.view.transform = CGAffineTransformMakeTranslation(0, 0)
        })
        
        if (textField == beginDateTextField) {
            if (beginDateTextField.text != nil) {
                travel.itinerary!.startTime = beginDateTextField.text!.stringToDate
            }
        }
        else if (textField == endDateTextField) {
            if (travel.itinerary!.startTime != nil) {
                if (textField.text != nil) {
                    // Pensar o que fazer aqui, não é trivial
                    //travel.itinerary!.setTimeInterval(textField.text!.stringToDate!)
                }
            }
            else {
                showErro("Porfavor preencha uma data de inicio")
            }
        }
        else if (textField == nameTextField) {
            if (tutorial != nil) {
                if (textField.text != nil &&
                    textField.text! != "") {
                        travel.name = "Tutorial " + String(tutorial!.index) + ": " + nameTextField.text!
                }
                else {
                    travel.name = "Tutorial " + String(tutorial!.index) + ": " + "(sem nome)"
                }
            }
            else {
                if (textField.text != nil &&
                    textField.text! != "") {
                        travel.name = nameTextField.text!
                }
                else {
                    travel.name = "(sem nome)"
                }
                headerLabel.text = travel.name!
            }
        }
        else if (textField == sourceCityTextField) {
            // Aqui precisa fazer alguma parada também.
        }
    }
    
    // MARK: - TableView Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return travelers.count + 1
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let result: UITableViewCell!
        
        if (indexPath.row == 0) {
            let travelerCell = tableView.dequeueReusableCellWithIdentifier("travelTravelerCell") as! TravelerCell
            travelerCell.fillCell(databaseManager!.traveler!, traveler: travel.travelerOwner!)
            if (travel.travelerOwner == currentTraveler) {
                travelerCell.buttonVisibility(true)
            }
            result = travelerCell
        } else if (indexPath.row < travelers.count+1){
            let travelerCell = tableView.dequeueReusableCellWithIdentifier("travelTravelerCell") as! TravelerCell
            travelerCell.fillCell(databaseManager!.traveler!, traveler: travelers[indexPath.row-1])
            
            if (travelers[indexPath.row-1] == currentTraveler) {
                travelerCell.buttonVisibility(true)
            }
            result = travelerCell
        } else {
            let newTraveler: NewTraveler = tableView.dequeueReusableCellWithIdentifier("newCell") as! NewTraveler
            result = newTraveler
        }
        
        return result
    }
    
    // MARK: - AutoComplete
    func setupAutoComplete(){
        sourceCityTextField.autoCompleteTextColor = ColorPalette.purple()
        sourceCityTextField.autoCompleteTextFont = UIFont(name: "HelveticaNeue-Light", size: 12.0)
        sourceCityTextField.autoCompleteCellHeight = 45.0
        sourceCityTextField.maximumAutoCompleteCount = 20
        sourceCityTextField.hidesWhenSelected = true
        sourceCityTextField.hidesWhenEmpty = true
        sourceCityTextField.enableAttributedText = true
        var attributes = [String:AnyObject]()
        attributes[NSForegroundColorAttributeName] = UIColor.blackColor()
        attributes[NSFontAttributeName] = UIFont(name: "HelveticaNeue-Bold", size: 12.0)
        sourceCityTextField.autoCompleteAttributes = attributes
        
        addTravelerTextField.autoCompleteTextColor = ColorPalette.purple()
        addTravelerTextField.autoCompleteTextFont = UIFont(name: "HelveticaNeue-Light", size: 12.0)
        addTravelerTextField.autoCompleteCellHeight = 45.0
        addTravelerTextField.maximumAutoCompleteCount = 20
        addTravelerTextField.hidesWhenSelected = true
        addTravelerTextField.hidesWhenEmpty = true
        addTravelerTextField.enableAttributedText = true
        addTravelerTextField.autoCompleteAttributes = attributes
    }
    
    func handleTextFieldInterfaces(){
        sourceCityTextField.onTextChange = {text in
            if !text.isEmpty{
                
                self.cities = City.search(text, queries: nil)
                var citiesString: [String] = []
                
                for city in self.cities {
                    if (city.region != nil){
                        citiesString.append("\(city.name!) - \(city.region!), \(city.country!.name!)")
                    } else {
                        citiesString.append("\(city.name!), \(city.country!.name!)")
                    }
                }
                
                self.sourceCityTextField.autoCompleteStrings = citiesString
            } else {
                self.sourceCity = nil
            }
        }
        
        sourceCityTextField.onSelect = {text, indexpath in
            self.sourceCity = self.cities[indexpath.row]
            
            if let oldRoot = self.travel.itinerary!.root() {
                oldRoot.category = .NONE
                oldRoot.interval = 1
                oldRoot.save()
            }
            
            if let place = self.travel.itinerary?.placeThatRepresents(self.sourceCity!) {
                self.root = place
            } else {
                self.root = Place()
            }
            
            self.root!.category = PlaceType.ROOT
            self.root!.itinerary = self.travel.itinerary!
            self.root!.interval = 0
            self.root!.city = self.sourceCity!
            self.root!.save()
        }
        
        
        addTravelerTextField.onTextChange = {text in
            if !text.isEmpty{
                
                let request = NSMutableDictionary()
                request["type"] = SearchType.TRAVELERS.rawValue
                request["query"] = text
                request["options"] = NSMutableDictionary()
                
                self.queue.addOperation(SearchTravel(request: request, search: self))
            }
        }
        
        addTravelerTextField.onSelect = {text, indexpath in
            self.addTravelerTextField.text = ""
            
            let addTraveler: Traveler = self.searchTravelers[indexpath.row]
            
            if (!self.travelers.contains(addTraveler) && addTraveler != self.travel.travelerOwner!){
                let waiting = UIActivityIndicatorView()
                waiting.color = UIColor.blackColor()
                
                self.addTravelerTextField.startWaiting(waiting)
                dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
                    let result:Result = databaseManager!.inviteTravelerToTravel(addTraveler, travel: self.travel)
                    dispatch_async(dispatch_get_main_queue()) {
                        waiting.stopWaiting(waiting)
                        switch (result) {
                        case .YES, .WAIT:
                            //reload
                            
                            self.travelers = self.travel.travelers?.allObjects as! [Traveler]
                            
                            self.travelersTableView.beginUpdates()
                            
                            self.travelersTableView.deleteSections(NSIndexSet(index: 0), withRowAnimation: .None)
                            self.travelersTableView.insertSections(NSIndexSet(index: 0), withRowAnimation: .None)
                            
                            self.travelersTableView.endUpdates()
                            break
                        default:
                            break
                        }
                    }
                }
            } else {
                self.cantAddTravelerAlert()
            }
            
            
            //self.travelers.append(self.addTraveler!)
            //self.travelersTableView.reloadData()
            //self.travel.save()
        }
    }
    
    // MARK: - Auxiliary Methods
    func showErro(msg:String){
        let alert = UIAlertController(title: "Erro", message: msg, preferredStyle: UIAlertControllerStyle.Alert)
        let defaultAction = UIAlertAction(title: "Entendido", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        presentViewController(alert, animated: true, completion: nil)
        //        let alert = UIAlertView()
        //        alert.title = "Erro"
        //        alert.message = msg
        //        alert.addButtonWithTitle("Entendido")
        //        alert.show()
    }
    
    func cantAddTravelerAlert(){
        let alert = UIAlertController(title: "Esse viajante já participa da viagem", message: "", preferredStyle: UIAlertControllerStyle.Alert)
        let defaultAction = UIAlertAction(title: "Entendido", style: .Default, handler: nil)
        alert.addAction(defaultAction)
        presentViewController(alert, animated: true, completion: nil)
    }
    
    func travelInformation() {
        if (travel.name != nil) {
            if (tutorial != nil) {
                nameTextField.text = travel.name!.componentsSeparatedByString(": ")[1]
                headerLabel.text = travel.name
            }
            else {
                nameTextField.text = travel.name
                headerLabel.text = travel.name
            }
        }
        
        if (travel.itinerary != nil) {
            if (travel.itinerary!.startTime != nil) {
                beginDateTextField.text = travel.itinerary!.startTime!.dateToString
            }
            if (travel.itinerary!.interval != nil) {
                endDateTextField.text = travel.itinerary!.startTime!.dateByAddingTimeInterval(NSTimeInterval(Double(travel.itinerary!.interval!) * 86400.00)).dateToString
                travelTime.text = String(Int(travel.itinerary!.interval!))
            }

            root = travel.itinerary?.root()
            
            if (root != nil) {
                sourceCity = root!.city
                sourceCityTextField.text = "\(root!.city!.name!), \(root!.city!.country!.name!)"
                if (root!.city!.region != nil){
                    sourceCityTextField.text = ("\(root!.city!.name!) - \(root!.city!.region!), \(root!.city!.country!.name!)")
                } else {
                    sourceCityTextField.text = "\(root!.city!.name!), \(root!.city!.country!.name!)"
                }
            }
        }
    }
    
    // MARK: - Tutorial
    
    func tutorialTap(recognizer: UITapGestureRecognizer) {
        tutorialBlurView.hidden = true
    }
    
    func initTutorial () {
        if (tutorial != nil) {
            tutorialBlurView.hidden = false
            let tutorialTap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("tutorialTap:"))
            tutorialBlurView.addGestureRecognizer(tutorialTap)
            tutorial!.initTutorial()
        }
    }
}

extension TravelController: Search {
    
    func searchDelegate(anyObject: [AnyObject]) {
        if (anyObject.count > 0){
            if (anyObject.first is City){
                self.cities = anyObject as! [City]
                
                var citiesString: [String] = []
                
                for city in cities {
                    citiesString.append("\(city.name!), \(city.country!.name!)")
                }
                
                dispatch_sync(dispatch_get_main_queue()) {
                    self.sourceCityTextField.autoCompleteStrings = citiesString
                }
                
            } else if (anyObject.first is Traveler){
                self.searchTravelers = anyObject as! [Traveler]
                
                var searchTravelersString: [String] = []
                
                for traveler in searchTravelers {
                    searchTravelersString.append("\(traveler.firstName!), @\(traveler.id!)")
                }
                
                dispatch_sync(dispatch_get_main_queue()) {
                    self.addTravelerTextField.autoCompleteStrings = searchTravelersString
                }
            }
        }
    }
}

extension TravelController: TutorialDelegate {
    func prepareTutorial(index: Int) {
        switch index {
        case 1:
            let subView: UIView = UIView.loadFromNibNamed("Tutorial 1")
            tutorialBlurView.addSubview(subView)
            break
        default:
            tutorialBlurView.hidden = true
            break
        }
    }
    
    func finishTutorial() {
        tutorial = nil
    }
    
    func advanceLevel() {
        
    }
    
    func updateName() {
        nameTextField.text = travel.name!.componentsSeparatedByString(": ")[1]
        headerLabel.text = travel.name
    }
}

// MARK: - Connection Delegate
//extension TravelController {
//    func statusChanged(notification:NSNotification) {
//        let status = Connection(rawValue: notification.object as! Int)
//        if (status == Connection.LOGGED) {
//            travelInformation()
//        } else {
//            
//        }
//    }
//}
