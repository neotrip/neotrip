//
//  TestsViewController.swift
//  NeoTrip
//
//  Created by Marcos Kobuchi on 10/27/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import UIKit
import CoreData

class TestsViewController: UIViewController, NSURLSessionDownloadDelegate {
    
    var task : NSURLSessionTask!
    
    var percentageWritten:Float = 0.0
    var taskTotalBytesWritten = 0
    var taskTotalBytesExpectedToWrite = 0
    
    lazy var session : NSURLSession = {
        let config = NSURLSessionConfiguration.ephemeralSessionConfiguration()
        config.allowsCellularAccess = false
        let session = NSURLSession(configuration: config, delegate: self, delegateQueue: NSOperationQueue.mainQueue())
        return session
    }()
    
    
    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didWriteData bytesWritten: Int64, totalBytesWritten writ: Int64, totalBytesExpectedToWrite exp: Int64) {
        print("downloaded \(100*writ/exp)")
        
        taskTotalBytesWritten = Int(writ)
        taskTotalBytesExpectedToWrite = Int(exp)
        percentageWritten = Float(taskTotalBytesWritten) / Float(taskTotalBytesExpectedToWrite)
        
        print(percentageWritten)
    }
    
    func URLSession(session: NSURLSession, task: NSURLSessionTask, didCompleteWithError error: NSError?) {
        print("completed: error: \(error)")
    }

    func URLSession(session: NSURLSession, downloadTask: NSURLSessionDownloadTask, didFinishDownloadingToURL location: NSURL) {
        
        print("finished downloading")
        let data = NSData(contentsOfURL: location)!
        let delimiter:String = ";"
        let csv = CSV(data: data)
        
        var i:Int = 0
        let batch:Int = 1000
        
        var country:Country?
        csv!.nextLine()
        while let line = csv!.nextLine() {
            let values = line.componentsSeparatedByString(delimiter)
            
            if (country == nil || country!.id != values[1]) {
                country = Country.country(values[1])
                country!.id = values[1]
                country!.name = values[2]
            }
            
            autoreleasepool {
                let id = Int(values[0])!
                let city:City = City.city(id)
                city.id = id
                city.name = values[4]
                city.latitude = Double(values[5])
                city.longitude = Double(values[6])
                city.region = values[3]
                city.country = country
            }
            
            if (batch < i++) {
                CoredataManager.sharedInstance.saveContext()
                CoredataManager.sharedInstance.managedObjectContext.reset()
                i = 0
                print(csv!.percentageRead())
            }
        }
        
        CoredataManager.sharedInstance.saveContext()
        CoredataManager.sharedInstance.managedObjectContext.reset()
        
        print("finished inserting")
    }
    
    override func viewDidLoad() {
        
        let url:NSURL = NSURL(string: "http://www.neotripapp.com/database/EN_US.csv")!
        //let data = NSData(contentsOfURL: url)
        //let data = NSData(contentsOfFile: "/Users/marcoskobuchi/Desktop/EN_US.csv")!
        
        let request = NSURLRequest(URL:url)
        let task = self.session.downloadTaskWithRequest(request)
        self.task = task
        task.resume()
        
        
        /*
        var inputStream : NSInputStream = NSInputStream(data: data!)
        inputStream.open()

        var buffer = [UInt8](count: 1024, repeatedValue: 0)
        var line1:String = ""
        
        print(buffer.capacity)
        while (inputStream.hasBytesAvailable) {
            var bytesRead = inputStream.read(&buffer, maxLength: buffer.capacity)
//            let string = NSString(bytes: buffer, length: bytesRead, encoding: encoding)
            let t = NSMutableData(bytes: buffer, length: bytesRead)
            let range = t.rangeOfData("\n".dataUsingEncoding(encoding)!, options: [], range: NSMakeRange(0, t.length))
            
            
            
            
            let line = NSString(data: t.subdataWithRange(NSMakeRange(0, range.location)),
                encoding: encoding)
            print(line)
//            print(String())
            //buffer.range
            */
            /*
            var lines: [String] = []
            string!.stringByTrimmingCharactersInSet(NSCharacterSet.newlineCharacterSet()).enumerateLines { line, stop in lines.append(line) }
            
            for (index, line) in lines.enumerate() {
                if index == 0 {
                    print("linha: \(line1 + lines[0])")
                } else if (index + 1 < lines.count) {
                    print("linha: \(line)")
                } else {
                    line1 = line
                    if (line == "\n") {
                        sleep(5)
                        print(line)
                    }
                    
                }
            }*/
        
        }

        
        
        //inputStream.close()

        /*
        print("downloaded")
        if let a = data?.rangeOfData("\n".dataUsingEncoding(encoding)!, options: NSDataSearchOptions.Anchored, range: NSMakeRange(0,data!.length)) {
            print(a.length)
        } else {
            print("epa")
        }*/
        
        
        //print(String(data: weatherData!, encoding: encoding))
        
        /*
        let sessionConfig = NSURLSessionConfiguration.defaultSessionConfiguration()
        let session = NSURLSession(configuration: sessionConfig, delegate: nil, delegateQueue: nil)
        let request = NSMutableURLRequest(URL: url)
        request.HTTPMethod = "GET"
        
        let task = session.dataTaskWithRequest(request, completionHandler: { (data: NSData?, response: NSURLResponse?, error: NSError?) -> Void in
            print("\n")
            if (error == nil) {
                // Success
                let statusCode = (response as! NSHTTPURLResponse).statusCode
                print("Success: \(statusCode)")
                
                let csv = CSV(data: data!)
                print("parseou")
                self.add(csv!)
                
            }
            else {
                // Failure
                print("Failure: %@", error!.localizedDescription);
            }
        })
        task.resume()
    }*/
    
    func add(csv:CSV) {
    }

    
    /*
    
    let interval = NSTimeInterval(12310)
    //let database = ParseManager()
    var traveler:Traveler?
    let managedObjectContext = CoredataManager.sharedInstance.managedObjectContext
    let delayTime = dispatch_time(DISPATCH_TIME_NOW, Int64(5 * Double(NSEC_PER_SEC)))
    
    override func viewDidLoad() {
        
        super.viewDidLoad()
        
        let entityDescription = NSEntityDescription.entityForName("Travel", inManagedObjectContext: managedObjectContext)
        let request = NSFetchRequest()
        request.entity = entityDescription
        do {
            let list = try managedObjectContext.executeFetchRequest(request) as! [Travel]
        } catch {}
        
        /* Test creating a traveler and user. */
        let traveler = Traveler()
        traveler.id = "user"
        traveler.countries = ""
        traveler.firstName = "usuario"
        traveler.lastName = "da silva"
        traveler.messages = nil
        traveler.posts = nil
        traveler.travels = nil
        traveler.travelsOwned = nil

        
        /* Automatic Login */
        do {
            let entityDescription = NSEntityDescription.entityForName("User", inManagedObjectContext: managedObjectContext)
            let request = NSFetchRequest()
            request.entity = entityDescription
            let list = try managedObjectContext.executeFetchRequest(request) as! [User]
            
            var user:User?
            if list.count > 0 {
                user = list[0]
            } else {
                user = User()
                user!.id = "user"
                user!.password = "password"
            }
            
            if let traveler2 = database.login(user!) {
                self.traveler = traveler2
                /*
                dispatch_after(delayTime, dispatch_get_main_queue()) {
                    transport.delete()
                }
                
                let delayTime2 = dispatch_time(DISPATCH_TIME_NOW, Int64(20 * Double(NSEC_PER_SEC)))
                dispatch_after(delayTime2, dispatch_get_main_queue()) {
                    place.delete()
                }*/
                
                /*
                let thirdPlace = Place()
                thirdPlace.startTime = NSDate()
                thirdPlace.interval = interval
                thirdPlace.price = 100
//                thirdPlace.address = "Rua 3"
                
                dispatch_after(delayTime, dispatch_get_main_queue()) {
                    thirdPlace.itinerary = itinerary
                    //database.createPlace(thirdPlace)
                }
                
                let newTransport = Transport()
                newTransport.price = 1000
                newTransport.startTime = NSDate()
                newTransport.interval = interval
                newTransport.startPoint = place
                newTransport.endPoint = secondPlace
                
                dispatch_after(delayTime, dispatch_get_main_queue()) {
                    newTransport.itinerary = itinerary
                    //database.createTransport(newTransport)
                }
                
                

                
                let travelZero = database.getTravelsOwned(forTraveler: traveler)![0] as! Travel
                database.getTravelInfo(forTravel: travelZero)
                
                for transport in travelZero.itinerary!.transports! {
                    let t = transport as! Transport
                    database.editTransport(t)
                }
                
                for place in travelZero.itinerary!.places! {
                    let p = place as! Place
                    database.editPlace(p)
                }
                
                database.editItinerary(travelZero.itinerary!)
                database.editTravel(travelZero)
                
                let travels = database.getTravelsTakingPart(forTraveler: traveler)
                //let travelZero = travels![0] as! Travel
                dispatch_after(delayTime, dispatch_get_main_queue()) {
                    database.deleteTravel(travelZero)
                }

*/
                
                
                
            } else {
                print("falha ao logar")
            }
        } catch {
        
        }
        
        /*
        let date1 = NSDate()
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            let date2 = NSDate()
            print(date1)
            print(date2)
            if date1.compare(date2) == NSComparisonResult.OrderedAscending {
                print("entrou")
            }
        }*/

        //database.signup(traveler, withUser: user)

    }
    
    var travel:Travel?
    @IBAction func doIt(sender: AnyObject) {
        
        /* Test creating a Travel with itinerary. */
        travel = Travel()
        let itinerary = travel!.itinerary!
        let place = Place()
        let secondPlace = Place()
        let transport = Transport()
        
        
        travel!.name = "Teste"
        travel!.travelerOwner = traveler
        travel!.save()
        
        
        //                itinerary.startTime = NSDate()
        itinerary.interval = interval
        itinerary.save()
        
        place.name = "Lugar 1"
        place.startTime = NSDate()
        place.interval = interval
        place.price = 100
        //                place.address = "Rua 1"
        place.itinerary = itinerary
        place.save()
        
        secondPlace.name = "Lugar 2"
        secondPlace.startTime = NSDate()
        secondPlace.interval = interval
        secondPlace.price = 100
        //                secondPlace.address = "Rua 2"
        secondPlace.itinerary = itinerary
        secondPlace.save()
        
        transport.price = 1000
        transport.startTime = NSDate()
        transport.interval = interval
        transport.startPoint = place
        transport.endPoint = secondPlace
        transport.itinerary = itinerary
        transport.save()

    }

    @IBAction func doSomething(sender: AnyObject) {
        
        database.getTravelInfo(forTravel: travel!)
        
        /*
        let entityDescription = NSEntityDescription.entityForName("Travel", inManagedObjectContext: managedObjectContext)
        let request = NSFetchRequest()
        request.entity = entityDescription
        
        do {
            let list = try managedObjectContext.executeFetchRequest(request) as! [Travel]
            
            if list.count > 0 {
                print("existe")
            }
            
            
        } catch {
        
        }*/
    }
    
    @IBAction func doMoreSomething(sender: AnyObject) {
        travel?.delete()
        
        /*
        
        let entityDescription = NSEntityDescription.entityForName("Travel", inManagedObjectContext: managedObjectContext)
        let request = NSFetchRequest()
        request.entity = entityDescription
        
        do {
            let list = try managedObjectContext.executeFetchRequest(request) as! [Travel]
            
            if list.count > 0 {
                print("existe \(list.count)")
                let travel = list[0]
                
                /*
                let t = travel.itinerary?.transports?.anyObject() as! Transport
                t.delete()
                */

            }
            
            
        } catch {
            
        }*/
    }
    */
}

