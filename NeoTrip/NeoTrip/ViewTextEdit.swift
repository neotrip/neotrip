//
//  ViewTextEdit.swift
//  NeoTrip
//
//  Created by Joao Pedro Fabiano Franco on 10/29/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import UIKit

class ViewTextEdit: UIView, UITextFieldDelegate {
    
    var textedit: UITextField!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        setupText()
    }
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        
        self.backgroundColor = ColorPalette.gray()
        self.makeCircle()
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func setupText(){
        textedit = UITextField()
        textedit.textAlignment = .Center
        textedit.textColor = ColorPalette.purple()
        textedit.translatesAutoresizingMaskIntoConstraints = false
        textedit.delegate = self
        
        self.addSubview(textedit)
        
        let textTop = NSLayoutConstraint(item: textedit, attribute: NSLayoutAttribute.Top, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Top, multiplier: 1, constant: 0)
        let textBottom = NSLayoutConstraint(item: textedit, attribute: NSLayoutAttribute.Bottom, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Bottom, multiplier: 1, constant: 0)
        let textLeft = NSLayoutConstraint(item: textedit, attribute: NSLayoutAttribute.Left, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Left, multiplier: 1, constant: 0)
        let textRight = NSLayoutConstraint(item: textedit, attribute: NSLayoutAttribute.Right, relatedBy: NSLayoutRelation.Equal, toItem: self, attribute: NSLayoutAttribute.Right, multiplier: 1, constant: 0)
        
        self.addConstraint(textTop)
        self.addConstraint(textBottom)
        self.addConstraint(textLeft)
        self.addConstraint(textRight)
    }
    
    func getText() -> String{
        return textedit.text!
    }

}
