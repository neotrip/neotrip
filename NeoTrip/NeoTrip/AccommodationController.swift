//
//  AccommodationController.swift
//  NeoTrip
//
//  Created by Lucas Padilha on 11/25/15.
//  Copyright © 2015 NeoTrip. All rights reserved.
//

import UIKit

class AccommodationController: UIViewController, UITextFieldDelegate {
    
    @IBOutlet weak var nameTextField: UITextField!
    @IBOutlet weak var daysSlider: UISlider!
    @IBOutlet weak var priceTextField: UITextField!
    @IBOutlet weak var daysLabel: UILabel!
    
    var place : Place!
    var itinerary : Itinerary!
    var accommodation : Accomodation?
    
    var days : NSNumber = 1
    var price : NSNumber = 0.0
    var name : String = ""
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Gestures
        // Do any additional setup after loading the view.
        let touchOutside: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("cancelAccommodation:"))
        self.view.addGestureRecognizer(touchOutside)
        
        // Delegates
        nameTextField.delegate = self
        priceTextField.delegate = self
        
        // Default
        if (accommodation != nil) {
            if (accommodation!.name != nil) {
                name = accommodation!.name!
                nameTextField.text = name
            }
            
            if (accommodation!.interval != nil) {
                let total = accommodation!.usedDays()
                
                let maximumValue = Float(Int(place!.interval!) - total)
                daysSlider.maximumValue = maximumValue
                
                days = accommodation!.interval!
                daysSlider.value = Float(Int(days))
                daysLabel.text = String(Int(days))
            }
            
            if (accommodation!.price != nil) {
                price = accommodation!.price!
                priceTextField.text = price.stringValue
            }
        }
        else {
            var total = 0
            for a in itinerary.accommodationsIn(place!.city!) {
                total = total + Int(a.interval!)
            }
            let maximumValue = Float(Int(place!.interval!) - total)
            if (maximumValue <= 0) {
                daysSlider.minimumValue = 0
                daysSlider.maximumValue = 0
                daysSlider.value = 0
                daysLabel.text = String(0)
            }
            else {
                days = maximumValue
                daysSlider.maximumValue = maximumValue
                daysSlider.value = maximumValue
                daysLabel.text = String(Int(maximumValue))
            }
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        UIApplication.sharedApplication().statusBarStyle = .LightContent
    }
    
    override func viewDidDisappear(animated: Bool) {
        super.viewDidDisappear(animated)
        
        UIApplication.sharedApplication().statusBarStyle = .Default
    }
    
    // MARK: - Button Action
    @IBAction func saveAccomodation(sender: UIButton) {
        
        if (validAccommodation()) {
            if (accommodation == nil) {
                accommodation = Accomodation()
            }
            
            accommodation!.price = price
            accommodation!.name = name
            accommodation!.interval = days
            
            accommodation!.city = place.city
            accommodation!.itinerary = itinerary
            
            accommodation!.category = PlaceType.ACCOMODATION
            
            accommodation!.save()
            performSegueWithIdentifier("closeDoneModalAccommodation", sender: self)
        }
    }
    
    // MARK: - Tap Gesture
    func cancelAccommodation(recognizer: UITapGestureRecognizer) {
        performSegueWithIdentifier("closeCancelModalAccommodation", sender: self)
    }
    
    
    // MARK: - Slider Action
    @IBAction func daysChanged(sender: UISlider) {
        daysLabel.text = String(Int(sender.value))
        days = NSNumber(integer: Int(sender.value))
    }
    
    // MARK: - TextField Delegate
    
    func textFieldDidBeginEditing(textField: UITextField) {
        
        if (textField == priceTextField){
            UIView.animateWithDuration(0.5, animations: { () -> Void in
                self.view.transform = CGAffineTransformMakeTranslation(0, -100)
            })
        }
    }
    
    func textFieldDidEndEditing(textField: UITextField) {
        
        if (textField == nameTextField) {
            self.name = textField.text!
        }
        
        if (textField == priceTextField) {
            if ((Double(textField.text!)) != nil) {
                self.price = (textField.text?.priceValue)!
            }
        }
        
        UIView.animateWithDuration(0.5, animations: { () -> Void in
            self.view.transform = CGAffineTransformMakeTranslation(0, 0)
        })
        
        self.view.endEditing(true)
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return true
    }
    
    // MARK: - Auxiliar Methods
    func validAccommodation () -> Bool {
        if (name != "") {
            return true
        }
        else {
            let alert = UIAlertController(title: "Campos Vazios", message: "É preciso preencher todas os campos para salvar", preferredStyle: UIAlertControllerStyle.Alert)
            let defaultAction = UIAlertAction(title: "Entendido", style: .Default, handler: nil)
            alert.addAction(defaultAction)
            presentViewController(alert, animated: true, completion: nil)
//            let alert = UIAlertView()
//            alert.title = "Campos Vazios"
//            alert.message = "É preciso preencher todas os campos para salvar"
//            alert.addButtonWithTitle("Entendido")
//            alert.show()
            return false
        }
    }
    
}
