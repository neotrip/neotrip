//
//  TransportNode.swift
//  NeoTrip
//
//  Created by Joao Pedro Fabiano Franco on 1/13/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

protocol TransportNodeDelegate{
    func showAddNewCity()
    func showEditTransport(transport: Transport)
    func touchDelete(transportNode: TransportNode)
    func longPress(TransportNode: TransportNode)
}

class TransportNode: UIView {
    
    var startPoint: CGPoint!
    var endPoint: CGPoint!
    var transport: Transport?
    
    var arrow: Arrow!
    
    var transportNodeDelegate: TransportNodeDelegate!
    
    var priceLabel: UILabel!
    var deleteView: UIView!
    
    var blockLong: Bool = false
    var animation : CABasicAnimation!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        self.backgroundColor = UIColor.clearColor()
    }
    
    override func drawRect(rect: CGRect) {
        super.drawRect(rect)
        
        let touchGesture: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("editTransport:"))
        self.addGestureRecognizer(touchGesture)
        
        let longPress:UILongPressGestureRecognizer = UILongPressGestureRecognizer(target: self, action: Selector("longPressOnNode:"))
        self.addGestureRecognizer(longPress)
        
        let height = self.bounds.size.height
        let width = self.bounds.size.width
        let x = self.bounds.origin.x
        let y = self.bounds.origin.y
        
        if (self.transportNodeDelegate is PlannerController){
            priceLabel = UILabel()
            priceLabel.frame = CGRect(x: x + width/2 - 30, y: y + height*0.25, width: 60, height: height*0.5)
            priceLabel.text = String.cashString(CGFloat(transport!.price!))
            priceLabel.backgroundColor = UIColor.whiteColor()
            priceLabel.font = UIFont(name: "Helvetica", size: 12.0)
            priceLabel.textAlignment = .Center
            priceLabel.layer.zPosition = 2
            self.addSubview(priceLabel)
        }
    
        deleteView = UIView()
        deleteView.frame = CGRect(x: x + width*0.4 - height/4, y: y + height*0.4 - height/4, width: 0.8*height, height: 0.8*height)
        deleteView.hidden = true
        deleteView.layer.zPosition = 3
        self.addSubview(deleteView)
        
        let deleteImageView = UIImageView(frame: deleteView.bounds)
        deleteImageView.image = UIImage(named: "Delete")
        deleteView.addSubview(deleteImageView)
        
        let tapDelete : UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: Selector("tapOnDelete:"))
        deleteView.addGestureRecognizer(tapDelete)
        
        // Arrow
        arrow = setArrow(CGPoint(x: x, y: y + 0.5*height), point2: CGPoint(x: 0.99*(x + width), y: y + 0.5*height))
        self.layer.addSublayer(arrow)
    }
    
    func setArrow(point1: CGPoint, point2: CGPoint) -> Arrow{
        let arrowPath = Arrow()
        arrowPath.fillColor = ColorPalette.blue().CGColor
        arrowPath.path = Arrow.bezierPathWithArrowFromPoint(point1, endPoint: point2, tailWidth: 2, headWidth: 10, headLength: 16).CGPath
        
        return arrowPath
    }
    
    func setView(){
        let deltaX = abs(startPoint.x - endPoint.x)
        let deltaY = abs(startPoint.y - endPoint.y)
        let width = sqrt(deltaX*deltaX + deltaY*deltaY)
        let angle = atan2(endPoint.y-startPoint.y, endPoint.x-startPoint.x)
        
        self.layer.anchorPoint = CGPointMake(0.0, 0.0)
        self.frame = CGRect(origin: startPoint, size: CGSize(width: width, height: TRANSPORT_HEIGHT))
        self.transform = CGAffineTransformMakeRotation(angle)
            
        self.setNeedsDisplay()
        self.setNeedsLayout()
    }
    
    func removeArrow(){
        for layer in self.layer.sublayers!{
            layer.removeFromSuperlayer()
        }
    }
    
    func editTransport(recognizer: UITapGestureRecognizer){
        transportNodeDelegate.showEditTransport(self.transport!)
    }
    
    func changeDeleteMode (value: Bool) {
        deleteView.hidden = !value
        if (!value) {
            stopShacking()
        }
        else {
            startShaking()
        }
    }
    
    func startShaking() {
        animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.1
        animation.repeatCount = Float.infinity
        animation.autoreverses = true
        animation.fromValue = NSValue(CGPoint: CGPointMake(self.center.x - 2, self.center.y))
        animation.toValue = NSValue(CGPoint: CGPointMake(self.center.x + 2, self.center.y))
        self.layer.addAnimation(animation, forKey: "position")
    }
    
    func stopShacking() {
        self.layer.removeAllAnimations()
    }
    
    func longPressOnNode(recognizer: UILongPressGestureRecognizer) {
        if (recognizer.state == UIGestureRecognizerState.Began && !self.blockLong) {
            transportNodeDelegate.longPress(self)
            self.blockLong = true
        }
        if (recognizer.state == UIGestureRecognizerState.Ended ||
            recognizer.state == UIGestureRecognizerState.Cancelled ||
            recognizer.state == UIGestureRecognizerState.Failed) {
                self.blockLong = false
        }
    }
    
    func tapOnDelete (recognizer: UITapGestureRecognizer) {
        transportNodeDelegate.touchDelete(self)
    }
}

class Arrow: CAShapeLayer {
    
    class func getAxisAlignedArrowPoints(inout points: Array<CGPoint>, forLength: CGFloat, tailWidth: CGFloat, headWidth: CGFloat, headLength: CGFloat ) {
        
        let tailLength = forLength - headLength
        points.append(CGPointMake(0, tailWidth/2))
        points.append(CGPointMake(tailLength, tailWidth/2))
        points.append(CGPointMake(tailLength, headWidth/2))
        points.append(CGPointMake(forLength, 0))
        points.append(CGPointMake(tailLength, -headWidth/2))
        points.append(CGPointMake(tailLength, -tailWidth/2))
        points.append(CGPointMake(0, -tailWidth/2))
    }
    
    
    class func transformForStartPoint(startPoint: CGPoint, endPoint: CGPoint, length: CGFloat) -> CGAffineTransform{
        let cosine: CGFloat = (endPoint.x - startPoint.x)/length
        let sine: CGFloat = (endPoint.y - startPoint.y)/length
        
        return CGAffineTransformMake(cosine, sine, -sine, cosine, startPoint.x, startPoint.y)
    }
    
    
    class func bezierPathWithArrowFromPoint(startPoint:CGPoint, endPoint: CGPoint, tailWidth: CGFloat, headWidth: CGFloat, headLength: CGFloat) -> UIBezierPath {
        let xdiff: Float = Float(endPoint.x) - Float(startPoint.x)
        let ydiff: Float = Float(endPoint.y) - Float(startPoint.y)
        let length = hypotf(xdiff, ydiff)
        
        var points = [CGPoint]()
        self.getAxisAlignedArrowPoints(&points, forLength: CGFloat(length), tailWidth: tailWidth, headWidth: headWidth, headLength: headLength)
        
        var transform: CGAffineTransform = self.transformForStartPoint(startPoint, endPoint: endPoint, length:  CGFloat(length))
        
        let cgPath: CGMutablePathRef = CGPathCreateMutable()
        CGPathAddLines(cgPath, &transform, points, 7)
        CGPathCloseSubpath(cgPath)
        
        let uiPath: UIBezierPath = UIBezierPath(CGPath: cgPath)
        return uiPath
    }
}
