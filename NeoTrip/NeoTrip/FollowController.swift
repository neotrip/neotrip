//
//  FollowController.swift
//  NeoTrip
//
//  Created by Joao Pedro Fabiano Franco on 1/22/16.
//  Copyright © 2016 NeoTrip. All rights reserved.
//

import UIKit

class FollowController: UIViewController {

    @IBOutlet weak var peopleTableView: UITableView!
    
    var followTravelers: [Traveler] = []
    var selectedTraveler: Traveler?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        peopleTableView.reloadData()
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if (segue.identifier == "showGenericProfile") {
            let genericProfile : GenericProfileController = segue.destinationViewController as! GenericProfileController
            if (selectedTraveler != nil) {
                genericProfile.traveler = selectedTraveler!
            }
        }
    }
    
    // MARK: - Action
    @IBAction func back(){
        navigationController?.popViewControllerAnimated(true)
    }
}

extension FollowController: UITableViewDelegate, UITableViewDataSource{
    
    func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return followTravelers.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let result: UITableViewCell!
        
        let travelerCell = tableView.dequeueReusableCellWithIdentifier("followTravelerCell", forIndexPath: indexPath) as! TravelerCell
        travelerCell.fillCell(databaseManager!.traveler!, traveler: followTravelers[indexPath.row])
        result = travelerCell
        
        return result
    }
    
    func tableView(tableView: UITableView, heightForRowAtIndexPath indexPath: NSIndexPath) -> CGFloat {
        return 62
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedTraveler = followTravelers[indexPath.row]
        self.performSegueWithIdentifier("showGenericProfile", sender: self)
    }

}

